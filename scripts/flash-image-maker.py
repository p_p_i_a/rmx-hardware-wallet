#!/usr/bin/env python3

#
# This file is a part of RMX HARDWARE WALLET
#
# Copyright (C) 2018 i_a <i_a@rmxwallet.org>
#
# This library is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this library.  If not, see <http://www.gnu.org/licenses/>.
#

#from crccheck.crc import crcBase
from hashlib import sha256
import math
import os
import sys

print("spi_flashing.py runs python version")
print(sys.version)

final_binary_name = "binary.bin"
program_image_name = "main.bin"  #orezane_securelo_binarka.bin"

#################################
#   beginning of binary image   #
#################################


def beginning():
    header_offset = 2

    tag.append(0x02)
    tag.append(0x00)
    tag.append(0x00)
    tag.append(0x83)  #crc checksum for address 0x000002(00)
    for x in range(0, 12):  #finish the 16byte chunk
        tag.append(0xFF)

    for x in range(0, (header_offset * 256 - 16)):  #fill in the rest of bytes
        tag.append(0xFF)
    return


#################################
#            header             #
#################################


def header():
    # Byte 0
    header_beginning.append(ord('P'))
    # Byte 1
    header_beginning.append(ord('H'))
    # Byte 2
    header_beginning.append(ord('C'))
    # Byte 3
    header_beginning.append(ord('M'))

    # Byte 4
    header_beginning.append(0x00)  #header version
    # Byte 5
    header_beginning.append(0x07)  # Bit [7] CHPA_MISO
    # Bit [6] CHPA_MOSI
    # Bit [5] CPOL
    # Bit [4] SPI Slew rate
    # Bit [3:2] SPI Driver strength
    # Bit [1:0] SPI Clock speed
    # Byte 6
    header_beginning.append(0x00)  # Bit [7] Encryption
    # Bit [6] Reserved, 0
    # Bit [5:3] Must be 0x07
    # Bit [2:0] VTRx Level Bit Value
    # Byte 7
    header_beginning.append(0x01)  # Flash read command
    #		command:	SPI read command:
    #			0		0x03		normal read
    #			1		0x0B		fast read
    #			2		0x3B		fast read with double data return
    #			3		0x6B		fast read with quad data return

    # Byte 8 - 11		#start address in SRAM, little endian[7:0] [15:8] [23:16] [31:24]
    header_beginning.append(0x00)
    header_beginning.append(0x00)
    header_beginning.append(0x0B)
    header_beginning.append(0x00)

    # Byte 12 - 15		# Firmware entry point - here ROM code jumps after successfull load and verify
    # reads pointer from binary image
    with open(program_image_name, 'r+b') as fw_image:
        dump = fw_image.read()
    # appends this pointer
    header_beginning.append(dump[4+0x8000])
    header_beginning.append(dump[5+0x8000])
    header_beginning.append(dump[6+0x8000])
    header_beginning.append(dump[7+0x8000])

    # Byte 16 - 17		# FW size in bytes * 64

    size = int(math.ceil(len(dump) / 64.0))
    print("\r\nSize of alligned image in bytes: ")
    print(size * 64)
    print("\r\nSize of alligned image in 64B blocks: ")
    print(size)
    #In case size of firmware is bigger than 256B it needs to be divided into two 8bit integers:
    if size >= 256:
        header_beginning.append(size & 0xff)
        header_beginning.append((size >> 8) & 0xff)
    else:
        header_beginning.append(size)
        header_beginning.append(0x00)

    # Byte 18 - 19		# Reserved, 0
    header_beginning.append(0x00)
    header_beginning.append(0x00)
    # Byte 20 - 23		# (Unsigned) offset between beginning of the header and start of the fw
    # [7:0] [15:8] [23:16] [31:24], must be disible by 64
    header_beginning.append(
        0x80
    )  #0x80 means that fw starts 128B behind the beginning of header(PHCM...)
    header_beginning.append(0x00)
    header_beginning.append(0x00)
    header_beginning.append(0x00)

    # byte 24 - 63		# Reserved, 0
    for x in range(0, 40):
        header_beginning.append(0x00)

    # byte 64 - 95		# R term, signature of header
    hash_object = sha256(header_beginning)
    hex_dig = hash_object.hexdigest()
    print("Hash of header:")
    print(hex_dig)
    hexified = bytearray.fromhex(hex_dig)

    for x in range(0, 32):
        header_beginning.append(hexified[x])
    # byte 96 - 127		# S term, signature of header
    for x in range(0, 32):
        header_beginning.append(0x00)

    return


###################################
#   sha256 checksum  of header?   #
###################################


def sha256_binary():

    with open(program_image_name, 'r+b') as fw_image:
        dump = fw_image.read()

    hash_object = sha256(dump)
    hex_dig = hash_object.hexdigest()
    #print hex_dig		#debug
    hexified = bytearray.fromhex(hex_dig)

    for x in range(0, 32):
        sha256_fw_hash.append(hexified[x])

    #put zeroes at the end
    for x in range(0, 32):
        sha256_fw_hash.append(0x00)
    return


##########################################################################################
#   binary image align - must be padded with zeros and must fit into 64B blocks          #
##########################################################################################


def binary_image_align():

    with open(program_image_name, 'r+b'):
        dump = fw_image.read()

    #hash_object = sha256(dump)
    #hex_dig = hash_object.hexdigest()
    #print hex_dig		#debug
    #hexified = bytearray.fromhex(hex_dig)
    size_of_file = os.path.getsize(program_image_name)
    #udelat z neho nasobek 64
    zbytek = size_of_file % 64

    #print zbytek

    for x in range(0, size_of_file):
        binary_image_to_align.append(dump[x])
    if zbytek != 0:
        for x in range(0, (64 - zbytek)):
            binary_image_to_align.append(0x00)

    #put zeroes at the end
    #for x in range (0, 32):
    #	sha256_fw_hash.append(0x00)
    return


###########################################
#   sha256hash of aligned binary          #
###########################################


def second_binary_hash():

    hash_object = sha256(binary_image_to_align)
    hex_dig = hash_object.hexdigest()
    hexified = bytearray.fromhex(hex_dig)
    print("Hash of binary:")
    print(hex_dig)  #debug
    print("\r\n")
    for x in range(0, 32):
        hash_of_aligned_binary.append(hexified[x])

    #add zeroes
    for x in range(0, 32):
        hash_of_aligned_binary.append(0x00)


#####################################
#     code for making SPI image     #
#####################################

# -- Tooks all single parts defined by single bytearrays, put them together, construct one output file
# -- This file is ready to be flashed into cec1702 external flash

with open(final_binary_name, "w+b") as f,\
     open(program_image_name, "r+b") as fw_image:

    #dump = fw_image.read()
    #print dump
    #fw_image.seek(0)

    #inicialization of single pieces of binary data:
    tag = bytearray()
    header_beginning = bytearray()
    sha256_fw_hash = bytearray()
    sha256_header_hash = bytearray()
    binary_image_to_align = bytearray()
    hash_of_aligned_binary = bytearray()
    dummy_FFs = bytearray()

    #constructing all binary pieces
    beginning()
    header()
    #sha256_header()
    sha256_binary()
    binary_image_align()
    second_binary_hash()

    f.write(tag)
    f.write(header_beginning)
    f.write(binary_image_to_align)
    #f.write(	f.read()  +  fw_image.read()	)#adding binary image to spi image
    f.write(hash_of_aligned_binary)

    #and finally add FF to all unused positions in flash
    #print f.tell()  #tells size of file
    #i am using only 2MB from flash memory, so: 2 097 152
    positions_to_fill = (2097152 - f.tell())
    for x in range(0, positions_to_fill):
        dummy_FFs.append(0xFF)
    f.write(dummy_FFs)

#print "\r\n*********************\r\n*  IMAGE GENERATED  *\r\n*********************"
