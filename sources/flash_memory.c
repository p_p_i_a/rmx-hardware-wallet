/*
* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <string.h>

#include "uart.h"
#include "CEC1702_defines.h"
#include "flash_memory.h"
#include "crypto.h"
#include "login.h"
#include "storage.h"
#include "spi.h"



void spiEnable(void) {
  GPIO055_PIN_CONTROL_REGISTER = 0x10243UL; // CS
  GPIO056_PIN_CONTROL_REGISTER = 0x02003UL; // SCK  
  GPIO223_PIN_CONTROL_REGISTER = 0x02003UL;
  GPIO224_PIN_CONTROL_REGISTER = 0x02003UL;
  GPIO016_PIN_CONTROL_REGISTER = 0x10243UL;
  GPIO227_PIN_CONTROL_REGISTER = 0x10243UL;
  QQMSPI_MODE |= 0x2;                       // Reset the block
  wait_us(10);
  QQMSPI_MODE = 0x1;                        // See datasheet - SPI speed settings, ...
  QQMSPI_MODE = 0x300001;                   // See datasheet - SPI speed settings, ...
  QQMSPI_CONTROL = 0x20644;                 // See datasheet - SPI speed settings, ...
}


void spiDisable(void) {
  GPIO055_PIN_CONTROL_REGISTER = 0x40UL;
  GPIO056_PIN_CONTROL_REGISTER = 0x40UL;
  GPIO223_PIN_CONTROL_REGISTER = 0x40UL;
  GPIO224_PIN_CONTROL_REGISTER = 0x40UL;
  GPIO016_PIN_CONTROL_REGISTER = 0x40UL;
  GPIO227_PIN_CONTROL_REGISTER = 0x40UL;

  QQMSPI_MODE |= 0x2; //reset
  QQMSPI_MODE = 0x0;
}


uint8_t SPITransfer(uint8_t byte) {
  /* Transfer is currently executing.. */
  while((QQMSPI_STATUS & (1UL<<16))); 
  QQMSPI_TRANSMIT_BUFFER = byte;
  /* Handling some registers -> check datasheet*/
  QQMSPI_STATUS = 0xFFFFFFFF;
  QQMSPI_EXECUTE |= 0x1;
  while(!(QQMSPI_STATUS & 0x1));
  uint8_t received = QQMSPI_RECEIVE_BUFFER;
  /* Clear buffers */
  QQMSPI_EXECUTE |= 0x4;
  return received;
}


void resetFlash(void) {
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_RESET_ENABLE);
  flashCS(FLASH_DESELECT);
  wait(2);
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_RESET);
  flashCS(FLASH_DESELECT);
  wait(2);
}


void globalUnlockFlash() {
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_RESET_ENABLE);
  flashCS(FLASH_DESELECT);
  wait(2);
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_GLOBAL_BLOCK_PROTECTION_UNLOCK);
  flashCS(FLASH_DESELECT);
}


void unlockFlash() {
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_WRITE_ENABLE);
  flashCS(FLASH_DESELECT);
}


void flashCS(bool flag) {
  CS_MEMORY_OUTPUT_BIT = (flag) ?  0: 1;
}


void eraseSector(uint32_t addr) {
  resetFlash();
  while((readStatusRegister() & FLASH_IS_BUSY)); 
  CS_MEMORY_OUTPUT_BIT = 0;
  SPITransfer(FLASH_WRITE_ENABLE);
  CS_MEMORY_OUTPUT_BIT = 1;
  wait_us(10);
  CS_MEMORY_OUTPUT_BIT = 0;
  SPITransfer(0x20);
  SPITransfer((addr & 0x00FF0000) >> 16);
  SPITransfer((addr & 0x0000FF00) >>  8);
  SPITransfer( addr & 0x000000FF);
  
  CS_MEMORY_OUTPUT_BIT = 1;
  wait_us(100);
  while((readStatusRegister() & FLASH_IS_BUSY));
}


bool saveData(uint8_t * array, uint32_t addr) {
  globalUnlockFlash();
  eraseSector(addr);

  for(uint16_t i = 0; i<16; i++) {
    unlockFlash();
    CS_MEMORY_OUTPUT_BIT = 0;

    SPITransfer(0x02);
    SPITransfer((addr & 0x00FF0000) >> 16);
    SPITransfer((addr & 0x0000FF00) >>  8);
    SPITransfer( addr & 0x000000FF);

    for(uint16_t j = 0; j < 256; j++) {
      SPITransfer(array[i*256+j]);
    }
    CS_MEMORY_OUTPUT_BIT = 1;

    while((readStatusRegister() & FLASH_IS_BUSY));
    addr = addr + 256;
  }
  resetFlash();
  flashCS(FLASH_SELECT);
  while((readStatusRegister() & FLASH_IS_BUSY));
  flashCS(FLASH_SELECT);

  /* check */
  uint8_t temp[4096];
  readData(temp, addr - 4096);
  for(int i = 0; i<4096;i++) {
    if(temp[i] != array[i]) { //fixme compare function
      return true;
    }
  }
  return false; 
}


bool readData(uint8_t * array, uint32_t addr) {
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_WRITE_ENABLE);
  flashCS(FLASH_DESELECT);
  //wait(1);//comment out? fixme
  while((readStatusRegister() & FLASH_IS_BUSY));
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_READ_MEMORY);
  SPITransfer((addr & 0x00FF0000) >> 16);
  SPITransfer((addr & 0x0000FF00) >>  8);
  SPITransfer( addr & 0x000000FF);
  for(uint16_t i = 0; i<AES_BLOCK_SIZE; i++) {
    array[i] = SPITransfer(0x00);
  }
  flashCS(FLASH_DESELECT);
  return true;
}


void readJedecID(void) {
  uint8_t receive[3];
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_WRITE_ENABLE);
  CS_MEMORY_OUTPUT_BIT = 1;
  //wait(1);
  CS_MEMORY_OUTPUT_BIT = 0;
  SPITransfer(0x9F);
  receive[0] = SPITransfer(0x00);
  receive[1] = SPITransfer(0x00);
  receive[2] = SPITransfer(0x00);
  flashCS(FLASH_DESELECT);
  uartSend("JEDEC: \r\n");
  for(uint8_t i = 0; i<3; i++)
    uartSendChar_to_hex(receive[i]);
  uartSend("\r\n");
}


uint8_t readStatusRegister(void) {
  flashCS(FLASH_SELECT);
  SPITransfer(FLASH_READ_STATUS);
  uint8_t response = SPITransfer(0x00);
  flashCS(FLASH_DESELECT);//CS_MEMORY_OUTPUT_BIT = 1;
  return response;
}


/*
void dumpStorage(void) {
  char helper[4];
  uartSend("\r\n\r\n### XMPP STORAGE DEBUG DUMP ###\r\n");
  for(int16_t i = 0; i<16; i++) {
    uartSend("Position ");itoaa(i+1, helper);uartSend(helper);uartSend(":  ");
    uartSend((char*)xmpp_data[i].jid);
    uartSend("\r\n         ");
    uartSend(tohexLE((uint8_t*)xmpp_data[i].jid+32, 32));
    uartSend("\r\n         ");
    uartSend(tohexLE((uint8_t*)xmpp_data[i].metadata, 32));
    uartSend("\r\nPubkey:  ");
    uartSend(tohexLE((uint8_t*)xmpp_data[i].pubkey, 32));
    uartSend("\r\n");
  }
}
*/

