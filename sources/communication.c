/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

/* Two includes from nanopb submodule */
#include <pb_encode.h>
#include <pb_decode.h>

#include "communication.h"
#include "system-messages.pb.h"
#include "monero.pb.h"
#include "crypto.h"
#include "util.h"



uint8_t nanopb_buffer_out[BUFFER_OUT_SIZE]; //FIXME sizes :O) ?
uint8_t nanopb_buffer_in[BUFFER_IN_SIZE]; 



bool receiveMessage(const uint8_t id,  const pb_msgdesc_t *fields, void *msg, const uint32_t timeout) {

  uint8_t header[4];
  uint32_t proto_len = 0;
  uint32_t wait = 0;

  // Reading the first four bytes (header)
  for(uint8_t i = 0; i<4; i++) {
    while(!(UART_LINE_STATUS_REGISTER & 0x01) && (wait<timeout))  //DATA READY
      wait++;
    header[i] = UART_RECEIVE_BUFFER_REGISTER;
  }

  // Was timeout triggered?
  if(timeout == wait) {
    errorMessage("Timeout triggered");
    return 1;
  }

  // Check message's ID from header
  if((header[0] != START_TRANSMISSION_BYTE) || (header[1] != id)) {
    errorMessage("Received message formatting error? ID, START TRANSMISSION BYTE ...");
    return 1;
  }

  // Getting the length of a message
  proto_len = ((header[2] << 8 | header[3]));
  // Adding a space for checksum
  proto_len = proto_len + 8; 

  if(proto_len > BUFFER_IN_SIZE) {
  errorMessage("Incomming message is too long");
    return 1;
  }

  // Receiving the payload of a known length
  uint16_t i = 0;
  while(i<proto_len) {
    while(!(UART_LINE_STATUS_REGISTER & 0x01))  //DATA READY
      asm("mov r0,r0");//DO NOTHING
    nanopb_buffer_in[i] = UART_RECEIVE_BUFFER_REGISTER;
    i++;
  }

  /// checksum here ///

  pb_istream_t istream = pb_istream_from_buffer(nanopb_buffer_in, proto_len - 8);

  if(!pb_decode(&istream, fields, msg)) {
    errorMessage("Decoding msg_in");
    return 1;
  } 
  return 0;  
}


bool sendMessage(const uint8_t id, const pb_msgdesc_t *fields, void *msg) {

  pb_ostream_t ostream = pb_ostream_from_buffer(nanopb_buffer_out, sizeof(nanopb_buffer_out));

  if(!pb_encode(&ostream, fields, msg)) {
    errorMessage("encoding msg_out");
    return 1;
  }

  // Prepare checksum here ///

  uartSendByte(START_TRANSMISSION_BYTE);
  uartSendByte(id);
  uartSendByte((ostream.bytes_written>>8) & 0xFF);
  uartSendByte(ostream.bytes_written & 0xFF);
  for(uint16_t i = 0; (i<(uint16_t)ostream.bytes_written && i<BUFFER_OUT_SIZE); i++)
    uartSendByte(nanopb_buffer_out[i]);

  // Send checksum bytes
  for(uint16_t i = 0; i<8; i++)
    uartSendByte(0xAA);

  return 0;
}


bool debug(char message[]) {
  uint16_t length = strlen(message);

  if(length>255) {
    errorMessage("Debug message is too long!");
    return 1;
  }

  OutDebug msg_out = {0};

  memcpy(msg_out.message, message, length);

  if(sendMessage(OUT_DEBUG, OutDebug_fields, &msg_out)) {
    errorMessage("debug sending out");
    return 1;
  }

  return 0;
}

