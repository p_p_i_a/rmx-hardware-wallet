/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "CEC1702_defines.h"
#include "mec2016_rom_api.h"
#include "util.h"
#include "buttons.h"
#include "crypto.h"
#include "uart.h"
#include "rng.h"
#include "sha3.h"

union random_number {
   uint32_t rn[4];
   char chars[16];
} data;


void rngCEC1702chars(void) {
  fillBuf(0x0000);
  writeText(0,10, "CEC1702's Internal Random Number Generator\r\nspeed: 115200 baud\r\nParity:  none\r\nbits:  8\r\nstopbits:  1\r\nflow control:  none\r\n \r\nOK to exit", 0xffff);
  writeBuf();
  while((2 != scanAnyKey())){
    rng_get_random_blocking((uint32_t*)data.rn, 4);
    for(uint8_t i = 0; i<16; i++){
      uartSendChar(data.chars[i]);
    }
  }
  wait(200);
}


void rngCEC1702hexa(void) {
  fillBuf(0x0000);
  writeText(0,10, "CEC1702's Internal Random Number Generator\r\nspeed: 115200 baud\r\nParity:  none\r\nbits:  8\r\nstopbits:  1\r\nflow control:  none\r\n \r\nOK to exit", 0xffff);
  writeBuf();
  while((2 != scanAnyKey())){
    rng_get_random_blocking((uint32_t*)data.rn, 4);
    for(uint8_t i = 0; i<16; i++){
      uartSendChar_to_hex(data.chars[i]);
    }
  }
  wait(200);
}


/* EXPERIMENTAL EXTERNAL RNG */
#define SWEN1   (*((volatile uint32_t*) ((((((GPIO113_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define SWEN2   (*((volatile uint32_t*) ((((((GPIO107_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define COMP1   (*((volatile uint32_t*) ((((((GPIO120_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define COMP2   (*((volatile uint32_t*) ((((((GPIO112_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define DYLAJ 10

void rngExternalInit(void) {
  GPIO113_PIN_CONTROL_REGISTER = 0x10241UL; // SWEN1 (OUTPUT) GPIO113
  GPIO107_PIN_CONTROL_REGISTER = 0x10241UL; // SWEN2 (OUTPUT) GPIO107
  GPIO120_PIN_CONTROL_REGISTER = 0x40UL;    // COMP1 (INPUT)  GPIO120
  GPIO112_PIN_CONTROL_REGISTER = 0x40UL;    // COMP2 (INPUT)  GPIO112

  SWEN1 = 0;
  SWEN2 = 0;

  fillBuf(0x0000);
  writeText(0,10, "Modular entropy multiplier\r\nspeed: 115200 baud\r\nParity:  none\r\nbits:  8\r\nstopbits:  1\r\nflow control:  none\r\n \r\nOK to exit", 0xffff);
  writeBuf();
  uint8_t bit1, bit2, result;
  while((2 != scanAnyKey())){   
    for(int i = 0; i<4;i++) {
      SWEN2 = 1;
      wait_us(DYLAJ);
      bit2 = COMP2;
      SWEN2 = 0;
      wait_us(DYLAJ);
      SWEN1 = 1;
      wait_us(DYLAJ);
      bit1 = COMP1;
      SWEN1 = 0;
  
      bit1 = bit1<<1;
      bit2 = bit1 | bit2;

      result = result << 2;
      result = result | bit2;
    }
    uartSendChar_to_hex(result);
  }
  wait(200);
}




void rng_get_random_blocking(uint32_t *buffer, uint8_t buffer_length) {
  while (api_rng_get_num_random_words() < buffer_length);
  api_rng_get_random_words(buffer, buffer_length);
}

/** Random numbers from internal TRNG **/
void generateRandom(uint8_t *seed){
    uint8_t random[32];
    rng_get_random_blocking((uint32_t*) random, KEYSIZE/4);
    keccak_256(random, KEYSIZE, random);
    memcpy(seed, random, 32);
    memzero(random, 32);
}












