/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SPI_DISPLAY_H
#define SPI_DISPLAY_H

#include "stdint.h"
#include "MCHP_MEC170x.h"
#include "util.h"



// BIT-BANG
#define CS_DISPLAY_OUTPUT_BIT          (*((volatile uint32_t*) ((((((GPIO201_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define DC_OUTPUT_BIT          (*((volatile uint32_t*) ((((((GPIO204_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define OLED_RESET_OUTPUT_BIT  (*((volatile uint32_t*) ((((((GPIO202_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define OLED_LDO_EN_OUTPUT_BIT (*((volatile uint32_t*) ((((((GPIO203_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))

// SPI HW BLOCK DEFINES
#define SPI_ENABLE_REGISTER               (*((volatile unsigned int*) 0x40009400))
#define SPI_CONTROL_REGISTER              (*((volatile unsigned int*) 0x40009404))
#define SPI_STATUS_REGISTER               (*((volatile unsigned int*) 0x40009408))
#define SPI_TX_DATA_REGISTER              (*((volatile unsigned int*) 0x4000940C))
#define SPI_RX_DATA_REGISTER              (*((volatile unsigned int*) 0x40009410))
#define SPI_CLOCK_CONTROL_REGISTER        (*((volatile unsigned int*) 0x40009414))
#define SPI_CLOCK_GENERATOR_REGISTER      (*((volatile unsigned int*) 0x40009418))

/** SPI initialization **/
void spiInit(void);


/** SPI pin handling **/
void csLow (void);


/** SPI pin handling **/
void csHigh (void);


/** SPI pin handling **/
void dcLow (void);


/** SPI pin handling **/
void dcHigh (void);


/** Enabling the 12V LDO - ok, technically it's not a "LDO" but a LED driver **/
void ldoEnable (void);


/** Disabling the 12V LDO - ok, technically it's not a "LDO" but a LED driver **/
void ldoDisable (void);


/** Reseting the SPI display interface **/
void resetLow (void);


/** Reset high **/
void resetHigh(void);


/** Write a byte to SPI **/
void writeSPI(uint8_t c);


/** Write a "data" byte into display **/
void writeData(uint8_t c);


/** Write a "command" byte into display **/
void writeCmd(uint8_t c);


#endif
