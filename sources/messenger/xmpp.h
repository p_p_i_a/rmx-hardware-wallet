/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */
#include "stdint.h"
#include "stdbool.h"

#ifndef MESSAGING_H
#define MESSAGING_H

typedef struct{
  uint8_t jid[64];
  uint32_t timestamp;
  uint8_t message[256];
  //uint8_t aes_IV[32];
  uint8_t metadata[32];
  uint8_t checksum[32];
}  PAYLOAD;


//void XMPPSave(void);
void XMPPLogin(void);
void XMPPQueryUnread(void);

void XMPPDownloadMessage(uint8_t index);
void decryptMessage(void);

void encryptMessage(void);
void XMPPSendMessage(void);

void XMPPAddContact(void);
//bool XMPPReceiveAll(void);




void XMPPMessageAck (uint8_t ack);




void payloadZeroes(PAYLOAD* Structure);




void updateUnread(char str[], uint8_t unread);
void XMPPReadMessagesMenu(void);


/////////////////////////////////////////////////////////////////
void XMPPLoginSetup(void);
void deleteContact(void);
uint8_t browseContacts(void);
void saveContact(uint8_t data[]);





#endif
