/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */


#include "display.h"
#include "string.h"

/* This buffer represents 32768 Bytes to be displayed on the display. Each pixel contains 2Bytes of RGB information  */
static uint8_t _buffer[OLED_BUFSIZE];


/* Setting function for 128x128 RGB OLED display*/
void oledInit(void) {
  /* initial sequence */
  spiInit();
  oledReset();
  wait(1);
  ldoEnable();
  wait(30);
  csLow();
  wait(3);

  //TODO fosc
  writeCmd(SSD1351_SET_COMMANDLOCK);
  writeData(0x12);     
  writeCmd(SSD1351_SET_COMMANDLOCK);
  writeData(0xB1);
  writeCmd(SSD1351_SLEEP_ON);
  writeCmd(SSD1351_SET_CLOCK);
  writeData(0xF1);
  // use full screen beginning at 0
  writeCmd(SSD1351_SET_MUXRATIO);
  writeData(127);
  writeCmd(SSD1351_SET_STARTLINE);
  writeData(0x00);
  writeCmd(SSD1351_SET_OFFSET);
  writeData(0x00);
  //TODO for us 65k would probably be enough and can be done wth 2 bytes instead of 3
  writeCmd(SSD1351_SET_REMAP);
  writeData(0x66);
  writeCmd(SSD1351_SET_GPIO);
  writeData(0x00);
  // 8-bit parallel interface
  // Enable the internal Vdd regulator
  writeCmd(SSD1351_FUNCTIONSELECT);
  writeData(0x01);
  writeCmd(SSD1351_SET_RESETPRECHARGE);
  writeData(0x32);
  writeCmd(SSD1351_SET_VCOMH);
  writeData(0x05);
  writeCmd(SSD1351_MODE_NORMAL);
  writeCmd(SSD1351_SET_CONTRASTABC);
  writeData(0xC8);
  writeData(0x80);
  writeData(0xC8);
  writeCmd(SSD1351_SET_CONTRASTMASTER);
  writeData(0x0F);
  writeCmd(SSD1351_SET_VSL);
  writeData(0xA0);
  writeData(0xB5);
  writeData(0x55);
  writeCmd(SSD1351_SET_PRECHARGE2);
  writeData(0x01);
  writeCmd(SSD1351_SLEEP_OFF);
  csHigh();
}


/* RESET pin handling, OLED display */
void oledReset (void) {
  wait(1);
  resetLow();
  wait(1);
  resetHigh();
  wait(1);
}


void fillBuf(uint16_t fillcolor) {
  for (uint16_t i=0; i < 128*128; i++) {
    _buffer[2*i] = fillcolor >> 8;
    _buffer[2*i+1] = fillcolor;
  }
}


void writeBuf(void) {
  csLow();
  writeCmd(SSD1351_SET_COLUMN);
  writeData(0);
  writeData(127);
  writeCmd(SSD1351_SET_ROW);
  writeData(0);
  writeData(127);
  writeCmd(SSD1351_WRITERAM);
  for (uint16_t i=0; i < 32768; i++) {
    writeData(_buffer[i]);
  }
  csHigh();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


void drawPixel(uint8_t x, uint8_t y, uint16_t color) {
 /* if ((x < 0) || (y < 0) || (x >= OLED_WIDTH) || (y >= OLED_HEIGHT)) {
    return;
  }*/
  _buffer[y*128*2 + x*2    ] = (color >> 8) & 0xFF;
  _buffer[y*128*2 + x*2 + 1] = color & 0xFF;
}


void drawLine(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color) {
  bool steep = (abs(y2-y1) > abs(x2-x1));
  int16_t temp;
  if (steep) {
    temp = x1;
    x1 = y1;
    y1 = temp;
    
    temp = x2;
    x2 = y2;
    y2 = temp;
  }
  if (x1 > x2) {
    temp = x1;
    x1 = x2;
    x2 = temp;
    
    temp = y1;
    y1 = y2;
    y2 = temp;;
  }
  int16_t dx = x2-x1;
  int16_t dy = abs(y2-y1);
  int16_t error = dx / 2;
  int16_t ystep = (y1 < y2) ? 1 : -1;
  int16_t y = y1;
  int16_t maxX = x2;
    
  for (int16_t x = x1; x<maxX; x++) {
    if ((x >= 0) & (x<128) & (y >= 0) & (y<128)) {
      if (steep) {
        _buffer[y*2 + x*256] = color >> 8;
        _buffer[y*2 + x*256 + 1] = color;
      } else {
        _buffer[x*2 + y*256] = color >> 8;
        _buffer[x*2 + y*256 + 1] = color;
      }
    }
    error -= dy;
    if (error < 0) {
      y += ystep;
      error += dx;
    }
  }
}


void fillRect(int16_t x, int16_t y, uint16_t w, uint16_t h, uint16_t color) {
  int16_t start = y*256 + x*2;
  uint16_t xs = 0;
  uint16_t xe = 0;
  uint16_t ys = 0;
  uint16_t ye = 0;
    
  if (x < 0) {
    xs = -x;
  }
  if (y < 0) {
    ys = -y;
  }
  if (x+w > 127) {
    xe = x+w-128;
  }
  if (y+h > 127) {
    ye = y+h-128;
  }
  
  for (uint16_t j=0+ys; j<h-ye; j++) {
    for (uint16_t i=0+xs; i<w-xe; i++) {
      _buffer[start +j*256 + i*2] = color >> 8;
      _buffer[start +j*256 + i*2 +1] = color;
    }
  }
}


void fillCircle(int16_t x0, int16_t y0, uint16_t radius, uint16_t color) {
  int16_t x = radius;
  int16_t y = 0;
  int16_t err = 0;
    
  while (x >= y) {
    drawLine(x0,y0+y,x0+x,y0+y,color);
    drawLine(x0,y0+y,x0-x,y0+y,color);
    drawLine(x0,y0-y,x0+x,y0-y,color);
    drawLine(x0,y0-y,x0-x,y0-y,color);
        
    drawLine(x0,y0+x,x0+y,y0+x,color);
    drawLine(x0,y0+x,x0-y,y0+x,color);
    drawLine(x0,y0-x,x0+y,y0-x,color);
    drawLine(x0,y0-x,x0-y,y0-x,color);
        
    y += 1;
    if (err <= 0) {
      err += 2*y + 1;
    }
    if (err > 0) {
      x -= 1;
      err -= 2*x + 1;
    }
  }
}


void progressBarCircle(int8_t progress, uint16_t color, bool dir) {
  if(progress>32)
    progress = 31;
  if(progress<0)
    progress = 0;
 
//  int32_t sin[16] = {   0, 383, 707, 924, 1000, 924, 707, 383, 0, -383, -707, -924, -1000, -924, -707, -383};
//  int32_t cos[16] = {1000, 924, 707, 383, 0, -383, -707, -924, -1000, -924, -707, -383, 0, 383, 707, 924};
  int32_t sin[33] = {   0, 198, 383, 548, 707, 833, 924, 980, 1000, 980, 924, 833, 707, 548, 383, 198,    0,-198,-383,-548,-707,-833,-924,-980,-1000,-980,-924,-833,-707,-548,-383,-198,    0};
  int32_t cos[33] = {1000, 980, 924, 833, 707, 548, 383, 198,    0,-198,-383,-548,-707,-833,-924,-980,-1000,-980,-924,-833,-707,-548,-383,-198,    0, 198, 383, 548, 707, 833, 924, 980, 1000};
  int16_t x, y;
  //for(uint8_t k = 0; k<2;k++) {
  uint8_t length = 8;//+k;
  for(uint8_t angle = 0; angle<progress; angle++) {

    x = length*cos[angle];
    x = x/210;
    y = length*sin[angle];
    y = y/210;
    //color = (key[i]<<8)|key[i];
    if(dir)
      fillCircle(63+x, 63+y, 1, color);
    else
      fillCircle(63-x, 63-y, 1, color);
  } 
//}
}

/*
void progressBarCircle2(int8_t progress, uint16_t color, bool dir) {
  if(progress>32)
    progress = 31;
  if(progress<0)
    progress = 0;

  int32_t sin[33] = {   0, 198, 383, 548, 707, 833, 924, 980, 1000, 980, 924, 833, 707, 548, 383, 198,    0,-198,-383,-548,-707,-833,-924,-980,-1000,-980,-924,-833,-707,-548,-383,-198,    0};
  int32_t cos[33] = {1000, 980, 924, 833, 707, 548, 383, 198,    0,-198,-383,-548,-707,-833,-924,-980,-1000,-980,-924,-833,-707,-548,-383,-198,    0, 198, 383, 548, 707, 833, 924, 980, 1000};
  int16_t x1, y1, x2, y2;

  uint32_t length = 10;//+k;
  for(uint8_t angle = 0; angle<progress; angle++) {

    x1 = length*cos[angle];
    x1 = x1/210;
    y1 = length*sin[angle];
    y1 = y1/210;

    x2 = length*cos[angle+1];
    x2 = x2/210;
    y2 = length*sin[angle+1];
    y2 = y2/210;

    drawLine(61+x1, 61+y1, 63+x2+2, 63+y2+2, color);
  } 
}
*/	


/* fixme add different font? */
void drawChar(char c, int16_t x, int16_t y, uint16_t color, uint8_t zoom) {
  /*
  if (x <= -char_width) {
    return;
  }
  */
  int char_width = fontCharWidth(c);
  const uint8_t *char_data = fontCharData(c);
  
  for (int xo = 0; xo < char_width; xo++) {
    for (int yo = 0; yo < FONT_HEIGHT; yo++) {
      if (char_data[xo] & (1 << (FONT_HEIGHT - 1 - yo))) {
        if(zoom == 1) {
          drawPixel(x + xo, y + yo, color);
        } else {
            fillRect(x + xo*zoom, y + yo*zoom, zoom, zoom, color);
          }
      }
    }
  }
}





#define CR  0x0D 
#define LF  0x0A

void writeText(uint8_t  x, uint8_t y, char text[], uint16_t color) {
  if (!text)
    return;

  uint16_t move = 0;
  uint8_t zoom = 1;

  for (; *text; text++) {
    if((*text == CR ) && (*(text+1) == LF)) {
      x = 0;
      y = y+10;
      text = text+2;
    } 
    if(*text != ' ' ) {
      uint16_t char_pos = 0;
      uint16_t wordlen_x = 0;
      while((*(text+char_pos) != ' ') && (*(text+char_pos) != '\0')) {
        wordlen_x = wordlen_x + (fontCharWidth(*text+char_pos)*zoom+zoom);
        char_pos++;
      }
      if ((wordlen_x + x) > 127) {//?? 127 causes troubles sometime, maybe check font bitarray?
        y +=10;
        x = 0; 
      }
    } 

    drawChar(*text, x, y, color, zoom);
    move = (fontCharWidth(*text)*zoom+zoom);
    x += move;
  }
}








void drawStringCenteredZoom(int y, const char* text, uint16_t color, uint8_t zoom) {
  /*  
  if (!text)
    return;
  */
  //check the length
  int x = 0;
  uint8_t pointer_reload = 0;

  for (; *text; text++) {
    x += (fontCharWidth(*text)*zoom+zoom);
    pointer_reload++;
  }
  x = (128-x)/2;
  text = text - pointer_reload;
  for (; *text; text++) {
    drawChar(*text,x , y, color, zoom);
    x += (fontCharWidth(*text)*zoom+zoom);
  }
}

/*used for xmpp display message*/  
//FIXME length checking
void drawMessage(char* text, uint16_t color, uint8_t zoom) {
  /*  
  if (!text)
    return;
  */

  uint8_t x = 0;
  uint8_t y = 10;
  uint16_t chars_offset = 0;
  uint8_t move = 0;

  for (; *text; text++) {
    
    drawChar(*text, x, y, color, zoom);
    move = (fontCharWidth(*text)*zoom+zoom);
    x += move;
    chars_offset += move;

    if((chars_offset + (fontCharWidth(*text+1)*zoom+zoom))>127) {
      chars_offset = 0;
      y +=10;
      x = 0;
    }
  }
}


void animText(int x, int y, const char* text, uint16_t color, uint8_t zoom) {
  /*  
  if (!text)
    return;
  */
  for (; *text; text++) {
    drawChar(*text, x, y, color, zoom);
    x += (fontCharWidth(*text)*zoom+zoom);
    if(!(x%2))
      writeBuf();
  }
  writeBuf();
}


void drawStringZoom(int x, int y, const char* text, uint16_t color, uint8_t zoom) {
  /*  
  if (!text)
    return;
  */
  for (; *text; text++) {
    drawChar(*text, x, y, color, zoom);
    x += (fontCharWidth(*text)*zoom+zoom);
  }
}










/* LEFTRIGHT 0
 * YESNO     1
 * OK        2
*/
void placeNavigation(uint8_t type) {
  if(type == 0) {
    drawChar('<', 0, 120, 0xffff, 1);
    drawStringZoom(53, 120, "[OK]", 0xffff, 1);
    drawChar('>', 122, 120, 0xffff, 1);
  }
  else if (type == 1) {
    drawStringZoom(0, 120, "YES", 0xffff, 1);
    drawStringZoom(53, 120, "[OK]", 0xffff, 1);
    drawStringZoom(115, 120, "NO", 0xffff, 1);
  }
  else if (type == 2) {
    drawStringZoom(53, 120, "[OK]", 0xffff, 1);
  }
}


void placePictureWithAnimtext(uint8_t img_x, uint8_t img_y, const BITMASK *img, int text_y, const char* text, uint16_t color, uint8_t zoom) {
  uint8_t bytes = img->width/8;
  uint8_t byte;
  int16_t startpoint = (img_y)*128*2 + img_x*2; //top left point where img is placed
  fillBuf(0x0000);

  placeNavigation(LEFTRIGHT);
  writeBuf();

  for (uint8_t i=0; i < img->height; i++) { // for each line in image
    for (uint8_t k=0; k<bytes; k++) {       // for each byte
      byte = img->data[i*bytes+k];
      for (uint8_t j=0; j<8; j++) {
        if((byte) & 0b10000000) { 
          _buffer[startpoint + i*128*2 + k*8*2 + j*2  ]     = (img->color >> 8) & 0xFF;
          _buffer[startpoint + i*128*2 + k*8*2 + j*2+1]     = img->color & 0xFF;  
        }
        byte = byte << 1;
      }
    }
  }
  /*  
  if (!text)
    return;
  */
/*
  for (; *text; text++) {
    //length++;
    drawChar(*text, text_x, text_y, color, zoom);
    text_x += (fontCharWidth(*text)*zoom+zoom);
    if(!(text_x%2))
      writeBuf();
  }
*/
  drawStringCenteredZoom(text_y, text, color, zoom);
  writeBuf();
}


void placeImage( uint8_t x, uint8_t y, const BITMASK *img) {
  uint8_t bytes = img->width/8;
  uint8_t byte;
  int16_t startpoint = (y)*128*2 + x*2;     //top left point where img is placed

  for (uint8_t i=0; i < img->height; i++) { // for each line in image
    for (uint8_t k=0; k<bytes; k++) {       // for each byte
      byte = img->data[i*bytes+k];
      for (uint8_t j=0; j<8; j++) {
        if((byte) & 0b10000000) { 
          _buffer[startpoint + i*128*2 + k*8*2 + j*2  ]     = (img->color >> 8) & 0xFF;
          _buffer[startpoint + i*128*2 + k*8*2 + j*2+1]     = img->color & 0xFF;  
        }
        byte = byte << 1;
      }
    }
  }
}



