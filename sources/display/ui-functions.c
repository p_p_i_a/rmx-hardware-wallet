/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui-functions.h"
#include "util.h"
#include "display.h"
#include "buttons.h"
#include "monero_sync.h"

#include "benchmark.h"
#include "communication.h"
#include "init.h"
#include "atecc508a.h"
#include "efuse.h"
#include "xmpp.h"
#include "login.h"

#include "mec2016_rom_api.h"
#include "rng.h"
#include "flash_memory.h"
#include "storage.h"



void showMenu(const MENU *menu) {
  int focus = 0;
  int exit = menu->lines-1;
  while (1) {
    //wait(0);
    int8_t pressed = scanAnyKey();
    debounce();
    switch(pressed) {
      case 1:
        focus--; 
        break;
      case 2:
        if(focus == exit) {
         return;
        }

        if(focus == 0) {
          menu->functions[0]();
        } 
        if(focus == 1) {
          menu->functions[1]();
        }
        if(focus == 2) {
          menu->functions[2]();
        }
        if(focus == 3) {
          menu->functions[3]();
        }
        if(focus == 4) {
          menu->functions[4]();
        }
        if(focus == 5) {
          menu->functions[5]();
        }
        if(focus == 6) {
          menu->functions[6]();
        }
        if(focus == 7) {
          menu->functions[7]();
        }
        if(focus == 8) {
          menu->functions[8]();
        }
        if(focus == 9) {
          menu->functions[9]();
        }
        if(focus == 10) {
          menu->functions[10]();
        }
        if(focus == 11) {
          menu->functions[11]();
        }
        break;
      case 3:
        focus++; 
        break;
      default:
        break;
    }
    if (focus > menu->lines-1)
      focus = 0;
    if (focus < 0)
      focus = menu->lines-1;
    fillBuf(0x0000);    
    for(uint8_t i = 0; i<menu->lines; i++) {
      drawStringZoom(5, 4+(10*i), &menu->options[i][0], WHITE, 1);
      if(focus == i) {
        fillRect(3, (2+(10*i)), 110, 12, RED);
        drawStringZoom(5, 4+(10*i), &menu->options[i][0], WHITE, 1);
      }
      if(i == exit) {
        drawStringZoom(5, 4+(10*i), "EXIT", WHITE, 1);
      }
    }
    writeBuf();
  }
}



void uxLogoWelcome(void) {
  fillBuf(0x0000);
  animText(35, 92, "RMX WALLET", 0xffff, 1);
  wait(700);
  placeImage(32,15, &rmx_64x64);
  writeBuf();
}


void uxMessage(const char *message, uint8_t offset) {
  fillBuf(0x0000);
  drawStringCenteredZoom(offset, message, WHITE, 1);
  writeBuf();
  wait(400);
}




void errorMessage(char msg[]) {
  debug(msg);
  fillBuf(0x0000);
  drawStringZoom(0, 0, "Error message:", RED, 1);
  writeText(0, 20, msg, 0xffff);

  placeNavigation(2);
  writeBuf();

  waitForOK();
  fillBuf(0x0000);
  writeBuf();
  wait(100);
}




//debug
void showByte(uint8_t number) {
  //fillBuf(0x0000);
  char helper[5];
  itoaa(number, helper);
  drawStringCenteredZoom(77, helper, WHITE, 1);
  //writeBuf();
  //wait(400);
}


void placeholder(void) {
  fillBuf(0x0000);
  animText(5, 15, "Sorry, nothing", 0xffff, 1);
  animText(5, 25, "is here yet!", 0xffff, 1);
  wait(100);
  animText(5, 95, "Maybe next time.", 0xffff, 1);
  wait(100);
  animText(5, 110, "OK to exit", 0xffff, 1);
  while((2 != scanAnyKey()))
    ;
  wait(100);
}



