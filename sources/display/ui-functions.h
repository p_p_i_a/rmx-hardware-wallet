/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UI_FUNCTIONS
#define UI_FUNCTIONS

#include "display.h"
#include "menus.h"

//http://www.barth-dev.de/online/rgb565-color-picker/
//monero orange ff6600 in 16bit is FB20? (checkme ?)
#define RED            0xF800
#define GREEN          0x07E0
#define BLUE           0x001F
#define YELLOW         0x20FB
#define LIGHT_BLUE     0x079F
#define MONERO_ORANGE  0xFB20
#define MONERO_GREY    0x4A69
#define WHITE          0xFFFF
#define BLACK          0x0000



void showMenu(const MENU *menu);

void uxLogoWelcome(void);

void uxMessage(const char *message, uint8_t offset);


void placeholder(void);






void showByte(uint8_t number);











void errorMessage(char msg[]);






#endif
