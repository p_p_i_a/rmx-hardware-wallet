/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MENUS
#define MENUS

#include "display.h"

//http://www.barth-dev.de/online/rgb565-color-picker/
//monero orange ff6600 in 16bit is FB20? (checkme ?)
#define RED            0xF800
#define GREEN          0x07E0
#define BLUE           0x001F
#define YELLOW         0x20FB
#define LIGHT_BLUE     0x079F
#define MONERO_ORANGE  0xFB20
#define MONERO_GREY    0x4A69
#define WHITE          0xFFFF
#define BLACK          0x0000


typedef bool (*generic_menu_func)(void);

typedef struct{
  uint8_t lines;
  //int focus;
  const char options[11][18];
  generic_menu_func functions[12];
} MENU;

extern const MENU Experimental;
extern const MENU Rng;
extern const MENU Monero, Monero_setup;
extern const MENU Setup;
extern const MENU Xmpp;

//////////////
bool menuFunctionExperimental_0(void);
bool menuFunctionExperimental_1(void);
bool menuFunctionExperimental_2(void);
bool menuFunctionExperimental_3(void);
bool menuFunctionExperimental_4(void);
bool menuFunctionExperimental_5(void);
bool menuFunctionExperimental_6(void);
bool menuFunctionExperimental_7(void);
bool menuFunctionExperimental_8(void);
bool menuFunctionExperimental_9(void);
//////////////
bool menuFunctionRng_0(void);
bool menuFunctionRng_1(void);
bool menuFunctionRng_2(void);
//////////////
bool menuFunctionMonero_0(void);
bool menuFunctionMonero_1(void);
bool menuFunctionMonero_2(void);
bool menuFunctionMonero_3(void);
bool menuFunctionMonero_4(void);
bool menuFunctionMonero_5(void);
bool menuFunctionMonero_6(void);
bool menuFunctionMonero_7(void);
bool menuFunctionMonero_8(void);
//////////////
bool menuFunctionMonero_setup_0(void);
bool menuFunctionMonero_setup_1(void);
bool menuFunctionMonero_setup_2(void);
bool menuFunctionMonero_setup_3(void);
bool menuFunctionMonero_setup_4(void);
bool menuFunctionMonero_setup_5(void);
bool menuFunctionMonero_setup_6(void);
bool menuFunctionMonero_setup_7(void);
bool menuFunctionMonero_setup_8(void);
//////////////
bool menuFunctionMenu_0(void);
bool menuFunctionMenu_1(void);
bool menuFunctionMenu_2(void);
bool menuFunctionMenu_3(void);
bool menuFunctionMenu_4(void);
/////////////
bool menuFunctionXmpp_0(void);
bool menuFunctionXmpp_1(void);
bool menuFunctionXmpp_2(void);
bool menuFunctionXmpp_3(void);
bool menuFunctionXmpp_4(void);
bool menuFunctionXmpp_5(void);
bool menuFunctionXmpp_6(void);




















#endif
