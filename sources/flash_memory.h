/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _FLASH_MEMORY_H_
#define _FLASH_MEMORY_H_

#define   QQMSPI_MODE                  (*((volatile uint32_t*) 0x40005400))
#define   QQMSPI_CONTROL               (*((volatile uint32_t*) 0x40005404))
#define   QQMSPI_EXECUTE               (*((volatile uint32_t*) 0x40005408))
#define   QQMSPI_INTERFACE_CONTROL     (*((volatile uint32_t*) 0x4000540C))
#define   QQMSPI_STATUS                (*((volatile uint32_t*) 0x40005410))
#define   QQMSPI_BUFFER_COUNT_STATUS   (*((volatile uint32_t*) 0x40005414))
#define   QQMSPI_INTERRUPT_ENABLE      (*((volatile uint32_t*) 0x40005418))
#define   QQMSPI_BUFFER_COUNT_TRIGGER  (*((volatile uint32_t*) 0x4000541C))
#define   QQMSPI_TRANSMIT_BUFFER       (*((volatile uint8_t*)  0x40005420))
#define   QQMSPI_RECEIVE_BUFFER        (*((volatile uint8_t*)  0x40005424))
#define   QQMSPI_DESCRIPTION_BUFFER_0  (*((volatile uint32_t*) 0x40005430))
#define   QQMSPI_DESCRIPTION_BUFFER_1  (*((volatile uint32_t*) 0x40005434))
#define   QQMSPI_DESCRIPTION_BUFFER_2  (*((volatile uint32_t*) 0x40005438))
#define   QQMSPI_DESCRIPTION_BUFFER_3  (*((volatile uint32_t*) 0x4000543C))
#define   QQMSPI_DESCRIPTION_BUFFER_4  (*((volatile uint32_t*) 0x40005440))

#define CS_MEMORY_OUTPUT_BIT   (*((volatile uint8_t*) ((((((GPIO055_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))//manual SPI CS


#define AES_BLOCK_SIZE                  0x00001000
#define XMPP_STORAGE_SIZE               0x00001000
#define XMPP_MEMORY_ADDRESS             0x000FF000
#define MONERO_STORAGE_SIZE             0x00002000
#define MONERO_MEMORY_ADDRESS           0x00101000

/* chip select */
#define FLASH_SELECT                    1
#define FLASH_DESELECT                  0

/* flash commands */
#define FLASH_WRITE_ENABLE              0x06
#define FLASH_READ_STATUS               0x05
#define FLASH_IS_BUSY                   0x81
#define FLASH_READ_MEMORY               0x03
#define FLASH_RESET_ENABLE              0x66
#define FLASH_RESET                     0x99
#define FLASH_GLOBAL_BLOCK_PROTECTION_UNLOCK         0x98


/* 
 * SPI initialization based on datasheet:
 * Duplex mode, TX enable from buffer, dma disabled, RX enabled,
 * RX DMA disabled, CS UP at the end of TX, TX length in bytes, transfer length 1B??
*/
void spiEnable(void);


/* SPI disabling here, so the bus is free to boot from flash again */
void spiDisable(void);


/* Send byte, receive byte */
uint8_t SPITransfer(uint8_t byte);


/* Reset flash. You "should" send this prior to some flash commands */
void resetFlash(void);


/* You must to send this prior to some flash commands */
void unlockFlash(void);


/*
 * select and deselect CS pin on SPI bus 
 * #define SELECT     1
 * #define DESELECT   0
*/
void flashCS(bool flag);


/* Erases a sector in flash, because sector must be erased before writing. Call spiEnable() prior calling this function */
void eraseSector(uint32_t addr);


/* Erase whole memory. If used, wait around 70ms to finish, or check flash status register*/
void eraseMemory(void);


/* Send stored dataover UART, for debug*/
void dumpStorage(void);


/* Takes an array of data and saves it to some address */
bool saveData(uint8_t array[], uint32_t addr);


/* Reads an array from certain address */
bool readData(uint8_t array[], uint32_t addr);


/* Reads status register. Call spiEnable() prior calling this function */
uint8_t readStatusRegister(void);


/* Reads flash ID, call spiEnable() prior to calling this function */
void readJedecID(void);

/* Unlock the flash globally */
void globalUnlockFlash();


#endif
