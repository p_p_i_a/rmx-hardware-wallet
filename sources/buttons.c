/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <i_a@rmxwallet.io>
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

 
#include "buttons.h"
#include "CEC1702_defines.h"

// BIT BAND
#define ROW1_OUTPUT_BIT  (*((volatile uint32_t*) ((((((GPIO017_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define ROW2_OUTPUT_BIT  (*((volatile uint32_t*) ((((((GPIO040_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define ROW3_OUTPUT_BIT  (*((volatile uint32_t*) ((((((GPIO031_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define ROW4_OUTPUT_BIT  (*((volatile uint32_t*) ((((((GPIO026_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))
#define COL1_INPUT_BIT   (*((volatile uint32_t*) ((((((GPIO027_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define COL2_INPUT_BIT   (*((volatile uint32_t*) ((((((GPIO054_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define COL3_INPUT_BIT   (*((volatile uint32_t*) ((((((GPIO053_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))

void buttonsInit(void) {
  // GPIO register setting
  GPIO040_PIN_CONTROL_REGISTER = 0x10242UL; // ROW 1 (OUTPUT) GPIO040 
  GPIO017_PIN_CONTROL_REGISTER = 0x10242UL; // ROW 2 (OUTPUT) GPIO017
  GPIO031_PIN_CONTROL_REGISTER = 0x10242UL; // ROW 3 (OUTPUT) GPIO031
  GPIO026_PIN_CONTROL_REGISTER = 0x10242UL; // ROW 4 (OUTPUT) GPIO026
  GPIO054_PIN_CONTROL_REGISTER = 0x41UL;    // COL 1 (INPUT)  GPIO054
  GPIO053_PIN_CONTROL_REGISTER = 0x41UL;    // COL 2 (INPUT)  GPIO053
  GPIO027_PIN_CONTROL_REGISTER = 0x41UL;    // COL 3 (INPUT)  GPIO027

  GPIO040_PIN_CONTROL_2_REGISTER = 0b110000; // ROW 1 (OUTPUT) GPIO040 
  GPIO017_PIN_CONTROL_2_REGISTER = 0b110000; // ROW 2 (OUTPUT) GPIO017
  GPIO031_PIN_CONTROL_2_REGISTER = 0b110000; // ROW 3 (OUTPUT) GPIO031
  GPIO026_PIN_CONTROL_2_REGISTER = 0b110000; // ROW 4 (OUTPUT) GPIO026

  allRowsToOne();
}


void allRowsToZero(void) {
  ROW1_OUTPUT_BIT = 0;
  ROW2_OUTPUT_BIT = 0;
  ROW3_OUTPUT_BIT = 0;
  ROW4_OUTPUT_BIT = 0;
}


void allRowsToOne(void) {
  ROW1_OUTPUT_BIT = 1;
  ROW2_OUTPUT_BIT = 1;
  ROW3_OUTPUT_BIT = 1;
  ROW4_OUTPUT_BIT = 1;
}


void rowSelect(uint8_t row, bool select) {
  if (select) { 
    switch (row) { // Enable some row by setting it to 0
      case 0:
        ROW1_OUTPUT_BIT = 0;
        break;
      case 1:
        ROW2_OUTPUT_BIT = 0;
        break;
      case 2:
        ROW3_OUTPUT_BIT = 0;
        break;
      case 3:
        ROW4_OUTPUT_BIT = 0;
        break;
    }
  } else { // Disable some row by setting it to 1
    switch (row) {
      case 0:
        ROW1_OUTPUT_BIT = 1;
        break;
      case 1:
        ROW2_OUTPUT_BIT = 1;
        break;
      case 2:
        ROW3_OUTPUT_BIT = 1;
        break;
      case 3:
        ROW4_OUTPUT_BIT = 1;
        break;
    }
  }
}


bool readCol(uint8_t col) {
  switch (col) {
    case 0:
      return COL3_INPUT_BIT;
    case 1:
      return COL2_INPUT_BIT;
    case 2:
      return COL1_INPUT_BIT;
  }
  return false;
}


uint16_t scanMatrix(void) {
  uint16_t pressed = 0;
  uint16_t test = 1;
  for (int row = 0; row < 4; ++row) {
    rowSelect(row, true);
    for (int col = 0; col < 3; ++col) {
      if (!readCol(col)) {
        pressed |= test;
      }
      test <<= 1;
    }
    rowSelect(row, false);
  }
  return pressed;
}


int8_t scanAnyKey(void) {
  uint16_t pressed = scanMatrix();
  uint16_t test = 1;
  for (int i = 12; i > 0 ; --i) {
    if (pressed & test) return i;
    test <<= 1;
  }
  return -1;
}


#define DELAY 30
#define MESSAGE_SIZE  160 //fixme
uint16_t keyboard(char* message, uint16_t size, char navigation_string[]) {

  if(size <=0)
    size = 2;

  //char message[256];// = {' '};
  int index = -1;
  int new_pressed = 0;
  int last_pressed = 0;
  int n_button_n_pressed = 0;

  char button1[] = {'.',',','?',    '!','1','-','+'};
  char button2[] = {'a','b','c',    '2','A','B','C'};
  char button3[] = {'d','e','f',    '3','D','E','F'};
  char button4[] = {'g','h','i',    '4','G','H','I'};
  char button5[] = {'j','k','l',    '5','J','K','L'};
  char button6[] = {'m','n','o',    '6','M','N','O'};
  char button7[] = {'p','q','r','s','7','P','Q','R','S'};
  char button8[] = {'t','u','v',    '8','T','U','V'};
  char button9[] = {'w','x','y','z','9','W','X','Y','Z'};
  // Button 10 is DELETE
  char button11[] = {' ',':','#','*','0','/','@'};
  // Button 12 is SAVE END EXIT


  // Showing the navigation string:
  fillBuf(0x0000);
  drawStringZoom(0,0, navigation_string, 0xffff, 1);
  writeBuf();


  uint32_t button_move = 0;
  debounce();
  while(((new_pressed = scanAnyKey()) != 12) && (index <= size-2)) {
    if(new_pressed>0 && new_pressed<13) {
      if((new_pressed != last_pressed)) {//&& (index<message_length)
        n_button_n_pressed = 0;
        index ++;
      }
      // This is triggering the cursor to move to the next position
      if((new_pressed == last_pressed) && (button_move > 25000)) {
        n_button_n_pressed = 0;
        index ++;
        last_pressed = 0x20;
      }
      switch(new_pressed) {
        case 1:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button1[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;     
        case 2:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button2[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 3:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button3[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 4:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button4[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 5:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button5[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 6:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button6[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 7:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >9)
            n_button_n_pressed = 0;
          message[index] = (button7[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 8:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button8[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 9:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >9)
            n_button_n_pressed = 0;
          message[index] = (button9[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
        case 10:
          if(index>=0)
            index--;
          while(scanAnyKey() != -1)
            ;
//          wait(DELAY);
          break;
        case 11:
          if(last_pressed == new_pressed)
            n_button_n_pressed++;
          if(n_button_n_pressed >7)
            n_button_n_pressed = 0;
          message[index] = (button11[n_button_n_pressed]);
          debounce();
//          wait(DELAY);
          break;
      }
      button_move = 0;
      message[index+1] = '_';
      message[index+2] = '\0';
      fillBuf(0x0000);
      drawMessage(message, 0xFFFF, 1);
      drawStringZoom(0,0, navigation_string, 0xffff, 1);
      writeBuf();
      last_pressed = new_pressed;
      // Debug output:
      // uartSend(message);
      // uartSend("\r\n");  
    }
  button_move++;
  if (button_move == 0xfffffff0)
    button_move = 0;
  }
  //get rid of _ character
  message[index+1] = '\0';
  return index+1;
}


void waitForOK(void) {
  while((2 != scanAnyKey()))
    ;
}


void debounce(void) {
  while(scanAnyKey() != -1)
    ;
  wait(30);
}



