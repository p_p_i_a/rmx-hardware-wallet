#ifndef EFUSE_H
#define EFuSE_H
/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

/*
For programming OTP memory 1.52V must be externally wired to ADC pin. 
In order to automatize this, there is TS5A3357 used (check datasheet in docs/TS5A3357.pdf). 
It has two inputs driven by gpio203 and 204 and based on combinations,
ADC pin is driven:
|----------------------------------|
| gpio203:  | gpio204: | ADC pin:  |
|-----------|--------- |-----------|
|   low     |   low    | off       |
|   high    |   low    | 3.3V      |
|   low     |   high   | 1.52V     | <- OTP programming state
|   high    |   high   | GND       |
|----------------------------------|
*/


#include "stdint.h"
#include "util.h"
/*
OTP efuse region byte numbers (start from address 0x40082010 )
------------------------------------------------------------------------------
0-31 	       |  el. curve privkey for decryption during fw loading process |  block 0
32-127 	       |  Microchip's stuff                                          |
------------------------------------------------------------------------------
128-159        |  primary authentication pubkey Qx, X coordinate             |  block 1
160-191        |  primary authentication pubkey Qy, Y coordinate             |
192-255        |  CUSTOMER USE                                               |
------------------------------------------------------------------------------
256-383        |  CUSTOMER USE                                               |  block 2
------------------------------------------------------------------------------
384-415        |  CUSTOMER USE                                               |  block 3
416-447        |  secondary authentication pubkey Rx, X coordinate           |
448-479        |  secondary authentication pubkey Rx, X coordinate           |
482            |  One byte for debug functions, single bits are iportant..   |
483            |  Customer flags - encryption enable, ...                    |
484-507        |  Microchip test, do not modify                              |
508-509        |  SPI tag base                                               |
510-511        |  Version identifier                                         |
------------------------------------------------------------------------------
otp lock register 4000FC24
*/

/** EFUSE ADDRESSES **/
#define EFUSE_MANUAL_CONTROL_REGISTER       0x40082004
#define EFUSE_CONTROL_REGISTER              0x40082000
#define EFUSE_MANUAL_MODE_ADDRESS_REGISTER  0x40082006

#define EFUSE_MANUAL_CONTROL                (*((volatile uint16_t*) EFUSE_MANUAL_CONTROL_REGISTER))
#define EFUSE_CONTROL                       (*((volatile uint16_t*) EFUSE_CONTROL_REGISTER))
#define EFUSE_MANUAL_MODE_ADDRESS           (*((volatile uint16_t*) EFUSE_MANUAL_MODE_ADDRESS_REGISTER))
#define EFUSE_EFUSE_MEMORY                  (*((volatile uint32_t*) EFUSE_EFUSE_MEMORY_REGISTER))

/** Offsets **/
#define EFUSE_BLOCK_0                  0x40082010
#define EFUSE_BLOCK_1                  0x40082090
#define EFUSE_BLOCK_2                  0x40082110
#define EFUSE_BLOCK_3                  0x40082190
/** Block 1 **/
#define EFUSE_MEMORY_START             0x40082010
#define EFUSE_MEMORY_PRIM_AUTH_PUBKEY  0x40082090  //  64B
#define EFUSE_MEMORY_HWKEY             0x400820D0  //  32B
#define EFUSE_MEMORY_SETUP_BYTES       0x400820F0  //   8B
#define EFUSE_MEMORY_VISUAL_ID         0x400820F8  //  24B
/** Block 2  **/
#define EFUSE_MEMORY_SEEDS       0x40082110        //  size 0xE0, 7 seeds 32B each

/* OTP used in RMX
|BYTE NUM | ADDRESS       |               Comment                                       | 
-----------------------------------------------------------------------------------------
0-31 	  0x010-0x02F     |  el. curve privkey for decryption during fw loading process |  block 0
32-127 	  0x030-0x08f     |  Microchip's stuff                                          |
-----------------------------------------------------------------------------------------  block 1
128-159   0x090-0x0Af     |  primary authentication pubkey Qx, X coordinate             |
160-191   0x0B0-0x0CF     |  primary authentication pubkey Qy, Y coordinate             |
192-223   0x0D0-0x0EF     |  device's hw key                                            |
224-255   0x0F0-0x10F     |  setup bytes[8], visual identifier[24]                      |
-----------------------------------------------------------------------------------------  block 2
256-287    0x110-0x12f    |  seed slot 0                                                |
288-319    0x130-0x14f    |  seed slot 1                                                |
320-351    0x150-0x16f    |  seed slot 2                                                |
252-383    0x170-0x18f    |  seed slot 3                                                |
-----------------------------------------------------------------------------------------  block 3
384-415    0x190-0x1Af    |  seed slot 4                                                |
416-447    0x1B0-0x1CF    |  Secondary authentication pubkey Rx, X coordinate           |
448-479    0x1D0-0x1EF    |  Secondary authentication pubkey Ry, Y coordinate           |
480-481    0x1F0-0x1F1    |  Microchip test functions, Should not be modified.          |
482        0x1F2          |  One byte for debug functions, single bits are iportant..   |
483        0x1F3          |  Customer flags - encryption enable, ...                    |
484-507    0x1F4-0x20B    |  Microchip test, do not modify                              |
508-509    0x20C-0x20C    |  SPI tag base                                               |
510-511    0x20D-0x20E    |  Version identifier                                         |
-----------------------------------------------------------------------------------------*/


/** Pin settings for external switch for switching reference programming voltage **/
void vrefAdcInit(void);


/** 1.52V is wired to ADC pin **/
void vrefAdc152V(void);


/**3.3V is wired to ADC pin**/
void vrefAdc330V(void);


/** ADC pin is wired to ground **/
void vrefAdcGnd(void);


/** ADC pin left open **/
void vrefAdcOpen(void);


/**
 Function reads eight OTP setup bytes from EFUSE and returns actual usage of seed slots.
 returns 16bits, left 8 are used position, right are used status.
 0x00 means position is empty, 0x01 means position contains encrypted seed 
**/
uint16_t checkSetupBytes(void);


/** Burns 32 bytes of seed data into efuse **/
bool writeSeed(uint8_t position, uint8_t *array);


/** Reads efuse block and dumps everything into serial port **/
void checkEfuse(void);


/** Enable EFUSE block **/
void efuseEnable(void);


/** Disable EFUSE block **/
void efuseDisable(void);


/** returns false on success, returns true if there was some error **/
bool burnSetupByte(uint8_t byte_pos, uint8_t value);


/*
1. Set the VREF_ADC pin to ground before powering up.
2. Power up the rest of the supplies
3. Set the MAN_ENABLE bit in the Manual Control Register to ‘1b’
4. Set the FSOURCE_EN_PRGM to ‘1b’
5. Set FSOURCE_EN_READ to ‘0b’ - DO NOT combine with step 4; writing FSOURCE_EN_PRGM and FSOURCE_EN_READ MUST be performed with separate writes
6. Set the VREF_ADC pin to the VPP programming voltage in between1.52V to 1.60V.
7. Set the IP_CS bit in the Manual Control Register to ‘0b’. The disables the eFuse memory array
8. Select the IP_ADDR_HI field in the Manual Mode Address Register to the block number of the block to be pro-grammed
9. Set the IP_CS bit in the Manual Control Register to ‘1b’. The enables and powers up the 		selected block in the eFuse memory array
10. Select the IP_ADDR_LO field in the Manual Mode Address Register to the address of the bit within the current block to be programmed
11. Wait 100 ns (min)
12. Set PROG_EN to HIGH for 10μs (typical.
13. Set PROG_EN to LOW for 100 ns (min)
14. Repeat steps 10 to 13 for all bits within a block of 1K bits that are to be programmed to ‘1b’
15. In order to program bits in another 1K bit block, go back to Step 7 and follow the subsequent steps to programsthe bits in that block
*/
void efuseProgram( uint8_t block, uint8_t bit_address, uint8_t bytes_to_burn[], uint16_t number_of_bytes);





#endif
