/* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *  Copyright (C) 2019 m2049r@monerujo.io
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

  /*
  Keyboard wiring RMX v. 0.1:
  SIGNAL:  GPIO:   IRQ:     BIT:   PIN:
  row1     120     GIRQ09   16     [H10]
  row2     027     GIRQ11   23     [J10]
  row3     053     GIRQ10   11     [K10]
  row4     054     GIRQ10   12     [J9 ]
  col1     026     GIRG11   22     [K9 ]
  col2     031     GIRQ11   25     [J8 ]
  col3     040     GIRQ10   0      [K8 ]

  Keyboard wiring RMX v. 0.2:
  SIGNAL:  GPIO:   IRQ:     BIT:   PIN:
  row1    026      GIRG11   22
  row3    053      GIRQ10   11
  row4    027      GIRQ11   23
  row2    054      GIRQ10   12
  col3    031      GIRQ11   25
  col2    040      GIRQ10   0
  col1    017      GIRQ11   15
  */


#ifndef BUTTONS_GIRQ_H
#define BUTTONS_GIRQ_H

#include <stdint.h>

/** Interrupts init **/
void GIRQ10_GIRQ11_init(void);


/** Interrupts for buttons ENABLE **/
void GIRQ10_GIRQ11_enable(void);


/** Interrupts for buttons DISABLE **/
void GIRQ10_GIRQ11_disable(void);


/** NVIC handler **/
void NVIC_Handler_GIRQ10(void);


/** NVIC handler **/
void NVIC_Handler_GIRQ11(void);


#endif
