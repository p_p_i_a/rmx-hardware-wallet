/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "uart.h"
#include "rng.h"
#include "util.h"
#include "crypto/crypto.h"
#include <string.h>
#include "systick.h"
#include <stdio.h>
#include "monero_sync.h"
#include "sha3.h"

#define KEYSIZE 32
#define PUBSIZE (1+2*KEYSIZE+4)

#define TESTSIZE 500





////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////  Stuff needed for compiling m2049r's benchmark code:  ///////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// initialize my secret keys
key32_t key_a;
key32_t key_b;
scalar32_t eight_a;
key32_t key_A;
key32_t keys_B[10];
size_t keys_B_size;
void initMyKeys() {
  // 55jmiG8FqZRLjrdGYStxXJ9GsKhUaFsSe5puySep445558a8t94Ld2JCXpsiegkg4y482iDYnDCwSUgKBkyhftDo1eMBSwC
  // a secret view:  d129fb0b31ef4c2ffbe21cf6be39a2f4bf46048242c462079099bcb5e6455c04
  // b secret spend: 5a877db95d9c89d296499cf0d3054e15b12b5502dea7be95b6d1cc04493cc80f
  memcpy(key_a, fromhexLE("d129fb0b31ef4c2ffbe21cf6be39a2f4bf46048242c462079099bcb5e6455c04"), KEYSIZE);
  memcpy(key_b, fromhexLE("5a877db95d9c89d296499cf0d305	4e15b12b5502dea7be95b6d1cc04493cc80"), KEYSIZE);
  // A public view:  b1e3ea47b50dc344f132227d9277ea12a9119611fc3d71a5799139f8c1d24205
  // B public spend: 671be150055c587602d834ed354d413176e9938682eaf71cda731066584f2018
  scalarMultBase(key_A, key_a);
  scalarMultBase(keys_B[0], key_b);
  keys_B_size = 1;

  mult8_modp(eight_a, key_a);
}

// initialize 8*a*R - needed once per transaction, R := TX_public_key from extra
static key32_t D;

uint8_t* getD() {
  return D;
}

void initMatch(const key32_t R) {
  // compute D = 8*a*R
  scalarMultKey(D, eight_a, R);
}

/* check if P (output key image) matches for output slot <outputIndex>
 * prereqs:
 *  initMyKeys()
 *  view keys are initialized in keys_B[keys_B_size]
 *  D is initialized through initMatch(R)
 */
bool keyMatches(const key32_t Pk, const size_t outputIndex) {
  /* H(D||i) */
  scalar32_t Hs_D;
  derivation_to_scalar(D, outputIndex, Hs_D);
  coord32_t P;
  recoverPoint(&P, Pk);
  /* B' := P - H(D||i)G
   *     = P - (Hs_D)G
   */
   key32_t B;
   // no idea why this works as it seems to P+hG and we need P-hG (= P+hG')
  calcBprime(B, Hs_D, &G, &P);
  for (size_t i = 0; i < keys_B_size; i++) {
    if (!memcmp(keys_B[i], B, SCALARSIZE)) {
      uartSend("match: "); uartSend(tohexBE((uint8_t*) &i, sizeof(size_t)));
      return true;
    }
  }
  return false;
}

/*
void matchKeyList(const output_list_t *outputs) {
  initMatch(outputs->R);
  for (size_t i = 0; i < outputs->size; i++) {
    keyMatches(outputs->P[i], i); // not sure yet what to do with the results
  }
}
*/

// check all key images given a sorted array of key images by index 0..n
void matchKeys(const key32_t P[], const size_t num, bool res[]) {
  for (size_t i = 0; i < num; i++) {
    res[i] = keyMatches(P[i], i);
  }
}

/* calculate x - the secret key for the given output - TODO: this probably does not work for subaddresses
 * prereqs:
 *  initMyKeys()
 *  D is initialized through initMatch(R)
 */
bool calcSecret(const key32_t P, const size_t outputIndex) {
  /* H(D||i) */
  scalar32_t Hs_D;
  derivation_to_scalar(D, outputIndex, Hs_D);

  /* x := H(D||i)+b */
  scalar32_t x;
  add_modp(x, Hs_D, key_b);

  /* P := (H(aR||i)+b)G */
  key32_t Pd;
  scalarMultBase(Pd, x);

  for (size_t n = 0; n < KEYSIZE; n++)
    if (P[n] != Pd[n]) return false;
  return true;
}








///// benchmark:

void testWalletFromSeed(key32_t spendkey) {
  // reduce to spendkey
  reduce32(spendkey);

  key32_t viewkey;
  // hash on the way to view key
	keccak_256(spendkey, KEYSIZE, viewkey);
	// reduce to view key
	reduce32(viewkey);

	// make public spend key
	key32_t pubSpendkey;
	scalarMultBase(pubSpendkey, spendkey);
	// make public view key
	key32_t pubViewkey;
	scalarMultBase(pubViewkey, viewkey);
}

void benchmark(void) {
  uint32_t t, tA;

	uint8_t spendkey[TESTSIZE][KEYSIZE];
  uint8_t key[KEYSIZE];

	uartSend("\r\nTESTSIZE: 0x");
  uint32_t d = TESTSIZE;
  uartSend(tohexBE((uint8_t*) &d, 4));
  uartSend("\r\n");

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
    rng_get_random_blocking((uint32_t*) spendkey[i], KEYSIZE/4);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\nRNG: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

#if 0
  for (int i = 0; i < TESTSIZE; i++) {
    uartSend(tohexBE((uint8_t*) spendkey[i], 32));
    uartSend("\r\n");
  }
#endif

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
    reduce32(spendkey[i]);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\nreduce32: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
    keccak_256(spendkey[i], KEYSIZE, key);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\nkeccak_256: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
   ;// fucked duriong cleaning, need to be found in old commits testTX();
  }
  t = sm_now_ms() - tA;
	uartSend("\r\ntestTX: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

  initMyKeys();
  key32_t R;
  memcpy(R, fromhexLE("0cf6f212b46af4d5413bb3572985d863a7b09d04e32b04e33339cc843bd0a8db"), KEYSIZE);
  initMatch(R);
  key32_t P0;
  memcpy(P0, fromhexLE("dcf7d03af280f24d57fe1dc0c9a1667f19009843f15fa11f7af9d13f0e54521f"), KEYSIZE);
  key32_t P1;
  memcpy(P1, fromhexLE("6548c1d84e0c231d3ab270e13b3dfe23f40c23cac6e65d3b6b7dbb5e325d7730"), KEYSIZE);

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
    initMatch(R);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\ninitMatch: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE/2; i++) {
    //keyMatches(P0, 0);
    keyMatches(P1, 1);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\nkeyMatches: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
  	scalarMultBase(key, spendkey[i]);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\npubkey: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");


  /* H(D||i) */
  scalar32_t Hs_D;
  derivation_to_scalar(getD(), 1, Hs_D);
  /*
   * B' := P - H(D||i)G
   *     = P - (Hs_D)G
   */
  coord32_t P;
  recoverPoint(&P, P1);

  // P+hG // TODO: minus??
  key32_t B;
  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
    calcBprime(B, Hs_D, &G, &P);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\nverify: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");

/*
  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
  	testWalletFromSeed(spendkey[i]);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\ngenerateWalletFromSeed: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\n");
*/
}

#include "util.h"
void benchmark_publickey(void) {
  uint32_t t, tA;

  const uint8_t *test_key = fromhexLE("a988079406c852ed5836cc3acb699e846b1051d03babb69aafa25689c3312801");
  key32_t seckey;
  memcpy(seckey, test_key, KEYSIZE);

  uartSend("\r\nseckey=");
  uartSend(tohexLE((uint8_t*) seckey, KEYSIZE));

  key32_t pubkey;

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
  	scalarMultBase(pubkey, seckey);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\npubkey: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\npubkey=");
  uartSend(tohexLE((uint8_t*) pubkey, KEYSIZE));

  tA = sm_now_ms();
  for (int i = 0; i < TESTSIZE; i++) {
  	scalarMultBase(pubkey, seckey);
  }
  t = sm_now_ms() - tA;
	uartSend("\r\npubkeyX: 0x");
  uartSend(tohexBE((uint8_t*) &t, 4));
  uartSend("\r\npubkey=");
  uartSend(tohexLE((uint8_t*) pubkey, KEYSIZE));
}
