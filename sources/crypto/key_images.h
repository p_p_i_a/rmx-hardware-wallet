

#ifndef KEY_IMAGES_H
#define KEY_IMAGES_H

#include "stdint.h"
#include <stdlib.h>


#define mul32x32_64(a,b) (((uint64_t)(a))*(b))
#define ALIGN(x) __attribute__((aligned(x)))

typedef uint32_t bignum25519[10];



typedef struct ge25519_t {
	bignum25519 x, y, z, t;
} ge25519;


typedef struct ge25519_p1p1_t {
	bignum25519 x, y, z, t;
} ge25519_p1p1;

typedef struct ge25519_pniels_t {
	bignum25519 ysubx, xaddy, z, t2d;
} ge25519_pniels;


typedef uint32_t bignum256modm_element_t;
typedef bignum256modm_element_t bignum256modm[9];



////
void ge25519_scalarmult(ge25519 *r, const ge25519 *p1, const bignum256modm s1);

//dependencies:


void contract256_window4_modm(signed char r[64], const bignum256modm in);


void ge25519_full_to_pniels(ge25519_pniels *p, const ge25519 *r);


void ge25519_set_neutral(ge25519 *r);


void ge25519_pnielsadd(ge25519_pniels *r, const ge25519 *p, const ge25519_pniels *q);

void ge25519_move_conditional_pniels_array(ge25519_pniels * r, const ge25519_pniels * p, int pos, int n);


void ge25519_pnielsadd_p1p1(ge25519_p1p1 *r, const ge25519 *p, const ge25519_pniels *q, unsigned char signbit);


void curve25519_add_after_basic(bignum25519 out, const bignum25519 a, const bignum25519 b);


//static 
void ge25519_cmove_stride4b(long * r, long * p, long * pos, long * n, int stride);

void expand256_modm(bignum256modm out, const unsigned char *in, size_t len);


void barrett_reduce256_modm(bignum256modm r, const bignum256modm q1, const bignum256modm r1);

void reduce256_modm(bignum256modm r);






////

void ge25519_p1p1_to_partial(ge25519 *r, const ge25519_p1p1 *p);


void ge25519_p1p1_to_full(ge25519 *r, const ge25519_p1p1 *p);



/* out = a - b, where a is the result of a basic op (add,sub) */
void curve25519_sub_after_basic(bignum25519 out, const bignum25519 a, const bignum25519 b);

/* out = a - b */
void curve25519_sub(bignum25519 out, const bignum25519 a, const bignum25519 b);


void curve25519_add(bignum25519 out, const bignum25519 a, const bignum25519 b);


void ge25519_double_p1p1(ge25519_p1p1 *r, const ge25519 *p);


void ge25519_double_partial(ge25519 *r, const ge25519 *p);


void ge25519_double(ge25519 *r, const ge25519 *p);


void ge25519_mul8(ge25519 *r, const ge25519 *t);


void curve25519_neg(bignum25519 out, const bignum25519 a);


void curve25519_contract(unsigned char out[32], const bignum25519 in);


int curve25519_isnegative(const bignum25519 f);


void curve25519_mul(bignum25519 out, const bignum25519 a, const bignum25519 b);


void curve25519_set(bignum25519 r, uint32_t x);


void curve25519_add_reduce(bignum25519 out, const bignum25519 a, const bignum25519 b);

/* out = in * in */
void curve25519_square(bignum25519 out, const bignum25519 in);


void curve25519_expand(bignum25519 out, const unsigned char in[32]);


void curve25519_expand_reduce(bignum25519 out, const unsigned char in[32]);


void ge25519_fromfe_frombytes_vartime(ge25519 *r, const unsigned char *s);


void curve25519_divpowm1(bignum25519 r, const bignum25519 u, const bignum25519 v);


void curve25519_sub_reduce(bignum25519 out, const bignum25519 a, const bignum25519 b);


/* out = in */
void curve25519_copy(bignum25519 out, const bignum25519 in);


int curve25519_isnonzero(const bignum25519 f);



////////////////////

#define curve25519_mul_noinline curve25519_mul

void curve25519_recip(bignum25519 out, const bignum25519 z);



/* out = in ^ (2 * count) */
void curve25519_square_times(bignum25519 out, const bignum25519 in, int count);



/*
 * In:  b =   2^5 - 2^0
 * Out: b = 2^250 - 2^0
 */
void curve25519_pow_two5mtwo0_two250mtwo0(bignum25519 b);


void ge25519_pack(unsigned char r[32], const ge25519 *p);









#endif
