/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "spi.h"
#include "CEC1702_defines.h"



void spiInit(void) {
  GPIO004_PIN_CONTROL_REGISTER = 0x2241UL;  // SDA   GPIO004
  GPIO034_PIN_CONTROL_REGISTER = 0x2241UL;  // SCK   GPIO034
  GPIO201_PIN_CONTROL_REGISTER = 0x10240UL; // CS    GPIO201
  GPIO204_PIN_CONTROL_REGISTER = 0x10240UL; // DC    GPIO204
  GPIO202_PIN_CONTROL_REGISTER = 0x10240UL; // RESET GPIO202
  GPIO203_PIN_CONTROL_REGISTER = 0x00240UL; // LDO   GPIO203

  // hw block settings
  SPI_CONTROL_REGISTER         |= 0x00UL;
  SPI_CLOCK_CONTROL_REGISTER    = 0x00UL;
  SPI_CLOCK_GENERATOR_REGISTER |= 0x00UL;
  SPI_ENABLE_REGISTER          |= 0x01UL;
}


void csLow (void) {
  CS_DISPLAY_OUTPUT_BIT = 0;
}


void csHigh (void) {
  CS_DISPLAY_OUTPUT_BIT = 1;
}


void dcLow (void) {
  DC_OUTPUT_BIT = 0;
}


void dcHigh (void) {
  DC_OUTPUT_BIT = 1;
}


void ldoDisable (void) {
  OLED_LDO_EN_OUTPUT_BIT = 0;
}


void ldoEnable (void) {
  OLED_LDO_EN_OUTPUT_BIT = 1;
}


void resetLow (void) {
  OLED_RESET_OUTPUT_BIT  = 0;
}


void resetHigh(void) {
  OLED_RESET_OUTPUT_BIT  = 1;
}


void writeSPI(uint8_t c) {
  // is TX buffer empty?
  if ((SPI_STATUS_REGISTER & 1UL) == 1) {
    SPI_TX_DATA_REGISTER = c;
  }
  // wait until transmission is done
  while (!(SPI_STATUS_REGISTER & (1UL)))
    ;
}


void writeCmd(uint8_t c) {
  dcLow();
  writeSPI(c);
}


void writeData(uint8_t c) {
  dcHigh();
  writeSPI(c);
}
