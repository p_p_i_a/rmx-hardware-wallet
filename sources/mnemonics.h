/* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) m2049r <m2049r@monerujo.io>
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MNEMONICS_H_
#define MNEMONICS_H_

#include <stdbool.h>
#include <stdint.h>

/** Converts 32B seed into string with words (in a form of three beginning letters, without spaces) **/
int bytesToWords(const uint8_t bytes[], char words[]);

/** Converts string with words (separated with spaces) into 32B seed**/
int wordsToBytes(const char words[], uint8_t bytes[]);

/** Calculating checksum word **/
uint32_t checksumWord(const char seed[]);


/** Passing a string, if match is found, output is an index from a dictionary **/
uint16_t decodeWord(char input[]);


/** Passing an index, returning a word from a dictionary **/
const char* mnemonicWord(uint32_t word_idx);


/* crc is previous value for incremental computation, 0xffffffff initially */
uint32_t checksum_crc32(char *data, uint32_t length, uint32_t crc);



#endif /* MNEMONICS_H_ */
