/* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 * Copyright (C) 2018 m2049r <m2049r@monerujo.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "mec2016_rom_api.h"
#include "uart.h"
#include "util.h"
#include "crypto/crypto.h"
#include <string.h>
#include "mnemonics.h"
#include "storage.h"
#include "login.h"
#include "communication.h"
#include "sha3.h"
#include "monero_login.h"
#include "monero_messages.h"
#include "monero_storage.h"
#include "monero_sync.h"




//#include "key_images.h"
/* Switching debug code compilation on/off */
#include "debug.h"



extern MONERO_DATA monero_data[];
uint32_t blockheight_helper;
// when calling for batch of data, they are stored here
InGetTransactionBatch Scanning_data_structure = {0};
InGetTransactionDetails Tx_data_structure = {0};



//------------------------------------------------------------------------------------------------------
void moneroSynchronize(void) {
  uint32_t next_blockheight = monero_data->setup.local_blockheight;
  uint32_t next_tx_offset = monero_data->setup.local_tx_offset;
  monero_data->setup.blocks = 0;


  if(initScanning(monero_data->setup.batch_size))
    errorMessage("init scanning failed");
 
  while((monero_data->setup.local_blockheight < monero_data->setup.global_blockheight) && (scanAnyKey() != 0x02)) { //button OK to exit
    blockheight_helper = monero_data->setup.local_blockheight;

    char b[16];
    fillBuf(0x0000);
    //ring update
    progressBarCircle(32, MONERO_ORANGE, 1);  
    drawStringCenteredZoom(0, "Waiting for rmxd..", WHITE, 1);
    drawStringCenteredZoom(55, itoaa(monero_data->setup.local_blockheight, b), WHITE, 1);
    drawStringCenteredZoom(65, itoaa(monero_data->setup.global_blockheight - monero_data->setup.local_blockheight, b), WHITE, 1);
    //drawStringCenteredZoom(75, itoaa(monero_data->setup.matches, b), WHITE, 1);
    writeBuf();

    //ask node for batch from exact point and save it into Scanning_data_structure.data[BATCH_SIZE]
    while (getBatch(next_blockheight, next_tx_offset)) {// && (scanAnyKey() != 0x02)) {
      errorMessage("getBatch()");
    }
    //now data re ready in scnning data structure
    //computing number of blocks in a batch, used for progressbar
    monero_data->setup.blocks = monero_data->setup.local_blockheight - blockheight_helper;
    // now we have all data in struct Scanning_data_structure.data, we can process them.
    scanBatch();  
    //Mirroring data into persistence storage
    monero_data->setup.local_blockheight = Scanning_data_structure.end_blockheight;
    monero_data->setup.local_tx_offset = Scanning_data_structure.end_tx_offset;
    monero_data->setup.global_blockheight = Scanning_data_structure.global_blockheight;
    next_blockheight = Scanning_data_structure.next_blockheight;
    next_tx_offset = Scanning_data_structure.next_tx_offset;
    //saving into persistece
    storageUpdate(1);
  }
//  fillBuf(0x0000);
//  drawStringCenteredZoom(100, "exiting...", 0xFFFF, 1);
//  writeBuf();
//  debounce();//debounce when exiting synchronize
}



//------------------------------------------------------------------------------------------------------
bool scanBatch(void) {
  uint8_t number_of_R = 0; 
  size_t number_of_P = 0;
  uint16_t Tx_offset_within_a_batch = 0;
  uint32_t i  = 0; //main index in the batch dat field
  uint8_t dummy = 0;

  //now secret spend is in privkey, secret view is in key_a
  key32_t key_a; //secret view
  hash_to_scalar(privkey, KEYSIZE, key_a); 
  
  //public keys
  key32_t key_A;
///  key32_t keys_B[10];
  scalarMultBase(key_A, key_a);
///  scalarMultBase(keys_B[0], privkey);

  //8*a
  scalar32_t eight_a;
  mult8_modp(eight_a, key_a);

  key32_t R, D;
  key32_t P;
  scalar32_t Hs_D;
  scalar32_t x;
  key32_t Pd;

  while((i<BATCH_SIZE) && (scanAnyKey() != 0x02)) {
  
    if (Scanning_data_structure.data[i] == 0x81 ) {//Tx starts from here
      i++; 
      Tx_offset_within_a_batch = Scanning_data_structure.data[i];//Tx offset
      i++;
      number_of_R = Scanning_data_structure.data[i];
      i++;
      number_of_P = Scanning_data_structure.data[i];
      i++;
      //now testing matches
      for(size_t r = 0; r<number_of_R; r++) { //for each R
        //precomputing for each R once
        memcpy(R, &Scanning_data_structure.data[i+(r*KEYSIZE)], KEYSIZE);
        scalarMultKey(D, eight_a, R);//initMatch(R);
        for(size_t output_index = 0; output_index<number_of_P; output_index++) { //for each P
          derivation_to_scalar(D, output_index, Hs_D);
          add_modp(x, Hs_D, privkey);
          scalarMultBase(Pd, x);
          memcpy(P, &Scanning_data_structure.data[i+number_of_R*KEYSIZE +output_index*KEYSIZE], KEYSIZE);
          // if match found:
          if (0 == memcmp(P, Pd, 32)) {
            //monero_data->setup.matches++;
            ledBlink(SHORT);
            //getting the Tx ID and saving into Tx_data_structure
            if(getTxDetails(Scanning_data_structure.start_blockheight, Scanning_data_structure.start_tx_offset, Tx_offset_within_a_batch)) {
              errorMessage("getTxDetails()");
            }
            if (scanTx())
              errorMessage("scanTx()");
          }
        }
      }
      Tx_offset_within_a_batch++;
    }
    // TX was checked for match, now move on in a batch to another one
    i = i + (number_of_R+number_of_P)*KEYSIZE;
    //skip the block padding
    if (Scanning_data_structure.data[i] == 0xFF) {
      break;
    }

    if(i > ((BATCH_SIZE/32)*dummy)) {
      scanningDisplayUpdate(dummy+1);
      dummy++;
    }
  }
  return 0;
}

bool scanTx(void) {

  uint8_t ecdhInfo[8] = {0};
  key32_t hash;
  uint8_t data[38];

  //now secret spend is in privkey, secret view is in key_a
  key32_t key_a; //secret view
  hash_to_scalar(privkey, KEYSIZE, key_a); 
  
  //public keys
  key32_t key_A;
///  key32_t keys_B[10];
  scalarMultBase(key_A, key_a);
///  scalarMultBase(keys_B[0], privkey);

  //8*a
  scalar32_t eight_a;
  mult8_modp(eight_a, key_a);

  key32_t R, D;
  key32_t P;
  scalar32_t Hs_D;
  scalar32_t x;
  key32_t Pd;


  //check this TxID
  if (isTxSaved(Tx_data_structure.TxID))
    return 0;  //just skip the saving process
  //save TxID
  if(saveTxID(Tx_data_structure.TxID)) {
    errorMessage("Saving TXID before processing the P's hahaha");
  }
  uint16_t *p_key_images_num =  (uint16_t*)&monero_data->wallet.TxImages[monero_data->wallet.savedBytes - 8];//fixme 8 define
  *p_key_images_num = 0;

  //and go after key images:
  //now testing matches
  for(uint32_t r = 0; r<Tx_data_structure.Rnum; r++) { //for each R
    //precomputing for each R once
    memcpy(R, &Tx_data_structure.Tx_data[r*KEYSIZE], KEYSIZE);
    scalarMultKey(D, eight_a, R);
    for(size_t output_index = 0; output_index<Tx_data_structure.Pnum; output_index++) { //for each P
      derivation_to_scalar(D, output_index, Hs_D);
      add_modp(x, Hs_D, privkey);
      scalarMultBase(Pd, x);
      memcpy(P, &Tx_data_structure.Tx_data[Tx_data_structure.Rnum*KEYSIZE + output_index*(KEYSIZE + AMOUNTSIZE)], KEYSIZE);
      // if match found:
      if (0 == memcmp(P, Pd, 32)) {
        // computing keyImage = x(Hp(P))
        key32_t point;
        hash_to_ec(P, point);
        scalar32_t key_image;
        generate_key_image(point, x, key_image);

        //save key image just after previously saved TxID
        if(saveKeyImage(key_image)) {
          errorMessage("no more space for saving key image hahahhha");
        }
        //decrypting amount
        memcpy(ecdhInfo, &Tx_data_structure.Tx_data[Tx_data_structure.Rnum*KEYSIZE + (output_index*(KEYSIZE + AMOUNTSIZE) + KEYSIZE)], AMOUNTSIZE);
        memcpy(data, "amount", 6);
        memcpy(data + 6, &Hs_D, 32);
        keccak_256(data, 38, hash);
        for (int i = 0; i < 8; ++i)
          ecdhInfo[i] ^= hash[i];

        //save amount just after previously saved key image
        if(saveAmount(ecdhInfo)) {
          errorMessage("no more space for saving decrypted amount hahahhha");
        }
        // ++ amount of key images after stored TX
        *p_key_images_num = *p_key_images_num + 1;
      }
    }
  }
  return 0;
}




///monero scanning display stuff
void scanningDisplayUpdate (int progressbar) {
  //char b[16];
  fillBuf(0x0000);
  // Ring update
  progressBarCircle(32, MONERO_GREY, 1);
  progressBarCircle(progressbar, MONERO_ORANGE, 1);  
  drawStringCenteredZoom(0, "Scanning outputs", WHITE, 1);
  drawStringCenteredZoom(120, "Hold OK to exit", WHITE, 1);
  writeBuf(); 
}


bool finalize1(void) {
  uint32_t i = 0;

  // check ifthere are some data to finalize
  if(monero_data->wallet.savedBytes == 0x00) {
    return 1;
  }

  //delete spent outputs
  while(i<monero_data->wallet.savedBytes) {
    // storing num of key images in this TX
    uint16_t keyImagesNum = (uint16_t)monero_data->wallet.TxImages[i+32];
    //so i can later do num-- after deleting spent key image
    uint16_t *p_to_images_num = &monero_data->wallet.TxImages[i+32];

    // shifting to the first key image and its amount  
    i +=40;
    // now go through keyImages and check if they are spent
    for(uint32_t j = 0; j < keyImagesNum; j++) { 
      if(isSpent((uint8_t*)&monero_data->wallet.TxImages[i]) == 1) {
      //if(0 == memcmp((uint8_t*)&keyimage, (uint8_t*)&monero_data->wallet.TxImages[i], 32)) {
//  scalar32_t keyimage;
//  memcpy(keyimage, fromhexLE("cc0c81c2803f72f924c4503560f5383eca91aedda9cba06c7dbca2f0f5c082b5"), KEYSIZE);
        ledBlink(LARGE);
        if(keyImagesNum == 1) {
          uint8_t *p_shift_here = &monero_data->wallet.TxImages[i-40];
          uint8_t *p_from_here  = &monero_data->wallet.TxImages[i+40];
          i -= 40;
          // shift data over this txid  (delete it by overwriting)
          for(uint32_t j = i; j<monero_data->wallet.savedBytes + 80; j++ ) {
            p_shift_here[j-i] = p_from_here[j-i];
          }
          monero_data->wallet.savedBytes -= 80;
          //pokud num = 1, shiftnu +40 na -40 a usedbytes -=80; i -=40
        }
        if(keyImagesNum > 1) {
          uint8_t *p_shift_here = &monero_data->wallet.TxImages[i];
          uint8_t *p_from_here  = &monero_data->wallet.TxImages[i+40];
          // shift data over this txid  (delete it by overwriting)
          for(uint32_t j = i; j<monero_data->wallet.savedBytes + 40; j++ ) {
            p_shift_here[j-i] = p_from_here[j-i];
          }
          monero_data->wallet.savedBytes -= 40;
          *p_to_images_num = keyImagesNum - 1; 
          //pokud num > 1, shiftnu +40 na   0, num --
        } 
      } else {
          i+=40; 
      }
    }
  }
  return 0;
}


bool finalize2(void) {
  uint64_t money = 0;
  uint32_t i = 0;

  // check ifthere are some data to finalize
  if(monero_data->wallet.savedBytes == 0x00) {
    return 1;
  }

   //sum amounts
  while(i<monero_data->wallet.savedBytes) {
    // storing num of key images in this TX
    uint16_t keyImagesNum = (uint16_t)monero_data->wallet.TxImages[i+32];
    // shifting to the first key image and its amount  
    i +=40;
    // now go through keyImages and finalize stuff
    for(uint32_t j = 0; j < keyImagesNum; j++) {
      uint64_t helper;
      memcpy((uint8_t*)&helper, (uint8_t*)&monero_data->wallet.TxImages[i+32], 8);
/*// Debug code:
char many [33];
amountToMoney((uint8_t*)&helper,many);
uartSend("\r\n");
uartSend(many);
*/
      money += helper;
      i+=40;
    }
  }
  // Save changes into persistence
  storageUpdate(1);

  char string[32];
  itoaa(money, string);   
  //shifting and making a space for '.'

  uint8_t str_len = strlen(string);
  for(uint8_t i = str_len+1; i>str_len - 12 - 1; i--)
    string[i+1] = string[i];
  // place a decimal dot
  string[str_len-12] = '.';

  fillBuf(0x0000);
  drawStringCenteredZoom(10, "Money:", WHITE, 1);
  drawStringCenteredZoom(30, string, WHITE, 1);
  drawStringCenteredZoom(50, "XMR", WHITE, 1);
  placeNavigation(2);
  writeBuf();
  waitForOK();

  return 0;
}










#ifdef DEBUG

void isSpentTest(void) {
  scalar32_t keyimage;
  memcpy(keyimage, fromhexLE("e5e3d5e0ad265041ef61ef646a13e361a885fa3e714397952871cecc2d9937c6"), KEYSIZE);
  fillBuf(0x0000);

  uint32_t key_image_status = isSpent(keyimage);

  switch (key_image_status) {
    case 0:
      drawStringCenteredZoom(10, "is not spent", WHITE, 1);
      break;
    case 1:
      drawStringCenteredZoom(10, "is spent", WHITE, 1);
      break;
    case 2:
      drawStringCenteredZoom(10, "is in a pool", WHITE, 1);
      break;
    default:
      errorMessage("After decoding InIsSpent msg_in");
      break;
  }
  writeBuf();
  wait(1400);
}






#endif

