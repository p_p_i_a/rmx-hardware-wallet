/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#ifndef MONERO_MESSAGES
#define MONERO_MESSAGES


/* Batch data transmission during scanning

  "InSendBatch" message description:
    Because we could cross number of blocks during batch scanning, node prepares the batch data 
    bytearray and calculates the point where the batch ends, sends this point as new_blockheight 
    and new_offset.
      1) uint32 new_blockheight
      2) uint32 new_offset
    Also every batch node sends new global network blockheight, because it could also change. 
    This way we know after scanning each batch, if we need to scan more, or we are updated. 
    In case it doesn't change, node sends the same new_global_blockheight.
      3) uint32 new_global_blockheight
    Batch data array is BATCH_SIZE long and uses this encoding. Byte 0 marks the start of header, 
    then each header consists of information about single TX: its offset within this batch, 
    number of P and number of R in that particular transaction. There will be a number of 
    transaction in this data bytearray. Length is always BATCH_SIZE bytes, padded with 0xFF.

      4) byte data[BATCH_SIZE:
   +-----------------+--------------+---------+
   |                 | byte offset  | example | 
   +-----------------+--------------+---------+
   | tx1 header byte |     0        |  0x81   | 
   +-----------------+--------------+---------+
   | tx1 offset      |     1        |  0x00   |  
   +-----------------+--------------+---------+
   | tx1 number of R |     2        |    1    |     
   +-----------------+--------------+---------+
   | tx1 number of P |     3        |    2    |
   +-----------------+--------------+---------+
   | tx1 R1          |   4-36       |   --    |
   +-----------------+--------------+---------+
   | tx1 P0          |   37-69      |   --    |
   +-----------------+--------------+---------+
   | tx1 P1          |   70-101     |   --    |
   +-----------------+--------------+---------+
                    .......
   +-----------------+--------------+---------+
   |    ...for each tx, until END_BYTE        |
   +-----------------+--------------+---------+
                    .......
   +-----------------+--------------+---------+
   |    END_BYTE     | < BATCH_SIZE |  0xFF   |
   +-----------------+--------------+---------+

   +-----------------+--------------+---------+
   |padding with 0xFF until end of batch_data |
   +-----------------+--------------+---------+   */

uint32_t initScanning(uint32_t batch_size);


bool getBatch (uint32_t local_blockheight, uint32_t offset);


bool getTxDetails(const uint32_t start_blockheight, const uint32_t start_tx_offset, const uint32_t offset_within_batch);


bool shareMoneroAddress(void);


bool isSpent(uint8_t key_image[]);


/** Init testing
    test_type:   keyImages   10
 **/
//bool initTesting(uint32_t test_type);


#endif
