/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MONERO_H
#define MONERO_H

#include <stdint.h>
#include "crypto.h"
#include "monero.pb.h"
extern InGetTransactionBatch Scanning_data_structure;
extern InGetTransactionDetails Tx_data_structure;


//monero_synchronisation
void moneroSynchronize(void);
bool scanBatch(void);
bool scanTx(void);
bool finalize1(void);
bool finalize2(void);
void scanningDisplayUpdate(int progressbar);



//monero_transactions

//monero_login

//void generateWalletFromSeed(key32_t seed);








// big function




#endif



