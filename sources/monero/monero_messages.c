/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - This program also includes the software created under the copyright of Aleksey Kravchenko 
 *     <rhash.admin@gmail.com> (2013) et al.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 *
 * This program includes sha3.h - an implementation of Secure Hash Algorithm 3 (Keccak)
 * based on The Keccak SHA-3 submission. Submission to NIST (Round 3), 2011 by Guido Bertoni, Joan Daemen, Michaël Peeters and Gilles Van      
 * Assche
 */

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include "monero_messages.h"
#include <pb_encode.h>
#include <pb_decode.h>
#include "system-messages.pb.h"
#include "monero.pb.h"
#include "storage.h"
#include "monero_login.h"
#include "communication.h"

//#include "monero.h"


extern InGetTransactionBatch Scanning_data_structure;
extern InGetTransactionDetails Tx_data_structure;

bool shareMoneroAddress(void) {

  uint8_t address[96];
  generateWalletAddress(address, privkey);

  OutMoneroAddress msg_out = {0};
  InStatusMessage msg_in = {0};


  memcpy(msg_out.address, address, 96);

  if(sendMessage(OUT_MONERO_ADDRESS, OutMoneroAddress_fields, &msg_out)) {
    errorMessage(" Sending out address");
    return 1;
  }

  if(receiveMessage(IN_STATUS_MESSAGE, InStatusMessage_fields,  &msg_out, TIMEOUT*10)) {
    errorMessage(" Receiving after sending out address");
    return 1;
  }
  if(msg_in.status)  // OK
    return 0;
  
  return 1;
}




//------------------------------------------------------------------------------------------------------
bool getBatch (uint32_t local_blockheight, uint32_t TX_offset) {
  OutGetTransactionBatch msg_out = OutGetTransactionBatch_init_zero;

  msg_out.local_blockheight  = local_blockheight;
  msg_out.offset = TX_offset;

  if(sendMessage(OUT_GET_TRANSACTION_BATCH, OutGetTransactionBatch_fields, &msg_out)) {
    errorMessage("getBatch sending out");
    return 1;
  }

  if(receiveMessage(IN_GET_TRANSACTION_BATCH, InGetTransactionBatch_fields,  &Scanning_data_structure, TIMEOUT*10)) {
    errorMessage("getBatchsending out");
    return 1;
  }

  return 0;
}  


//------------------------------------------------------------------------------------------------------
uint32_t initScanning(uint32_t batch_size) {
  OutInitScanning msg_out = {0};
  InInitScanning  msg_in  = {0};

  msg_out.batch_size = batch_size;
  //memcpy(msg_out.node_address, monero_data->setup.node_address, 128);

  if(sendMessage(OUT_INIT_SCANNING, OutInitScanning_fields, &msg_out)) {
    errorMessage("init scanning sending out");
    return 1;
  }

  if(receiveMessage(IN_INIT_SCANNING, InInitScanning_fields,  &msg_in, TIMEOUT*10)) {
    errorMessage("init scanning sending in");
    return 1;
 }

  monero_data->setup.global_blockheight = msg_in.global_blockheight;//initScanning(monero_data->setup.batch_size);

  return 0;
}


//------------------------------------------------------------------------------------------------------
bool getTxDetails(const uint32_t start_blockheight, const uint32_t start_tx_offset, const uint32_t offset_within_batch) {
  OutGetTransactionDetails msg_out = {0};

  msg_out.batch_blockheight = start_blockheight;
  msg_out.batch_tx_offset = start_tx_offset;
  msg_out.want_tx_offset = offset_within_batch;

  if(sendMessage(OUT_GET_TRANSACTION_DETAILS, OutGetTransactionDetails_fields, &msg_out)) {
    errorMessage("getTxDetails sending out");
    return 1;
  }

  if(receiveMessage(IN_GET_TRANSACTION_DETAILS, InGetTransactionDetails_fields,  &Tx_data_structure, TIMEOUT*10)){
    errorMessage("getTxDetails sending in");
    return 1;
  }

  return 0;
}


bool isSpent(uint8_t key_image[]) { 
  OutIsSpent msg_out = {0};
  InIsSpent msg_in = {0};

  memcpy(msg_out.key_image, key_image, 32);

  if(sendMessage(OUT_IS_SPENT, OutIsSpent_fields, &msg_out)) {
    return 2;
    errorMessage("isspent sending out");
  }

  if(receiveMessage(IN_IS_SPENT, InIsSpent_fields,  &msg_in, TIMEOUT*10)) {
    return 2;
    errorMessage("isspent receiving in");
  }
  if(msg_in.spent == 0)
    return 0;
  
  return 1;
}



