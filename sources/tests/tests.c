/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2019 m2049r@monerujo.io
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * This file is part of monerujo-hw
 *
 * Copyright (C) 2018 m2049r <m2049r@monerujo.io>
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
#include "mec2016_rom_api.h"
#include "uart.h"
#include "monero.h"
#include "util.h"
#include "crypto/crypto.h"
#include <string.h>
#include "mnemonics.h"
#include "storage.h"
#include "login.h"
*/

#include <pb_encode.h>
#include <pb_decode.h>

#include "communication.h"
//#include "system-messages.pb.h"
#include "tests.pb.h"

#include "tests.h"
#include "monero_messages.h"
#include "debug.h"
#include "sha3.h"




#ifdef TESTS

/** Init testing
    test_type:   keyImages   10
 **/
bool initTesting(uint32_t test_type) {

  OutInitTesting msg_out = {0};

  msg_out.test_type = test_type;

  if(sendMessage(TEST_OUT_INIT_TESTING, OutInitTesting_fields, &msg_out)) {
    errorMessage("initTesting() sending out");
  return 1;
  }
  return 0;
}



/** Testing key images. Gets from rmxd output's privkey x, returns to rmxd output's pubkey P and key image I**/
bool testKeyImage (void) {

  InTestKeyImage msg_in = {0};
  OutTestKeyImage msg_out = {0};

  do { 

    if(receiveMessage(TEST_IN_TEST_KEY_IMAGE, InTestKeyImage_fields, &msg_in, TIMEOUT*10)) {
      errorMessage("testKeyImage() receiving in");
      return 1;
    }

    fillBuf(0x0000);
    drawStringCenteredZoom(50, "Testing key images:", 0xffff, 1);
    showByte(msg_in.test_cases_left);  //fixme printing out byte number, but it is uint32_t
    writeBuf();

    scalarMultBase(msg_out.P, msg_in.x);
    key32_t point;
    hash_to_ec(msg_out.P, point);
    generate_key_image(point, msg_in.x, msg_out.I);
  

    if(sendMessage(TEST_OUT_TEST_KEY_IMAGE, OutTestKeyImage_fields, &msg_out)) {
      errorMessage("testKeyImage() sending out");
      return 1;
    }
  } while (msg_in.test_cases_left != 0);


  return 0;
}  

#endif



