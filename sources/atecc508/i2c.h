/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - Whereas this program also includes the software created under the copyright of Karl Koscher, 
 *     the following conditions are also part of the additional terms:
 *
 *     Copyright 2016 Karl Koscher
 *
 *     Permission to use, copy, modify, distribute, and sell this
 *     software and its documentation for any purpose is hereby granted
 *     without fee, provided that the above copyright notice appear in
 *     all copies and that both that the copyright notice and this
 *     permission notice and warranty disclaimer appear in supporting
 *     documentation, and that the name of the author not be used in
 *     advertising or publicity pertaining to distribution of the
 *     software without specific, written prior permission.
 *
 *     The author disclaims all warranties with regard to this
 *     software, including all implied warranties of merchantability
 *     and fitness.  In no event shall the author be liable for any
 *     special, indirect or consequential damages or any damages
 *     whatsoever resulting from loss of use, data or profits, whether
 *     in an action of contract, negligence or other tortious action,
 *     arising out of or in connection with the use or performance of
 *     this software.
 *      
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 *  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 *  See the GNU General Public License for more details. Shall the abovementioned 
 *  disclaimer provide stricter or different rule, it is considered additional term permitted
 *  by the GNU General Public License and it prevails to the extent that it does not constitute
 *  the further restriction according to section 10. of the GNU General Public License.
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 *  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef I2C_H_
#define I2C_H_
/*
void i2cWake();
void i2cInit();
uint8_t i2cSendPkt(uint8_t addr, uint8_t *pkt, int len);
uint8_t i2cRecvPkt(uint8_t addr, uint8_t *pkt, int len);
uint8_t i2cSendExtPkt(uint8_t addr, uint8_t *p1, int lp1, uint8_t *p2, int lp2);
uint8_t i2cRecvVariableLenPkt(uint8_t addr, uint8_t *pkt, int maxLen);
*/

void i2cInit(uint8_t bus);
void i2cBitDelay();
void i2cSCLLow();
void i2cSCLHigh();
void i2cSDALow();
void i2cSDAHigh();
void i2cWake();
void i2cStart();
void i2cStop();
void i2cSendBit(uint8_t bit); 
uint8_t i2cRecvBit();
uint8_t i2cSendByte(uint8_t byte);
uint8_t i2cRecvByte(uint8_t nack);
uint8_t i2cSendPkt(uint8_t addr, uint8_t *pkt, int len); 
uint8_t i2cSendExtPkt(uint8_t addr, uint8_t *p1, int lp1, uint8_t *p2, int lp2); 
uint8_t i2cRecvPkt(uint8_t addr, uint8_t *pkt, int len); 
uint8_t i2cRecvVariableLenPkt(uint8_t addr, uint8_t *pkt, int maxLen); 


#endif /* i2c_H_ */
