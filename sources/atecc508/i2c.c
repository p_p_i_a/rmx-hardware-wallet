/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - Whereas this program also includes the software created under the copyright of Karl Koscher, 
 *     the following conditions are also part of the additional terms:
 *
 *     "Copyright 2016 Karl Koscher
 *
 *     Permission to use, copy, modify, distribute, and sell this
 *     software and its documentation for any purpose is hereby granted
 *     without fee, provided that the above copyright notice appear in
 *     all copies and that both that the copyright notice and this
 *     permission notice and warranty disclaimer appear in supporting
 *     documentation, and that the name of the author not be used in
 *     advertising or publicity pertaining to distribution of the
 *     software without specific, written prior permission.
 *
 *     The author disclaims all warranties with regard to this
 *     software, including all implied warranties of merchantability
 *     and fitness.  In no event shall the author be liable for any
 *     special, indirect or consequential damages or any damages
 *     whatsoever resulting from loss of use, data or profits, whether
 *     in an action of contract, negligence or other tortious action,
 *     arising out of or in connection with the use or performance of
 *     this software."
 *      
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. Shall the abovementioned 
 * disclaimer provide stricter or different rule, it is considered additional term permitted
 * by the GNU General Public License and it prevails to the extent that it does not constitute
 * the further restriction according to section 10. of the GNU General Public License.
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

//#include <avr/io.h>
//#include <util/delay.h>
#include "crc.h"
#include "util.h"
#include "MCHP_MEC170x.h"
#include "util.h"
#include "util.h"
#include "i2c.h"
#include "CEC1702_defines.h"

// ATECC508 IC 1 SUPPLY CHAIN

#define SCL1_INPUT_BIT    (((volatile uint32_t*) ((((((GPIO001_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define SCL1_OUTPUT_BIT   (((volatile uint32_t*) ((((((GPIO001_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))

#define SDA1_INPUT_BIT    (((volatile uint32_t*) ((((((GPIO002_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define SDA1_OUTPUT_BIT   (((volatile uint32_t*) ((((((GPIO002_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))


#define SCL2_INPUT_BIT    (((volatile uint32_t*) ((((((GPIO013_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define SCL2_OUTPUT_BIT   (((volatile uint32_t*) ((((((GPIO013_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))

#define SDA2_INPUT_BIT    (((volatile uint32_t*) ((((((GPIO012_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define SDA2_OUTPUT_BIT   (((volatile uint32_t*) ((((((GPIO012_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))

// SCL GPIO013
// SDA GPIO012

// SCL GPIO010
// SDA GPIO007
 //uint8_t *p_to_setup_bytes = (uint8_t*)(0x40082010 + 0x80 + 96);


#define I2C1  0
#define I2C2  1
volatile uint32_t* SCLInput;  
volatile uint32_t* SCLOutput; 
volatile uint32_t* SDAInput;
volatile uint32_t* SDAOutput;

void i2cInit(uint8_t bus) {

  if (!bus) {
    GPIO010_PIN_CONTROL_REGISTER     = 0x10240UL; // SCL GPIO010
    GPIO007_PIN_CONTROL_REGISTER     = 0x10240UL; // SDA GPIO007
    SCLInput  = SCL1_INPUT_BIT;  
    SCLOutput = SCL1_OUTPUT_BIT; 
    SDAInput  = SDA1_INPUT_BIT;
    SDAOutput = SDA1_OUTPUT_BIT;
  } else {  
    // I2C ATEC IC 1
    GPIO012_PIN_CONTROL_REGISTER     = 0x10240UL; // SCL GPIO013
    GPIO013_PIN_CONTROL_REGISTER     = 0x10240UL; // SDA GPIO012
    SCLInput  = SCL2_INPUT_BIT;  
    SCLOutput = SCL2_OUTPUT_BIT; 
    SDAInput  = SDA2_INPUT_BIT;
    SDAOutput = SDA2_OUTPUT_BIT;
  }

  i2cWake();
}

void i2cBitDelay() {
 // wait_us(5);
}

void i2cSCLLow() {
  *SCLOutput = 0;
}

void i2cSCLHigh()
{
  *SCLOutput = 1;
}

void i2cSDALow()
{
  *SDAOutput = 0;
}

void i2cSDAHigh()
{
  *SDAOutput = 1;
}


void i2cWake()
{
  i2cSDALow();
  wait(10);
  i2cSDAHigh();
  wait(10);
}

void i2cStart()
{
  i2cSDALow();
  i2cBitDelay();
  i2cSCLLow();
}

void i2cStop()
{
  i2cSDALow();
  i2cBitDelay();
  i2cSCLHigh();
  //while (!(i2c_PIN & _BV(i2c_SCL)));
  while (!*SCLInput);
  i2cBitDelay();
  i2cSDAHigh();
  i2cBitDelay();
}

void i2cSendBit(uint8_t bit) {
  if (bit) {
    i2cSDAHigh();
  } else {
    i2cSDALow();
  }
  i2cBitDelay();
  i2cSCLHigh();
  i2cBitDelay();
  //while (!(i2c_PIN & _BV(i2c_SCL)));
  while (!*SCLInput);	
  i2cSCLLow();
}

uint8_t i2cRecvBit()
{
  uint8_t bit;
	
  i2cSDAHigh();
  i2cBitDelay();
  i2cSCLHigh();
  //while (!(i2c_PIN & _BV(i2c_SCL)));
  while (!*SCLInput);
  i2cBitDelay();
  //bit = (i2c_PIN & _BV(i2c_SDA)) ? 1 : 0;
  bit = (*SDAInput) ? 1 : 0;
  i2cSCLLow();
  return bit;
}

uint8_t i2cSendByte(uint8_t byte)
{
  for (uint8_t i = 0; i < 8; i++) {
    i2cSendBit(byte & 0x80);
    byte <<= 1;
  }	
  return i2cRecvBit();
}

uint8_t i2cRecvByte(uint8_t nack) {
  uint8_t b = 0;
  for (uint8_t i = 0; i < 8; i++) {
    b <<= 1;
    b |= i2cRecvBit();
  }
  i2cSendBit(nack);
  return b;
}




/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////

/* 
   Send packet:
   addr    i2c address
   *pkt    buffer with data
   len     buffer length
*/
uint8_t i2cSendPkt(uint8_t addr, uint8_t *pkt, int len) {
  i2cStart();
  if (i2cSendByte(addr << 1)) {
    i2cStop();
    return 1;
  }
  while (len--) {
    if (i2cSendByte(*pkt++)) {
      break;
    }
  }
  i2cStop();
  return len;
}

/*
   Send extended packet:
   addr    i2c address
   *p1     buffer1 with data
   lp1     buffer1 length
   *p2     buffer2 with data
   lp2     buffer2 length
*/
uint8_t i2cSendExtPkt(uint8_t addr, uint8_t *p1, int lp1, uint8_t *p2, int lp2) {
  uint16_t crc = 0;
	
  i2cStart();
  if (i2cSendByte(addr << 1)) {
    i2cStop();
    return 1;
  }
  if (lp1--) {
    i2cSendByte(*p1++);
  }
  while (lp1--) {
    crc = feed_crc(crc, *p1);
    if (i2cSendByte(*p1++)) {
      break;
    }
  }
  while (lp2--) {
    crc = feed_crc(crc, *p2);
    if (i2cSendByte(*p2++)) {
      break;
    }
  }
  crc = reverse_bits(crc);
  i2cSendByte(crc & 0xff);
  i2cSendByte(crc >> 8);
  i2cStop();
  return 0;
}


/* 
   Receive packet:
   addr    i2c address
   *pkt    buffer with data
   len     buffer length
*/
uint8_t i2cRecvPkt(uint8_t addr, uint8_t *pkt, int len) {
  i2cStart();
  if (i2cSendByte((addr << 1) | 1)) {
    i2cStop();
    return 1;
  }
  while (len--) {
    *pkt++ = i2cRecvByte(!len);
  }
  i2cStop();
  return 0;
}


/*
   Receive extended packet:
   addr    i2c address
   *p1     buffer1 with data
   lp1     buffer1 length
   *p2     buffer2 with data
   lp2     buffer2 length
*/
uint8_t i2cRecvVariableLenPkt(uint8_t addr, uint8_t *pkt, int maxLen) {
  uint8_t len;
	
  i2cStart();
  if (i2cSendByte((addr << 1) | 1)) {
    i2cStop();
    return 0;
  }
  len = *pkt++ = i2cRecvByte(0);
  if (len < maxLen) {
    maxLen = len;
  }
  if (len > maxLen) {
    len = maxLen;
  }
  while (--maxLen) {
    *pkt++ = i2cRecvByte(!maxLen);
  }
  i2cStop();
  return len;
}



