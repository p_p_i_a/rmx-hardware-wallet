/*
* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef STORAGE_H
#define STORAGE_H

#include "flash_memory.h"


/* 
 * index 0 is for my jid, my pubkey and my login (instead of my metadata)
 * every metadata field position 31 contains letter G to be checked after decrypting
 * other fields are used for contacts
 */

/* XMPP data structure */
typedef struct{
  char jid[62];
  int16_t unread;
  uint8_t pubkey[32];
  uint8_t metadata[32];
} CONTACT;


/* MONERO setup structure, 288B */
typedef struct{
  char node_address[128];
  char node_password[64];
  char header[68];
  uint32_t local_blockheight;
  uint32_t local_tx_offset;
  uint32_t global_blockheight;
  uint32_t matches;
  uint64_t balance;
  uint32_t batch_size;
  uint32_t blocks;
} MONERO_NODE;

#define TXID_STORAGE_SIZE  3500//7860

/* monero data structure */
typedef struct{
  uint32_t savedBytes;
  /* TX ID's, unspent outputs  3808B + 4096B*/
  uint8_t TxImages[TXID_STORAGE_SIZE];
} MONERO_WALLET;

typedef struct {
  /*setup informations here 288B */
  MONERO_NODE  setup;            
  MONERO_WALLET wallet;
} MONERO_DATA;
 

/* Defined also in linker file */
extern uint8_t aes_input[];             //[4096]
extern uint8_t aes_output[];            //[4096]
extern CONTACT xmpp_data[];             //[4096]
extern MONERO_DATA monero_data[];
extern uint8_t aes_key_array[];         //[32];
extern uint8_t aes_init_vector[];       //[32];
extern uint8_t privkey[];               //[32];


/* Used for init (or wipe) the persistent storage on external flash */
void initPersistentStorage (void);


/* direction: true encrypt block, false decrypt block */
#define DECRYPT_BLOCK       0
#define ENCRYPT_BLOCK       1
bool cryptBlock (uint8_t direction);


/* Update storage - encrypt xmpp_data field, save it encrypted into flash */
void storageUpdate(uint8_t block);


/* test flash storage */
void testStorage();


#endif
