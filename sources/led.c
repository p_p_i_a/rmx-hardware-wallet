/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "led.h"
#include "MCHP_MEC170x.h"
#include "CEC1702_defines.h"

#define GPIO_140_OUTPUT_BIT   (*((volatile uint32_t*) ((((((GPIO140_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(16*4)+0x42000000) ))))


void ledInit(void) {
  GPIO140_PIN_CONTROL_REGISTER     = 0x240UL; // LED 1
}


void ledBlink(uint32_t delay) {  //toggle pin  ^= 1UL << 21
  GPIO_140_OUTPUT_BIT = 1UL;
  wait(delay);
  GPIO_140_OUTPUT_BIT = 0UL;
  wait(delay);
}


void errorBlinking(void) {
  while(1)
    ledBlink(MEDIUM);
}





