/* This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 76EC 7710 C597 C421 35CA F2EF F32D 7E64 6648 F1D7> <i_a@rmxwallet.io>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */

#include "led.h"
#include "MCHP_MEC170x.h"
#include "buttons.h"
#include "init.h"
#include "mec2016_rom_api.h"
#include "uart.h"
#include "systick.h"
#include "fifo.h"
#include "CEC1702_defines.h"
#include "efuse.h"
#include "i2c.h"

#include "irq.h"

#define GPIO047_INPUT_BIT   (*((volatile uint8_t*) ((((((GPIO047_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define GPIO162_INPUT_BIT   (*((volatile uint8_t*) ((((((GPIO162_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))
#define GPIO165_INPUT_BIT   (*((volatile uint8_t*) ((((((GPIO165_PIN_CONTROL_REGISTER_ADDRESS) - 0x40000000)*32)+(24*4)+0x42000000) ))))



void initCEC(void) {
 /* Init PKE block */	
  api_pke_power(true);
  api_pke_reset();  

  /* Init RNG Block */
  api_rng_power(true);
  api_rng_reset();
  api_rng_mode((uint8_t)0); // Asynchronous (true random mode)
  api_rng_start();
  while (!api_rng_is_on()); // wait for the rng block to start

  //* Init AES Block */
  //wait(100);
  api_aes_hash_power(true);
  api_aes_hash_reset();
  while(api_aes_busy())
    wait(1);

  //SHIELD DETECT --> uart_init();
  //fifoInit();
  ledInit();
  uart_init();
  oledInit();
  buttonsInit();
  vrefAdcInit();


  SysTick_Init();
  //GIRQ10_GIRQ11_init();
  __enable_irq(); // CPSIE I : Enable interrupt
  __ISB(); // Allow pended interrupts to be recognized
}























