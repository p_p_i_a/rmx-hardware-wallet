/*
* This file is a part of RMX HARDWARE WALLET
*
* This program is used exclusively under the belowmentioned license provided by 
* Microchip Technology Inc. and its subsidiaries and with the full compliance 
* with it. It is your responsibility to comply with these license terms:
*
* © 2018 Microchip Technology Inc. and its subsidiaries.
*
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your
* responsibility to comply with third party license terms applicable to your
* use of third party software (including open source software) that may
* accompany Microchip software.
*
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".
* NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
* INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
* AND FITNESS FOR A PARTICULAR PURPOSE.
*
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.
* TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
* CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT
* OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*****************************************************************************/

#ifndef INCLUDE_MEC2016_ROM_API_H_
#define INCLUDE_MEC2016_ROM_API_H_


#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/* QMSPI */

#define SPI_RW_CMD_MAX_PARAM  (8)

typedef enum
{
    QMSPI_DESCR0 = 0u,
    QMSPI_DESCR1 = 1u,
    QMSPI_DESCR2 = 2u,
    QMSPI_DESCR3 = 3u,
    QMSPI_DESCR4 = 4u,
    QMSPI_DESCR_MAX = 5u
} QMSPI_DESCR_ID;


typedef struct spi_rw_cmd {
    uint8_t param[SPI_RW_CMD_MAX_PARAM];
} SPI_RW_CMD;

typedef uint32_t (*FP_RU32_V)(void);

typedef struct qmspi_ctx {
    uint8_t iopha;
    uint8_t freq_div;
    uint8_t ifc;
    uint8_t rx_dma_chan;
    uint8_t tx_dma_chan;
    uint8_t flags;
    uint8_t nctx;
    uint8_t rsvdA[1];
    uint32_t nd;
    uint32_t descr[QMSPI_DESCR_MAX];
} QMSPI_CTX;



/**
 *  \name spi_port_sel
 *
 *  \param [in] port Parameter_Description
 *  \param [in] pin_mask Parameter_Description
 *  \param [in] en Parameter_Description
 *  \return Return_Description
 *
 *  \details This function controls the SPI port.
 */
extern void
spi_port_sel( uint8_t port,
              uint8_t pin_mask,
              bool    en );


/**
 *  \name spi_port_drv_slew
 *
 *  \param [in] port Parameter_Description
 *  \param [in] pin_mask Parameter_Description
 *  \param [in] drv_slew Parameter_Description
 *  \return Return_Description
 *
 *  \details Details
 */
extern void
spi_port_drv_slew( uint8_t port,
                   uint8_t pin_mask,
                   uint8_t drv_slew );




/**
 *  \name qmspi_init
 *
 *  \param [in] freq_hz Parameter_Description
 *  \param [in] spi_signalling Parameter_Description
 *  \param [in] ifctrl Parameter_Description
 *  \return None
 *
 *  \details This function initialises the QMSPI interface.
 */
extern void
qmspi_init( uint32_t freq_hz,
            uint8_t  spi_signalling,
            uint8_t  ifctrl );


/**
 *  \name qmspi_freq_get
 *  \param None
 *  \return Frequency of SPI
 *
 *  \details This function returns the frequency of SPI interface.
 */
extern uint32_t
qmspi_freq_get( void );


/**
 *  \name qmspi_freq_set
 *
 *  \param [in] freq_hz Parameter_Description
 *  \return None
 *
 *  \details This function is used to set frequency to QMSPI interface.
 */
extern void
qmspi_freq_set( uint32_t freq_hz );


/**
 *  \name qmspi_xfr_done_status
 *
 *  \param [in] hw_status Parameter_Description
 *  \return Returns boolean value of QMSPI status, true if busy, false otherwise.
 *
 *  \details This function is used to read the status of a QMSPI transaction.
 */
extern bool
qmspi_xfr_done_status( uint32_t* hw_status );


/**
 *  \name qmspi_start
 *
 *  \param [in] ien_mask Parameter_Description
 *  \return None
 *
 *  \details This function is used to start the qmspi module.
 */
extern void
qmspi_start( uint16_t ien_mask );


/**
 *  \name qmspi_read_dma
 *
 *  \param [in] spi_cmd Refer API manual for spi_cmd bit definitions
 *  \param [in] spi_address Address of SPI flash from where data is to be read
 *  \param [in] mem_addr Address of SRAM destination for data read
 *  \param [in] nbytes Parameter_Description
 *  \param [in] dma_chan Parameter_Description
 *  \return Number of bytes read
 *
 *  \details Details
 */
extern uint32_t
qmspi_read_dma( uint32_t spi_cmd,
                uint32_t spi_address,
                uint32_t mem_addr,
                uint32_t nbytes,
                uint8_t  dma_chan );


/**
 *  \name api_qmspi_port_ctrl
 *
 *  \param [in] port_id
 *  \param [in] width_mask
 *  \param [in] enable
 *  \return Number of bytes read
 *
 *  \details Details
 */
extern uint8_t
api_qmspi_port_ctrl (uint8_t port_id,
                     uint8_t width_mask,
                     uint8_t enable);


/**
 *  \name api_qmspi_port_drv_slew
 *
 *  \param [in] port_id
 *  \param [in] width_mask
 *  \param [in] drv_slew
 *  \return Return_Description
 *
 *  \details Details
 */
extern uint8_t
api_qmspi_port_drv_slew (uint8_t port_id,
                         uint8_t width_mask,
                         uint8_t drv_slew);


/**
 *  \name api_qmspi_is_done
 *
 *  \return 1 - operation is done; 0 - otherwise
 *
 *  \details checks if the operation has been completed
 */
extern uint8_t
api_qmspi_is_done(void);



/**
 *  \name api_qmspi_is_done
 *
 *  \param [in] pstatus pointer where the status value will be stored
 *
 *  \return 1 - operation is done; 0 - otherwise
 *
 *  \details updates the status value of the QMSPI block in the pointer argument
 */
extern uint8_t
api_qmspi_is_done_status(uint32_t *pstatus);



/**
 *  \name api_qmspi_is_done
 *
 *  \param [in] qctx pointer to a QMSPI_CTX  data structure
 *
 *  \details performs a soft reset
 */
extern void
api_qmspi_soft_reset (QMSPI_CTX *qctx);



/**
 *  \name api_qmspi_flash_read24_dma
 *
 *  \param [in] cmd_id
 *  \param [in] spi_addr Address of SPI flash from where data is to be read
 *  \param [in] nbytes number of bytes of data to be read from spi_addr
 *  \param [in] maddr Address of SRAM destination for data read
 *  \return
 *
 *  \details reads the specified number of bytes from the given flash address and stores in SRAM
 */
extern uint32_t
api_qmspi_flash_read24_dma (uint8_t cmd_id,
                            uint32_t spi_addr,
                            uint32_t nbytes,
                            uint32_t maddr);


/**
 *  \name api_qmspi_flash_cmd
 *
 *  \param [in] ntx
 *  \param [in] ptx
 *  \param [in] nrx
 *  \param [in] prx
 *  \param [in] ftmout A variable of the type FP_RU32_V
 *  \return
 *
 *  \details
 */

extern uint8_t
api_qmspi_flash_cmd (uint32_t ntx,
                     uint8_t *ptx,
                     uint32_t nrx,
                     uint8_t *prx,
                     FP_RU32_V ftmout);



/**
 *  \name api_qmspi_flash_program_dma
 *
 *  \param [in] prog_cmd
 *  \param [in] spi_addr
 *  \param [in] nbytes
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_flash_program_dma (uint8_t prog_cmd,
                             uint32_t spi_addr,
                             uint32_t nbytes);


/**
 *  \name api_qmspi_ctx_init
 *
 *  \param [in] qctx pointer to a QMSPI_CTX  data structure
 *  \param [in] fdiv
 *  \param [in] ioph_ifc
 *  \param [in] dmarxtx
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_ctx_init (QMSPI_CTX *qctx,
                    uint8_t fdiv,
                    uint32_t ioph_ifc,
                    uint32_t dmarxtx);


/**
 *  \name api_qmspi_init
 *
 *  \param [in] spi_mode
 *  \param [in] freq_div
 *  \param [in] ifc
 *
 *  \details
 */
extern void
api_qmspi_init (uint8_t spi_mode,
                uint8_t freq_div,
                uint16_t ifc);



/**
 *  \name api_qmspi_ctx_freq_set
 *
 *  \param [in] qctx pointer to a QMSPI_CTX  data structure
 *  \param [in] fdiv
 *
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_ctx_freq_set (QMSPI_CTX *qctx,
                        uint8_t fdiv);


/**
 *  \name api_qmspi_ctx_flash_rw_cmd
 *
 *  \param [in] qc pointer to a QMSPI_CTX  data structure
 *  \param [in] c pointer to a SPI_RW_CMD  data structure
 *  \param [in] spi_addr address of the SPI flash from where data is to be read.
 *
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_ctx_flash_rw_cmd (QMSPI_CTX *qc,
                            const SPI_RW_CMD *c,
                            uint32_t spi_addr);


/**
 *  \name api_qmspi_ctx_flash_rw_data_dma
 *
 *  \param [in] qc pointer to a QMSPI_CTX  data structure
 *  \param [in] c pointer to a SPI_RW_CMD  data structure
 *  \param [in] mbuf
 *  \param [in] nbytes
 *
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_ctx_flash_rw_data_dma (QMSPI_CTX *qc,
                                 const SPI_RW_CMD *c,
                                 void *mbuf,
                                 uint32_t nbytes);


/**
 *  \name api_qmspi_ctx_flash_rd_data_blocking
 *
 *  \param [in] qc pointer to a QMSPI_CTX  data structure
 *  \param [in] c pointer to a SPI_RW_CMD  data structure
 *  \param [in] mbuf
 *  \param [in] nbytes
 *
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_ctx_flash_rd_data_blocking (QMSPI_CTX *qc,
                                      const SPI_RW_CMD *c,
                                      void *mbuf,
                                      uint32_t nbytes);


/**
 *  \name api_qmspi_ctx_start
 *
 *  \param [in] qc pointer to a QMSPI_CTX  data structure
 *  \param [in] ien_mask
 *
 *  \return
 *
 *  \details
 */
extern uint8_t
api_qmspi_ctx_start (QMSPI_CTX *qc,
                     uint16_t ien_mask);



/**
 *  \name api_qmspi_start
 *
 *  \param [in] ien_mask integer specifying the interrupt mask
 *  \return None
 *
 *  \details This function is used to start the qmspi module with the specified interrupt mask.
 */
extern void
api_qmspi_start (uint16_t ien_mask);


/**
 *  \name api_dma_dev_xfr_cfg
 *
 *  \param [in] chan_id
 *  \param [in] dev_id
 *  \param [in] maddr
 *  \param [in] nbytes
 *  \param [in] flags
 *
 *  \return
 *
 *  \details
 */
extern uint8_t
api_dma_dev_xfr_cfg (uint8_t chan_id,
                     uint32_t dev_id,
                     uint32_t maddr,
                     uint32_t nbytes,
                     uint32_t flags);



/* RNG */

/**
 *  \name api_rng_power
 *
 *  \param [in] pwr_on Power On?
 *  \return none
 *
 *  \details Gate clocks on/off to NDRNG block
 */
extern void
api_rng_power(bool pwr_on);


/**
 *  \name api_rng_reset
 *
 *  \return None
 *
 *  \details Reset NDRNG block
 */
extern void
api_rng_reset(void);


/**
 *  \name api_rng_mode
 *
 *  \param [in] mode tmode_pseudo 0(asynchronous/true random mode),
 *              Non-zero(pseudo-random mode)
 *  \return None
 *
 *  \details Set NDRNG random number generation mode
 */
extern void
api_rng_mode( uint8_t mode );


/**
 *  \name api_rng_is_on
 *
 *  \return is NDRNG Block powered on? True if yes, false otherwise
 *
 *  \details Check if NDRNG block is powered on.
 */
extern bool
api_rng_is_on( void );


/**
 *  \name api_rng_start
 *
 *  \return None
 *
 *  \details Start NDRNG engine
 */
extern void
api_rng_start(void);


/**
 *  \name api_rng_stop
 *
 *  \return Void
 *
 *  \details Stop NDRNG engine
 */
extern void
api_rng_stop(void);


/** rng_get_random_bytes - Fill a buffer with random bytes
 *
 * \param pointer to byte buffer
 * \param number of random bytes to retrieve. Must be less or equal to
 * the size of the buffer.
 * \return number of random bytes retrieved.
 * \details NDRNG is powered on, one word(32-bit) of random data read
 * at a time, and bytes written to buffer. NDRNG is powered off
 * before returning.
 *
 */
extern uint32_t
api_rng_get_random_bytes( uint8_t* pbuff8,
							uint32_t num_bytes);


/**
 * api_rng_get_random_words - Reads specified number of random
 * words (32-bit) into buffer supplied by caller.
 *
 * \param pbuff32 pointer to word(32-bit) aligned SRAM buffer.
 * \param num_words number of 32-bit words of random data to
 *                  read.
 *
 * \return uint32_t actual number of words read.
 * \note This is an all-in-one routine. Powers on the NDRNG,
 *       starts the NDRNG, polls the FIFO level and reads words
 *       from the FIFO only if its is not empty. Loops until the
 *       specified number of words is read. No time out is
 *       implemented, if the NDRNG FIFO hardware stops filling
 *       then this routine will loop forever.
 */
extern uint32_t
api_rng_get_random_words(uint32_t* pbuff32,
												 uint32_t num_words);

/**
 * api_rng_get_num_random_words - Read the NDRNG FIFO level register
 * and return the number of 32-bit words of random data
 * currently in the FIFO.
 *
 *
 * @return uint32_t number of 32-bit words in the NDRNG FIFO.
 *         Maximum value is 32 (32x32 = 1024 bits).
 * @note Firmware should call this function before reading the
 *       FIFO and only read if this function returns non-zero.
 */
extern uint32_t
api_rng_get_num_random_words(void);

/* AES */

#define AES_OK                          (0)
#define AES_ERR_BUSY                    (1)
#define AES_ERR_BAD_KEY_LEN             (2)
#define AES_ERR_BAD_POINTER             (3)
#define AES_ERR_MISALIGNED_DATA         (4)
#define AES_ERR_UNSUPPORTED_OP          (5)

#define AES_MODE_ECB        (0ul)
#define AES_MODE_CBC        (1ul)
#define AES_MODE_CTR        (2ul)
#define AES_MODE_CFB        (3ul)
#define AES_MODE_OFB        (4ul)
#define AES_MODE_CCM        (5ul)
#define AES_MODE_GCM        (6ul)
#define AES_MODE_XTS        (7ul)
#define AES_MODE_CMAC       (8ul)
#define AES_MODE_DECRYPT    (0x80ul)

#define AES_KEYLEN_128          (0ul)
#define AES_KEYLEN_192          (1ul)
#define AES_KEYLEN_256          (2ul)


/**
 *  \name api_aes_hash_power
 *
 *  \param [in] pwr_on Gate/Ungate clocks to block
 *  \return None
 *
 *  \details Enable/Disable AES and HASH H/W Block
 */

extern void
api_aes_hash_power( uint8_t pwr_on );


/**
 *  \name api_aes_hash_reset
 *
 *  \return None
 *
 *  \details Stop AES and HASH
 */
extern void
api_aes_hash_reset( void );


/**
 *  \name api_aes_busy
 *
 *  \return Is AES Block Running? 0 if yes, non-zero value otherwise
 *
 *  \details Is AES Block Running?
 */

extern uint8_t
api_aes_busy( void );


/**
 *  \name api_aes_status
 *
 *  \return Status of AES Block
 *
 *  \details Returns the Status of AES Block bit 0 -> AES busy status(1- busy, 0 - ready),
 * 		     bit 1 -> AES CCM Mode MAC_T calculation valid(1- valid, 0 - not supported),
 * 			 bit 2 -> DMA error(1 - error)
 */
extern uint32_t
api_aes_status( void );


/**
 *  \name api_aes_is_done_status2
 *
 *  \param [in] hw_status Pointer to where the status value will be updated, bit 0 - busy status,
                          bit 1 - AES CCM Mode MAC_T calculation valid, bit 2 - DMA error
 *  \return 0 if done, non-zero value otherwise.
 *
 *  \details Returns the done status of AES block
 */
extern uint8_t
api_aes_is_done_status2(uint32_t* status_value);

/**
 *  \api_aes_stop
 *
 *  \return Return api_aes_busy() Status
 *
 *  \details Stop AES Operations
 */
extern uint8_t
api_aes_stop( void );


/**
 *  \name api_aes_start
 *
 *  \param [in] ien Enable interrupts?
 *  \return None
 *
 *  \details Start AES block with or without interrupts
 */
extern void
api_aes_start( uint8_t ien );


/**
 *  \name api_aes_iclr
 *
 *  \return Status of the AES Block
 *
 *  \details Clears AES Hash Interrupts
 */

extern uint32_t
api_aes_iclr( void );

/**
 *  \name api_aes_set_key_iv
 *
 *  @param pointer to word (32-bit) aligned buffer containing the AES key, LSB first.
 * 	@param pointer to word (32-bit) aligned buffer containing AES initialization
 * 		vector, LSB first. NULL if no initialization vector required.
 *	@param [in] key_len AES_KEYLEN_128, AES_KEYLEN_192, AES_KEYLEN_256
 *	@param [in] msbf Most Significant Byte order first?
 * 	@return AES_OK(success), AES_ERR_BAD_POINTER(pkey is NULL),
 * 		AES_ERR_BAD_KEY_LEN(key_len not supported),
 *
 *  \details Load AES Accelerator with key and optional Initialization vector
 */
extern uint8_t
api_aes_set_key(const uint32_t *pkey,
                       const uint32_t *piv,
                       uint8_t key_len,
                       uint8_t msbf);
/**
 *  \name api_aes_prog_key
 *
 *  \param [in] pkey Aligned buffer with AES Key
 *  \param [in] key_len AES_KEYLEN_128, AES_KEYLEN_192, AES_KEYLEN_256
 *  \param [in] msbf Most Significant Byte order first?
 *  \return AES_ERR_BAD_POINTER, AES_ERR_BAD_KEY_LEN, AES_OK
 *
 *  \details Load AES Accelerator with key
 */
extern uint8_t
api_aes_prog_key( const uint8_t *pkey,
                        uint8_t key_len,
                        uint8_t msbf);

/**
 *  \name api_aes_prog_iv
 *
 *  \param [in] piv Aligned buffer with AES initialization vector
 *  \param [in] iv_len Length of the AES initialization vector
 *  \param [in] msbf Most Significant Byte order first?
 *  \return AES_ERR_BAD_POINTER, AES_ERR_BAD_KEY_LEN, AES_OK
 *
 *  \details Load AES Accelerator with iv
 */
extern uint8_t
api_aes_prog_iv( const uint8_t *piv,
                        uint8_t iv_len,
                        uint8_t msbf);


/**
 *  \name api_aes_crypt
 *
 *  \param [in] data_in Aligned input data Buffer
 *  \param [in] data_out Aligned output data buffer
 *  \param [in] num_128bit_blocks Size of input in 16-byte blocks
 *  \param [in] mode AES Encryption/Decryption Mode
 *  \return AES_OK, AES_ERR_BAD_POINTER,
 *
 *  \details Program specified AES Operation using currently programmed key
 */
extern uint8_t
api_aes_crypt( const uint32_t* data_in,
                 uint32_t* data_out,
                 uint32_t  num_128bit_blocks,
                 uint8_t   mode );

/* SHA */

//#define SHA1_BLEN           (20u)
//#define SHA1_WLEN           (5u)
#define SHA2_BLEN           (32u)
#define SHA2_WLEN           (8u)
#define SHA12_BLOCK_BLEN    (64u)
#define SHA12_BLOCK_WLEN    (16u)
//#define SHA3_BLEN           (48u)
//#define SHA3_WLEN           (12u)
#define SHA5_BLEN           (64u)
#define SHA5_WLEN           (16u)
//#define SHA35_BLOCK_BLEN    (128u)
//#define SHA35_BLOCK_WLEN    (32u)
#define SHA5_BLOCK_BLEN     (128u)
#define SHA256_BLEN         (32u)
#define SHA256_WLEN         (8u)

#define SHA256_BLOCK_BLEN   (64u)
#define SHA256_BLOCK_WLEN   (16u)

/* return values */
#define SHA_RET_OK                      (0) /* OK */
#define SHA_RET_START                   (1) /* OK, SHA Engine started */
#define SHA_RET_ERROR                   (0x80)  /* b[7]==1 indicates an error */
#define SHA_RET_ERR_BUSY                (0x80)
#define SHA_RET_ERR_BAD_ADDR            (0x81)
#define SHA_RET_ERR_TIMEOUT             (0x82)
#define SHA_RET_ERR_MAX_LEN             (0x83)
#define SHA_RET_ERR_UNSUPPORTED         (0x84)
#define SHA_RET_ERR_MISALIGNED_DATA     (0x85)
#define SHA_RET_ERR_NULL_PTR            (0x86)
#define SHA_RET_ERR_MAX_REM             (0x87)



#define SHA_MODE_MD5    (0) // Not supported by HW
#define SHA_MODE_1      (1)
#define SHA_MODE_224    (2) // Not supported by HW
#define SHA_MODE_256    (3)
#define SHA_MODE_384    (4) // Not supported by HW
#define SHA_MODE_512    (5)

#define HASH_START_IEN      (1u)
#define HASH_START_NOIEN    (0u)

#define SHA5_BLOCK_BUF_LEN (SHA5_BLOCK_BLEN + 8) // 128 + 8 = 136

typedef union {
    uint32_t w[SHA2_WLEN];
    uint8_t  b[SHA2_BLEN];
} SHA12_DIGEST_U;


typedef union {
    uint32_t w[SHA5_WLEN];
    uint8_t  b[SHA5_BLEN];
} SHA35_DIGEST_U;


/*
 * !!! SHA-1 & SHA-256
 * HW Engine requires alignment >= 4-byte boundary !!!
 */
typedef struct sha12_context_s SHA12_CONTEXT_T;
struct sha12_context_s {
    union {
        uint32_t w[SHA256_WLEN];
        uint8_t  b[SHA256_BLEN];
    } digest;
    uint32_t mode;
    uint32_t block_len;
    uint64_t msg_byte_len;
    union {
        uint32_t w[SHA256_BLOCK_WLEN];
        uint8_t  b[SHA256_BLOCK_BLEN];
    } block;
};




typedef struct sha5_context_s SHA5_CONTEXT_T;
struct sha5_context_s {
    union {
        uint8_t b[SHA5_BLEN + 8];
        uint32_t w[(SHA5_BLEN)/4 + 2];
    } digest;
    uint32_t *pdigest;
    uint32_t *pblock;
    uint32_t mode;
    uint32_t block_len;
    uint16_t msg_blen[8];	// 128 bits
    uint8_t block[SHA5_BLOCK_BUF_LEN];
};


/**
 *  \name api_hash_busy
 *
 *  \return is busy? True if yes, Flase other wise
 *
 *  \details returns the busy status of Hash Block
 */
extern bool
api_hash_busy( void );

/**
 *  \name api_hash_start
 *
 *  \param [in] ien enable/disable interrupts
 *  \return None
 *
 *  \details start hash block
 */
extern void
api_hash_start (uint8_t ien);

/**
 *  \name api_hash_status
 *
 *  \param [in] None
 *  \return Status of hash block, hash_status = 1 (AHB error), else 0.
 *
 *  \details Hash status
 */
extern uint32_t
api_hash_status( void );

/**
 *  \name api_hash_is_done_status
 *
 *  \param [in] hw_status Pointer to 32 bit integer to hold Hash Status Register Value; bits31:16 - hash status register,
 *							bits 15:0 - hash control register
 *  \return true if done, false otherwise
 *
 *  \details reflects the done status of HASH black and updates
 *		status regsiter value into the input variable
 */
extern uint8_t
api_hash_is_done_status (uint32_t *status_value);


/**
 *  \name api_hash_is_done_status2
 *
 *  \param [in] hw_status Pointer to 32 bit integer to hold Hash Status Register Value; bit[0] = Hash Control Start/Busy bit[0]
 * bit[4] = Hash Status register bit[0] (AHB Error), bit[15] = Hash Done
 *  \return true if done, false otherwise
 *
 *  \details Return true if Hash engine is done(stopped).
 *		if passed pointer is not null then store Hash HW status into the input variable
 */
extern uint8_t
api_hash_is_done_status2(uint32_t *status_value);

/**
 *  \name api_sha_direct_init
 *
 *  \param [in] sha_mode SHA_MODE_1 or SHA_MODE_256 or SHA_MODE_512
 *  \param [in] pbuff pointer to the buffer containing the SHA digest and padding
 *  \return SHA_RET_ERR_BUSY, SHA_RET_ERR_UNSPPORTED, SHA_RET_OK, SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_MISALIGNED_DATA
 *
 *  \details
 */
extern uint8_t
api_sha_direct_init (uint8_t sha_mode,
                     uint32_t *pbuff);


/**
 *  \name api_sha_direct_update
 *
 *  \param [in] sha_mode SHA_MODE_1 or SHA_MODE_256 or SHA_MODE_512
 *  \param [in] pbuff
 *  \param [in] pblocks
 *  \param [in] nblocks
 *  \param [in] flags
 *  \return SHA_RET_ERR_BUSY, SHA_RET_ERR_UNSPPORTED, SHA_RET_OK, SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_MISALIGNED_DATA
 *
 *  \details programs hash engine with data address and the number of data blocks to process
 */
extern uint8_t
api_sha_direct_update (uint8_t sha_mode,
                       uint32_t *pbuff, uint32_t *pblocks,
                       uint32_t nblocks, uint32_t flags);


/**
 *  \name api_sha_direct_finalize
 *
 *  \param [in] sha_mode SHA_MODE_1 or SHA_MODE_256 or SHA_MODE_512
 *  \param [in] pbuff
 *  \param [in] total_mesg_byte_len
 *  \param [in] nrem
 *  \param [in] prembytes
 *  \return SHA_RET_ERR_BUSY, SHA_RET_ERR_UNSPPORTED, SHA_RET_OK, SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_MISALIGNED_DATA, SHA_RET_ERR_MAX_REM
 *
 *  \details Apply FIPS padding and perform final hash calculation.
 */
extern uint8_t
api_sha_direct_finalize (uint8_t sha_mode,
                         uint32_t *pbuff,
                         uint32_t total_mesg_byte_len,
                         uint32_t nrem,
                         uint8_t *prembytes);


/**
 *  \name api_sha1_init
 *
 *  \param [in] sha12_ctx Data structure for input message and Digest output
 *  \return SHA_RET_ERR_BAD_ADDR, SHA_RET_OK
 *
 *  \details Initialize the data structure provided
 */
extern uint8_t
api_sha1_init (SHA12_CONTEXT_T* sha12_ctx );


/**
 *  \name api_sha1_update
 *
 *  \param [in] sha12_ctx Data structure for input message and Digest output
 *  \param [in] data Input to Hash block
 *  \param [in] nbytes
 *  \return SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_BUSY, SHA_RET_OK
 *
 *  \details programs hash engine with data address and the number of data blocks to process
 */
extern uint8_t
api_sha1_update (SHA12_CONTEXT_T *sha12_ctx,
                 const uint8_t *data,
                 uint32_t nbytes);

/**
 *  \name sha35_finalize
 *
 *  \param [in] sha12_ctx Data structure for input message and Digest output
 *  \return SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_BUSY, SHA_RET_OK
 *
 *  \details Apply FIPS padding and perform final hash calculation.
 */
extern uint8_t
api_sha1_finalize (SHA12_CONTEXT_T *sha12_ctx);


/**
 *  \name api_hash_iclr
 *
 *  \return Hash Block status
 *
 *  \details Clear Hash Interrupt
 */
extern uint32_t
api_hash_iclr(void);


/**
 *  \name api_sha256_init
 *
 *  \param [in] sha12_ctx Data structure for input message and Digest output
 *  \return SHA_RET_ERR_BAD_ADDR, SHA_RET_OK
 *
 *  \details Initialize the data structure provided
 */
extern uint8_t
api_sha256_init (SHA12_CONTEXT_T* sha12_ctx );

/**
 *  \name api_sha256_update
 *
 *  \param [in] sha12_ctx Data structure for input message and Digest output
 *  \param [in] data Input to Hash block
 *  \param [in] nbytes
 *  \return SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_BUSY, SHA_RET_OK
 *
 *  \details programs hash engine with data address and the number of data blocks to process
 */
extern uint8_t
api_sha256_update (SHA12_CONTEXT_T *sha12_ctx,
                   const uint8_t *data,
                   uint32_t nbytes);

/**
 *  \name api_sha256_finalize
 *
 *  \param [in] sha12_ctx Data structure for input message and Digest output
 *  \return SHA_RET_ERR_NULL_PTR, SHA_RET_ERR_BUSY, SHA_RET_OK
 *
 *  \details Apply FIPS padding and perform final hash calculation.
 */
extern uint8_t
api_sha256_finalize (SHA12_CONTEXT_T * sha12_ctx);

/**
 *  \name api_sha256_under56
 *
 *  \param [in] pdigest pointer to digest
 *  \param [in] pdata56 pointer to the input data which is less than 56 bytes
 *  \param [in] nbytes length of pdata56 in bytes
 *  \param [in] flags
 *  \return 0-Failure, 1-Success
 *
 *  \details computes SHA256 for input data less than 56 bytes.
 */
extern uint8_t
api_sha256_under56 (uint32_t *pdigest,
                    uint32_t *pdata56,
                    uint8_t nbytes,
                    uint8_t flags);

/**
 *  \name api_sha256_block_init
 *  \param [in] pdigest pointer to digest
 *  \return  	0 = failure
 * 				1 = success
 *  \details 	Initialize Hash engine for SHA operation.
 */
extern uint8_t
api_sha256_block_init(uint32_t *pdigest);

/**
 *  \name api_sha256_block_update
 *
 *  \param [in] pblocks Input Data
 *  \param [in] nblocks Size of input data in bytes
 *  \param [in] flags
 *  \return 0 - failure, 1 - success
 *
 *  \details Run Hash block on data
 */
extern uint8_t
api_sha256_block_update(uint32_t *pblocks,
                        uint32_t nblocks,
                        uint32_t flags);

/**
 *  \name api_sha256_block_finalize
 *
 *  \param [in] ppadbuf Buffer for padding
 *  \param [in] total_nblocks
 *  \param [in] flags bit(0)
 *  \return 0 - OK, 1 - Hash Busy, 2 - bad address for data, 3 - Buffer not aligned
 *
 *  \details Run final SHA Calculations and add padding
 */
extern uint8_t
api_sha256_block_finalize(uint32_t *ppadbuf,
                          uint32_t total_nblocks,
                          uint32_t flags);

/**
 *  \name api_sha512_init
 *
 *  \param [in] sha5_ctx Data structure for input message and Digest output
 *  \return  	SHA_RET_ERR_BAD_ADDR,SHA_RET_OK
 *  \details 	Initialize Hash engine for SHA operation.
 */
extern uint8_t
api_sha512_init (SHA5_CONTEXT_T *sha5_ctx);


/**
 *  \name api_sha512_update
 *
 *  \param [in] sha5_ctx Data structure for input message and Digest output
 *  \param [in] data pointer to the data
 *  \param [in] nbytes
 *  \return SHA_RET_ERR_NULL_PTR, SHA_RET_OK, SHA_RET_ERR_BUSY
 *
 *  \details Run Hash block on data
 */
extern uint8_t
api_sha512_update (SHA5_CONTEXT_T *sha5_ctx,
                   const uint8_t *data,
                   uint32_t nbytes);


/**
 *  \name api_sha512_finalize
 *
 *  \param [in] sha5_ctx Data structure for input message and Digest output
 *
 *  \details Run final SHA Calculations and add padding
 */
extern uint8_t
api_sha512_finalize (SHA5_CONTEXT_T *sha5_ctx);


/* PKE Miscellaneous */

#define PKE_RET_STARTED                         (0)
#define PKE_RET_OK                              (0)
#define PKE_RET_ERR_BUSY                        (1)
#define PKE_RET_ERR_BAD_PARAM                   (2)
#define PKE_RET_ERR_BAD_ADDR                    (3)
#define PKE_RET_ERR_UNKNOWN_OP                  (4)
#define PKE_RET_ERR_INVALID_BIT_LENGTH          (5)
#define PKE_RET_ERR_INVALID_MSG_LENGTH          (6)

/*PKE status register bit definitions*/
#define PKE_STATUS_MASK                 (0x00011EFF)
#define PKE_STATUS_BUSY_BITPOS          (16u)
#define PKE_STATUS_BUSY                 (1UL << 16)
#define PKE_STATUS_PRIME_IS_COMPOSITE   (1UL << 12)
#define PKE_STATUS_NOT_INVERTIBLE       (1UL << 11)
#define PKE_STATUS_PARAM_AB_INVALID     (1UL << 10)
#define PKE_STATUS_SIG_NOT_VALID        (1UL << 9)
#define PKE_STATUS_PARAM_N_INVALID      (1UL << 7)
#define PKE_STATUS_COUPLE_INVALID       (1UL << 6)
#define PKE_STATUS_PX_AT_INFINITY       (1UL << 5)
#define PKE_STATUS_PX_NOT_ON_CURVE      (1UL << 4)
#define PKE_STATUS_FAIL_ADDR_MASK       (0x0Ful)

#define PKE_SRP_FLAG_LSBF   (0u << 0)
#define PKE_SRP_FLAG_MSBF   (1u << 0)
#define PKE_MAX_SRP_PARAM   (7u)
#define PKE_SRP_PARAM_P     (0u)
#define PKE_SRP_PARAM_G     (1u)
#define PKE_SRP_PARAM_A     (2u)
#define PKE_SRP_PARAM_B     (3u)
#define PKE_SRP_PARAM_X     (4u)
#define PKE_SRP_PARAM_K     (5u)
#define PKE_SRP_PARAM_U     (6u)

typedef struct {
    uint16_t len64b;
    uint16_t flags;
    uint8_t* param[PKE_MAX_SRP_PARAM];
} PKE_SRP_DATA;

typedef struct pke_big_int PKE_BIG_INT;
struct pke_big_int
{
    uint16_t flags;     /*!< flags bit[0]=0(LSBF), 1(MSBF) bit[7]=0(positive), 1(negative) */
    uint16_t blen;      /*!< byte length */
    uint8_t  *pbytes;   /*!< pointer to bytes */
};

typedef struct rsa_pub_key RSA_PUB_KEY;
struct rsa_pub_key {
    uint32_t    key_bitlen;     /*!< key bit length 1024, 2048, 3072, or 4096 */
    PKE_BIG_INT pub_mod;        /*!< RSA Public Modulus as PKE_BIG_INT */
    PKE_BIG_INT pub_exp;        /*!< RSA Public Exponent as PKE_BIG_INT */
};

typedef struct rsa_key RSA_KEY;
struct rsa_key {
    RSA_PUB_KEY pub_key;        /*!< RSA Public Key */
    PKE_BIG_INT prv_exp;        /*!< RSA Private Exponent as PKE_BIG_INT */
};


typedef struct rsa_crt_key RSA_CRT_KEY;
struct rsa_crt_key {
    uint32_t    key_bitlen;    /*!< key bit length 1024, 2048, 3072, or 4096 */
    PKE_BIG_INT prime1;        /*!< RSA prime 1 as PKE_BIG_INT */
    PKE_BIG_INT prime2;        /*!< RSA prime 2 as PKE_BIG_INT */
    PKE_BIG_INT crt_dp;        /*!< RSA CRT dP as PKE_BIG_INT */
    PKE_BIG_INT crt_dq;        /*!< RSA CRT dQ as PKE_BIG_INT */
    PKE_BIG_INT crt_i;         /*!< RSA CRT I as PKE_BIG_INT */
};


/**
 *  \name api_pke_power
 *
 *  \param [in] pwr_on power on?
 *  \return None
 *
 *  \details Gate or Ungate power to PKE block
 */
extern void
api_pke_power(uint8_t pwr_on);


/**
 *  \name api_pke_reset
 *
 *  \return None
 *
 *  \details Reset PKE Block
 */
extern void
api_pke_reset( void );


/**
 *  \name api_pke_status
 *
 *  \return Return PKE Status register value; See PKE status register bit definitions.
 *
 *  \details Checks the status of the PKE block
 */
extern uint32_t
api_pke_status(void);


/**
 *  \name api_pke_is_done_status2
 *
 *  \param [in] hw_status POinter where PKE Status is updated
 *  \return True if done, false otherwise
 *
 *  \details Returns the done status of PKE block
 */
extern bool
api_pke_is_done_status2(uint32_t *status_value);


/**
 *  \name api_pke_start
 *
 *  \param [in] ien Interrupt Enable?
 *  \return None
 *
 *  \details Starts PKE Block
 */
extern void
api_pke_start(bool ien);


/**
 *  \name api_pke_ists_clear
 *
 *  \param [in] None
 *  \return PKE status register in bits[16:0], GIRQ16.Source[1:0]
 *          in bits[21:20].
 *
 *  \details Read and clear PKE status
 */
extern uint32_t
api_pke_ists_clear(void);


/**
 *  \name api_pke_busy
 *
 *  \return Busy? True if busy, false otherwise
 *
 *  \details Returns status if PKE block is busy or otherwise.
 */
extern uint8_t
api_pke_busy(void);


/**
 *  \name api_pke_clear_scm
 *
 *  \return None
 *
 *  \details Clear the Shared Crypto memory
 */
extern void
api_pke_clear_scm(void);

/**
 *  \name api_pke_scm_clear_slot
 *
 *  \param [in] slot_num Slot number in Shared Crypto Memory
 *  \return None
 *
 *  \details Clear the specified slot in Shared Crypto Memory
 */
extern void
api_pke_scm_clear_slot (uint8_t slot_num);


/**
 *  \name api_pke_read_scm
 *
 *  \param [in] dest Pointer
 *  \param [in] nbytes Number of bytes to be read
 *  \param [in] slot_num Slot number from which data is to be read
 *  \param [in] reverse_byte_order Reverse Byte order? 1 if yes, 0 otherwise
 *  \return Number of bytes Read
 *
 *  \details Read data from specified slot number in Shared Crypto memory
 */
extern uint32_t
api_pke_copy_from_scm(uint8_t* dest,
                      uint32_t nbytes,
                      uint8_t slot_num,
                      uint8_t reverse_byte_order);



/**
 * \name api_pke_copy_to_scm
 *
 * \param [in] constant pointer to word aligned data
 * \param [in] number of words(32-bit) to copy
 * \param [in] SCM start slot number (0-63)
 * \note SCM is 24KB with the lower 16KB organized as 32 slots of
 * 512 bytes each.
 * \return Number of bytes copied
 *
 *  \details  Copy bytes from source to PKE SCM with zero fill
 */
extern uint32_t
api_pke_copy_to_scm(const uint8_t *pdata,
                    uint32_t nbytes,
                    uint8_t slot_num);

/**
 * \name api_pke_copy_to_scm2
 *
 * \param [in] constant pointer to words (aligned)
 * \param [in] number of words to copy
 * \param [in] SCM start slot number (0-63)
 * \param [in] 0 = normal byte order, 1 = reverse byte order. If reverse byte
 * order is selected the whole input is byte reversed.
 * \return number of bytes copied.
 * \note SCM is 16KB organized as 64 slots of 256 bytes each.
 * If number of bytes is not a multiple of 256 then the remaining
 * bytes in the slot are zero filled. Reverse byte order means the
 * source bytes are copied reverse order into the beginning of the
 * PKE SCM slot.
 * \details - Copy bytes from source to PKE SCM with zero fill
 * and option to reverse the byte order.
 */
extern uint32_t
api_pke_copy_to_scm2(const uint8_t *pdata,
                     uint32_t nbytes,
                     uint8_t slot_num,
                     uint8_t reverse_byte_order);


/**
 * \name api_pke_copy_to_scm4
 *
 * \param [in] constant pointer to words (aligned)
 * \param [in] number of words to copy
 * \param [in] SCM start slot number (0-63)
 * \param [in] b[0]==0 normal byte order, 1 = reverse byte order.
 * b[1]==0 (no zero fill), 1 (zero fill to end of slot)
 * \return number of bytes copied.
 * \note SCM is 16KB organized as 64 slots of 512 bytes each.
 * \details - Copy bytes from source to PKE SCM with zero fill
 * and option to reverse the byte order.
 */
extern uint32_t
api_pke_copy_to_scm4(const uint8_t *pdata,
                          uint32_t nbytes,
                          uint8_t slot_num,
                          uint8_t flags);

/**
 *  \name api_pke_set_operand_slots
 *
 *  \param [in] slot information for operands A,B,C
 *  \return void
 *
 *  \details Writes slot information corresponding to operand pointers - A,B,C.
 */
extern void
api_pke_set_operand_slots (uint32_t op_slots);


/**
 *  \name api_pke_get_slot_addr
 *
 *  \param [in] slot number
 *  \return address of specified slot
 *
 *  \details Returns the slot address given a slot number.
 */
extern uint32_t
api_pke_get_slot_addr (uint8_t slot_num);


/**
 *  \name api_pke_fill_slot
 *
 *  \param [in] slot number
 *  \param [in] fill_val
 *  \return None
 *
 *  \details Fills the specified slot with fill_val.
 */
extern void
api_pke_fill_slot (const uint8_t slot_num,
                   const uint32_t fill_val);

/* PKE RSA */

typedef struct buff8_s
{
    uint32_t len;
    uint8_t* pd;
} BUFF8_T;




/**
 *  \name api_pke_rsa_crypt
 *
 *  \param [in] pointer to the message to be encrypted/decrypted
 *  \param [in] length of the message in bytes
 *  \param [in] flags -  b[0] = 0(data is LSBF), 1(data is MSBF)
 *                       b[1] = 0(encrypt/decrypt with public key), 1(encrypt/decrypt with private key)
 *                       b[4] = 0(do not start), 1(start)
 *                       b[5] = 0(if start, do not enable interrupts), 1(if start enable interrupts)
 *  \return PKE_RET_ERR_BAD_ADDR, PKE_RET_ERR_BUSY, PKE_RET_OK, PKE_RET_ERR_BAD_PARAM
 *
 *  \details Encrypt/decrypt provided message. Load Keys before this function is called
 */
extern uint8_t
api_pke_rsa_crypt (const uint8_t *pdata,
                   uint32_t nbytes,
                   uint16_t flags);


/**
 *  \name api_pke_rsa_crt_decrypt
 *
 *  \param [in] pointer to the message to be encrypted/decrypted
 *  \param [in] length of the message in bytes
 *  \param [in] flags - b[0] = 0(data is LSBF), 1(data is MSBF)
 *                      b[1] = HW always performs decryption using private exponent
 *                      b[2] = ignored, always perform decryption
 *                      b[4] = 0(do not start), 1(start)
 *                      b[5] = 0(if start, do not enable interrupts), 1(if start enable interrupts)
 *  \return PKE_RET_ERR_BAD_ADDR, PKE_RET_ERR_BUSY, PKE_RET_OK, PKE_RET_ERR_BAD_PARAM
 *
 *  \details Perform crt decryption on provided encrypted message. load keys before calling this function
 */
extern uint8_t
api_pke_rsa_crt_decrypt (const uint8_t *pdata,
                         uint32_t nbytes,
                         uint16_t flags);



/**
 *  \name api_pke_rsa_load_param
 *
 *  \param [in] slot number where the parameter is to be loaded into
 *  \param [in] pointer to the data which is to be copied
 *  \param [in] number of bytes to be copied
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_BUSY, PKE_RET_ERR_BAD_ADDR, PKE_RET_OK, PKE_RET_ERR_BAD_PARAM
 *
 *  \details Load rsa params directly to slots of the crypto engine
 */
extern uint8_t
api_pke_rsa_load_param (uint16_t param_loc,
                        const uint8_t *p,
                   		uint16_t nbytes,
                        uint8_t msbf);

/**
 *  \name api_pke_rsa_get_param
 *
 *  \param [in] slot number where the parameter is to be read from
 *  \param [in] pointer to the destination where the data is to be copied
 *  \param [in] number of bytes to be copied
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_BUSY, PKE_RET_ERR_BAD_ADDR, PKE_RET_OK, PKE_RET_ERR_BAD_PARAM
 *
 *  \details Read rsa params from slots of the crypto engine
 */
extern uint8_t
api_pke_rsa_get_param (uint16_t param_loc,
                       uint8_t *p,
                       uint16_t nbytes,
                       uint8_t msbf);
/**
 *  \name api_pke_rsa_operation
 *
 *  \param [in] Key length: 1024, 2048 or 4096
 *  \param [in] RSA operation to be performed - RSA_DO_MOD_EXP
 *  \param [in] Flag indicating if the done and error interrupts are to be set
 *  \return PKE_RET_ERR_BUSY,PKE_RET_ERR_UNKNOWN_OP,PKE_RET_ERR_INVALID_BIT_LENGTH,PKE_RET_OK
 *  \details Performs RSA Modular exponentiaiton
 */
extern uint8_t
api_pke_rsa_operation (uint16_t key_bitlen,
                       uint32_t op,
                       uint32_t flags);

/**
 *  \name api_pke_rsa_load_pub_key
 *
 *  \param [in] Key length: 1024, 2048 or 4096
 *  \param [in] pointer to location where the public modulus is stored
 *  \param [in] pointer to location where the public exponent is stored
 *  \param [in] Size of the public exponent (in bytes)
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_BAD_PARAM, PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_OK
 *  \details Loads the public modulus to Slot 0 and the public exponent to slot 8
 */
extern uint8_t
api_pke_rsa_load_pub_key (uint16_t rsa_key_bitlen,
                          const uint8_t *pmod,
                          const uint8_t *pexp,
                          uint16_t explen,
                          uint8_t msbf);
 /**
 *  \name api_pke_rsa_is_signature_valid
 *
 *  \param void
 *  \return 1 is RSA signature is valid; 0 otherwise
 *
 *  \details checks if the RSA signature is valid
 */
extern uint8_t
api_pke_rsa_is_signature_valid (void);

/**
 *  \name rsa_gen_signature
 *
 *  \param [in] rsa_bit_len 1024, 2048, 4096
 *  \param [in] hash_digest - structure variable of type BUFF8_T, containing
 *				length and pointer to hash_digest data array.
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_OK,PKE_RET_ERR_BAD_ADDR\
 *          PKE_RET_ERR_BUSY
 *
 *  \details Generate signature for a given hash digest. Requires public modulus\
 *           and private exponent to be programmed in slots.
 */
extern uint8_t
rsa_signature_gen(       uint16_t rsa_bit_len,
                   const BUFF8_T* hash_digest,
                         bool     msbf );
/**
 *  \name api_pke_rsa_load_prv_key
 *
 *  \param [in] Key length: 1024, 2048 or 4096
 *  \param [in] pointer to location where the public modulus is stored
 *  \param [in] pointer to location where the private exponent is stored
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_BAD_PARAM, PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_OK
 *  \details Loads the public modulus to Slot 0 and the private exponent to slot 6
 */
extern uint8_t
api_pke_rsa_load_prv_key (uint16_t rsa_key_bitlen,
                          const uint8_t *pmod,
                          const uint8_t *prvexp,
                          uint8_t msbf);
/**
 *  \name api_pke_rsa_load_pub_keystruct
 *
 *  \param [in] pointer to the RSA_PUB_KEY structure containing details about the RSA public key
 *  \return PKE_RET_ERR_BAD_PARAM, PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_OK
 *  \details Loads the public modulus to Slot 0 and the public exponent to slot 8
 */
extern uint8_t
api_pke_rsa_load_pub_keystruct (const RSA_PUB_KEY *pkey);

/**
 *  \name api_pke_rsa_load_key
 *
 *  \param [in] pointer to the RSA_KEY structure containing details about the RSA keys
 *  \return PKE_RET_ERR_BAD_PARAM, PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_OK
 *  \details Loads the public modulus to Slot 0, the private exponent to Slot 6 and the public exponent to slot 8
 */
extern uint8_t
api_pke_rsa_load_key (const RSA_KEY *pkey);

/**
 *  \name api_pke_rsa_get_key
 *
 *  \param [in] pointer to the RSA_KEY structure containing details about the RSA keys
 *  \param [in] Length of the public exponent in bytes
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_BUSY, PKE_RET_ERR_BAD_ADDR, PKE_RET_OK, PKE_RET_ERR_BAD_PARAM
 *  \details Reads the public modulus from Slot 0, the private exponent from Slot 6 and the public exponent from slot 8
 */
extern uint8_t
api_pke_rsa_get_key (RSA_KEY *pkey,
                     uint32_t pubexp_len,
                     uint8_t msbf);
/**
 *  \name api_pke_rsa_load_crt_key
 *
 *  \param [in] pointer to the RSA_CRT_KEY structure containing details about the CRT parameters
 *  \return PKE_RET_ERR_BUSY, PKE_RET_OK, PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_ERR_BAD_ADDR, PKE_RET_ERR_BAD_PARAM
 *  \details Loads prime1 into slot 2, prime 2 into slot 3, dp into slot A, dq into slot B and coefficient into slot C
 */
extern uint8_t
api_pke_rsa_load_crt_key (const RSA_CRT_KEY *pkey);

/**
 *  \name rsa_verify_signature
 *
 *  \param [in] rsa_bit_len 1024, 2048, 4096
 *  \param [in] signature - structure variable of type BUFF8_T, containing
 *				length and pointer to signature data array.
 *  \param [in] hash_digest_pkcs15 expected hash digest - structure variable
 *              of type BUFF8_T, containing length and pointer to signature data array.
 *  \param [in] msbf - LSB first or MSB first data byte order.
 *  \return PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_OK,PKE_RET_ERR_BAD_ADDR\
 *          PKE_RET_ERR_BUSY
 *
 *  \details Verify if signature generated is valid for given expected hash digest.\
 *			 Requires public modulus exponent to be programmed in slots.
 */
extern uint8_t
rsa_signature_verify(       uint16_t rsa_bit_len,
                      const BUFF8_T* signature,
                      const BUFF8_T* hash_digest_pkcs15,
                            bool     msbf );


/**
 * \name api_pke_srp_sc
 *
 * \param [in] pointer PKE_SRP_DATA structure containing length and pointers
 *             to each of the 7 parameters.
 * \return PKE_RET_OK on success
 *         PKE_RET_ERR_BAD_ADDR for null pointer
 *
 * \details Use PKE to generate step 4 of SRP algorithm.
 *
 * \note length is in units of 64 bytes and in range [0x02, 0x40]
 */
extern uint8_t
api_pke_srp_sc (const PKE_SRP_DATA *psrp);
/* PKE ECDSA */


/* Curve parameters are most-significant-byte first.
 * EC firmware will byte reverse before writing to PKE SCM.
 */
#define EC_FLAG_LSB (0u << 0)
#define EC_FLAG_MSB (1u << 0)
#define EC_FLAG_F2M (1u << 1) // Curve is binary
#define EC_FLAG_ANEG (1u << 2)
#define EC_FLAG_BNEG (1u << 3)

// PKE stored curve parameters in units of 64-bits(8-bytes) padded with zeros.
#define EC_192_PKE_LEN (1792ul / 8ul)
#define EC_224_PKE_LEN (256ul / 8ul)
#define EC_256_PKE_LEN (256ul / 8ul)
#define EC_384_PKE_LEN (384ul / 8ul)
#define EC_512_PKE_LEN (512ul / 8ul)
#define EC_640_PKE_LEN (640ul / 8ul)

/*
 * Actual Elliptic curve parameter lengths. The above ELLIPTIC_CURVE_xxx_LEN
 * parameters are the size of the PKE engine slots used for the curve. Slot lengths are in
 * units of 8-bytes and may be larger than actual curve parameters. PKE requires zero
 * padding of data smaller than the slot length. The symbols below are the actual curve
 * coordinate byte lengths.
 */
#define EC_P192_LEN (0x18ul)
#define EC_P224_LEN (0x1Cul)
#define EC_P256_LEN (0x20ul)
#define EC_P384_LEN (0x30ul)
#define EC_P521_LEN (0x42ul)
//
#define EC_B163_LEN (0x15ul)
#define EC_B233_LEN (0x1Eul)
#define EC_B283_LEN (0x24ul)
#define EC_B409_LEN (0x34ul)
#define EC_B571_LEN (0x48ul)

#define PKE_STATUS_SIG_NOT_VALID        (1UL << 9)
#define PKE_STATUS_PX_NOT_ON_CURVE      (1UL << 4)

#define SLOT_C              (0xC)
#define SLOT_D              (0xD)

/* Order of parameter pointers in struct elliptic_curve param[] member
 * These indices correspond to the SCM slot numbers. This could change
 * in a newer PKE IP block. */
#define ELLIPTIC_CURVE_PARAM_P      (0u)
#define ELLIPTIC_CURVE_PARAM_N      (1u)
#define ELLIPTIC_CURVE_PARAM_GX     (2u)
#define ELLIPTIC_CURVE_PARAM_GY     (3u)
#define ELLIPTIC_CURVE_PARAM_A      (4u)
#define ELLIPTIC_CURVE_PARAM_B      (5u)
#define ELLIPTIC_CURVE_NPARAMS      (6u)

/*Return Value Macro definitions for EC curve APIs*/
#define ECC_STARTED                         (0ul)
#define ECC_OK                              (0ul)
#define ECC_ERR_BUSY                        (1ul)
#define ECC_ERR_BAD_PARAM                   (2ul)
#define ECC_ERR_ZERO_LEN_PARAM              (3ul)
#define ECC_ERR_PARAM_LEN_MISMATCH          (4ul)
#define ECC_ERR_BAD_ADDR                    (5ul)
#define ECC_ERR_BAD_CMD                     (6ul)

typedef struct elliptic_curve
{
    uint32_t *param[ELLIPTIC_CURVE_NPARAMS];
    uint16_t byte_len;
    uint8_t flags;
    uint8_t rsvd1;
} ELLIPTIC_CURVE;



/**
 *  \name api_pke_ec_prog_curve
 *
 *  \param [in] pcurve The elliptic Curve to be programmed into PKE; Curves are defined in symdef.txt
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details Programs an curve into the PKE engine
 */
extern uint8_t
api_pke_ec_prog_curve( const ELLIPTIC_CURVE* curve_p );

/**
 *  \name api_pke_ecdsa_verify3
 *
 *  \param [in] Q      Pointer to public key
 *  \param [in] S      Pointer to signature
 *  \param [in] digest pointer to digest
 *  \param [in] elen   length of EC curve
 *  \param [in] dlen   digest length in bytes
 *  \param [in] msbf   most significant byte first?
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details programs the data required for verification
 */
extern uint8_t
api_pke_ecdsa_verify3 (const uint8_t* Q,
                       const uint8_t* S,
                       const uint8_t* digest,
                       uint16_t elen,
                       uint16_t dlen,
                       bool msbf);


/**
 *  \name api_pke_ec_kcdsa_verify
 *
 *  \param [in] pointer to array containing Qx and Qy
 *  \param [in] qlen length of public key
 *  \param [in] pointer to data array containing signature
 *  \param [in] slen length of signature
 *  \param [in] hash hash digest of message + certificate data
 *  \param [in] hlen length of hash digest
 * 	\param [in] flags bit[0]=0(Qx,y are LSBF), 1(Qx,y are MSBF)
 *              bit[1]=0(r,s are LSBF), 1(r,s are MSBF)
 *              bit[2]=0(digest is LSBF), 1(digest is MSBF)
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details Used to verify signature programmed to slots.
             Load crt params directly to slots of the crypto engine.
 */
extern uint8_t
api_pke_ec_kcdsa_verify (const uint8_t* q,
                            		   uint16_t qlen,
                            		   const uint8_t* sig,
                            		   uint16_t slen,
                            		   const uint8_t* hash,
                            		   uint16_t hlen,
                            		   uint16_t flags);

/**
 *  \name api_pke_ec_check_poc3
 *
 *  \param [in] point pxy
 *  \param [in] plen, length of EC curve passed for co ordinates Px, Py
 *  \param [in] msbf - msb first or lsb first of pxy parameter
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details Check if point lies on curve, needs curve to be programmed previously.
 */
extern uint8_t
api_pke_ec_check_poc3 (const uint8_t* p,
                       uint16_t plen,
                       uint8_t msbf);



/**
 *  \name api_pke_ec_check_pxy_less_prime
 *
 *  \param [in] point pxy
 *  \param [in] plen, length of EC curve passed for co ordinates Px, Py.
 *  \param [in] msbf - msb first or lsb first - pxy
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details Check Point coordinates are less than prime
 * 			 Requires curve has been programmed into PKE via
 * 			 pke_ec_prog_curve().
 */
extern uint8_t
api_pke_ec_check_pxy_less_prime (const uint8_t* pxy,
                                 uint16_t plen,
                                 bool msbf);

/**
 *  \name api_pke_set_operand_slot
 *
 *  \param [in] operand pointer A/B/C
 *  \param [in] slot number to which the operand should be mapped.
 *
 *  \details To map operand pointer to slot, write to pke config register.
 */
extern void
api_pke_set_operand_slot (uint8_t operand,
                          uint8_t slot_num);


/**
 *  \name api_pke_get_operand_slot
 *
 *  \param [in] operand pointer A/B/C
 *  \return [out] slot number to which the operand has been mapped.
 *
 *  \details To get mapping of slot information, read from pke config register.
 */
extern uint8_t
api_pke_get_operand_slot(uint8_t operand);


/**
 *  \name api_pke_ec_check_poc2
 *
 *  \param [in] point px
 *  \param [in] point py
 *  \param [in] plen, byte length of co ordinates passed
 *  \param [in] msbf : 0 - lsbf,1 - msbf - pxy
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details Check if point lies on curve, needs curve to be programmed previously.
 */
extern uint8_t
api_pke_ec_check_poc2 (const uint8_t* px,
                       const uint8_t* py,
                       uint16_t plen,
                       bool msbf);


/**
 *  \name api_pke_ec_check_ab
 *
 *  \param [in] none
 *  \return ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details Check if a,b parameters are valid, needs curve to be programmed previously.
 */
extern uint8_t
api_pke_ec_check_ab (void);

/**
 *  \name api_pke_ec_check_n
 *
 *  \param [in] none
 *  \return ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details Check if n parameter is valid, not equal to p, needs curve to be programmed previously.
 */
extern uint8_t
api_pke_ec_check_n (void);

/**
 *  \name api_pke_modular_math
 *
 *  \param [in] operand size
 *  \param [in] Pointer to parameter P
 *  \param [in] byte length of P
 *  \param [in] Pointer to parameter A
 *  \param [in] byte length of A
 *  \param [in] Pointer to parameter B
 *  \param [in] byte length of B
 *  \return PKE_RET_ERR_INVALID_BIT_LENGTH, PKE_RET_ERR_BUSY, PKE_RET_ERR_BAD_PARAM,
 *			PKE_RET_ERR_UNKNOWN_OP, PKE_RET_OK
 *
 *  \details Perform modular arithmetic operation.
 */
extern uint8_t
api_pke_modular_math (uint32_t op_size,
                      const void *P,
                      uint16_t pnbytes,
                      const void *A,
                      uint16_t anbytes,
                      const void *B,
                      uint16_t bnbytes);


/**
 *  \name api_pke_ec_kcdsa_keygen
 *
 *  \param [in] d, pointer to array containing EC Private key
 *  \param [in] plen, byte length of d
 *  \param [in] flags : bit 0 - p1x msbf, bit 1 - k msbf
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *  \details Generate ec kcdsa keys. Caller must have previously programmed elliptic curve
 *   					into PKE e.g. api_pke_ec_prog_curve().
 */
extern uint8_t
api_pke_ec_kcdsa_keygen (const uint8_t* d,
                         uint16_t plen,
                         uint16_t flags);


/**
 *  \name api_pke_ec_kcdsa_sign
 *
 *  \param [in] pointer to array containing EC Private key
 *  \param [in] plen , length of private key
 *  \param [in] pointer to array containing r component of signature
 *  \param [in] byte length of r
 *  \param [in] pointer to hash digest of message
 *  \param [in] byte length of hash digest
 *  \param [in] flags : bit[0]=0(d is LSBF), 1(d is MSBF)
 *              bit[1]=0(r is LSBF), 1(r is MSBF)
 *              bit[2]=0(digest is LSBF), 1(digest is MSBF)
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details Generate s component of signature; Caller must have previously programmed elliptic curve
 *   		 into PKE e.g. api_pke_ec_prog_curve().
 */
extern uint8_t
api_pke_ec_kcdsa_sign (const uint8_t* prv_key,
                       uint16_t plen,
                       const uint8_t* r,
                       uint16_t rlen,
                       const uint8_t* hash,
                       uint16_t hlen,
                       uint16_t flags);


/**
 *  \name api_pke_ec25519_ptmult
 *
 *  \param [in] point parameter p1x
 *  \param [in] p1x length
 *  \param [in] scalar k
 *  \param [in] length of scalar k
 *  \param [in] flags : bit 0 - p1x msbf, bit 1 - k msbf
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details point multiplication ec25519.
 */
extern uint8_t
api_pke_ec25519_ptmult (const uint8_t* p1x,
                        uint16_t p1x_len,
                        const uint8_t* k,
                        uint16_t k_len,
                        uint16_t flags);

/**
 *  \name api_pke_ed25519_xrecov
 *
 *  \param [in] coordinate y of point
 *  \param [in] y length of y coordinate
 *  \param [in] flags: bit 0 - y msbf
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details ed25519 xrecover - generate x coordinate, given y coordinate of a point
 */
extern uint8_t
api_pke_ed25519_xrecov (const uint8_t* y,
                        uint16_t ylen,
                        uint16_t flags);


/**OLD api **/
extern uint8_t ec25519_xrecover(const uint8_t* y, uint16_t ylen, uint16_t flags);




/**
 *  \name api_pke_ed25519_scalarmult
 *
 *  \param [in] x coordinate of point px
 *  \param [in] px length of px
 *  \param [in] y coordinate of point py
 *  \param [in] py length of py
 *  \param [in] scalar e
 *  \param [in] length of scalar e
 *  \param [in] flags : bit 0 - px,py msbf, bit 1 - e msbf
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details This API is used to perform scalar multiplication on ed25519 curve.
 *  This function multiplies a point by the specified scalar and stores the result at
 *  Slot[0xA]=x-coordinate and Slot[0xB]=y-coordinate in the SCM.
 */
extern uint8_t
api_pke_ed25519_scalarmult (const uint8_t* px,
                            uint16_t pxlen,
                            const uint8_t* py,
                            uint16_t pylen,
                            const uint8_t* e,
                            uint16_t elen,
                            uint16_t flags);



#define ED_PARAM_AX   0
#define ED_PARAM_AY   1
#define ED_PARAM_RX   2
#define ED_PARAM_RY   3
#define ED_PARAM_SIG  4
#define ED_PARAM_HASH 5
#define ED_PARAM_MAX  6

typedef struct {
    uint8_t const * params[ED_PARAM_MAX];
    uint16_t paramlen[ED_PARAM_MAX];
    uint16_t flags;
    uint16_t rsvd;
} Ed25519_SIG_VERIFY;



/**
 *  \name api_pke_ed25519_valid_sig
 *
 *  \param [in] structure variable of type Ed25519_SIG_VERIFY
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details This API verifies if a given signature is valid for provided hash digest.
 * 1. Call pke_power() to power on the block.
 * 2. Call ed25519_valid_sig() API with structure variable of type Ed25519_SIG_VERIFY.
 * 3. Call pke_start() and wait for operation to complete(pke_busy()).
 * 4. The parameters P1x,P1y,P2x,P2y,P3x,P3y are loaded to slots A,B,C,D,E,F respectively.
 * 5. Verify validity of signature by comparing P1 and P3.
 * 
 * Measured empirically:
 * P1 := s.G
 * P2 := h.A
 * P3 := R + h.A
*/
extern uint8_t
api_pke_ed25519_valid_sig (const Ed25519_SIG_VERIFY* psv);


/**
 *  \name api_pke_ec_point_double
 *
 *  \param [in] point pxy
 *  \param [in] length of point pxy
 *  \param [in] msbf - pxy
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details ec point doubling
 */
extern uint8_t
api_pke_ec_point_double (const uint8_t* pxy,
                         uint16_t coord_len,
                         bool msbf);



/**
 *  \name api_pke_ec_point_addition
 *
 *  \param [in] point p1xy
 *  \param [in] point p2xy
 *  \param [in] length of points
 *  \param [in] msbf - pxy
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details addition of 2 points on curve
 */
extern uint8_t
api_pke_ec_point_addition (const uint8_t* p1xy,
                           const uint8_t* p2xy,
                           uint16_t coord_len,
                           bool msbf);
/**
 *  \name api_pke_ec_point_scalar_mult
 *
 *  \param [in] point pxy
 *  \param [in] length of pxy
 *  \param [in] scalar
 *  \param [in] length of scalar
 *  \param [in] msbf - 1 if msb first for parameter px, py
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details ec point scalar multiplication
 */
extern uint8_t
api_pke_ec_point_scalar_mult (const uint8_t* pxy,
                              uint16_t coord_len,
                              const uint8_t* pscalar,
                              uint16_t scalar_len,
                              uint8_t msbf);


/**
 *  \name api_pke_ec_point_scalar_mult2
 *
 *  \param [in] point px
 *  \param [in] point py
 *  \param [in] scalar
 *  \param [in] length of scalar
 *  \param [in] msbf - 1 if msb first for parameter px, py
 *  \return ECC_ERR_BAD_PARAM, ECC_ERR_BUSY, PKE_RET_OK
 *
 *  \details ec point scalar multiplication
 */
extern uint8_t
api_pke_ec_point_scalar_mult2 (const uint8_t* px,
                               const uint8_t* py,
                               const uint8_t* pscalar,
                               uint16_t byte_len,
                               bool msbf);



/**
 *  \name api_pke_ec_point_scalar_mult3
 *
 *  \param [in] point pxy
 *  \param [in] scalar
 *  \param [in] byte length of point
 *  \param [in] msbf - 1 if msb first for parameter pxy
 *  \return ECC_ERR_BAD_PARAM, PKE_RET_OK
 *
 *  \details ec point scalar multiplication
 */
extern uint8_t
api_pke_ec_point_scalar_mult3 (const uint8_t* pxy,
                               const uint8_t* pscalar,
                               uint16_t byte_len,
                               bool msbf);

typedef struct {
    uint32_t ld_addr;
    uint32_t byte_len;
    uint32_t spi_addr;
    uint32_t entry_addr;
} LOAD_DESCR;


/**
 *  \name api_ldr_api0
 *
 *  \param [in] config
 *  		    b[7:0] = interface
 *      		    0 = Use ROM POR load interface
 *      		    1 = Shared SPI
 *      		    2 = Private SPI
 *      		    3 = eSPI
 *      		    4 = Internal SPI
 *  			b[9:8] = SPI Freq MHz 0=48, 1=24, 2=16, 3=12 (N/A for eSPI)
 *  			b[11:10] = SPI Drive Strength (N/A for eSPI)
 *  			b[12] = SPI Slew Rate (N/A for eSPI)
 *  			b[15:13] = 0 reserved
 *  			b[19:16] = DMA channel (0-13)   (N/A for eSPI)
 *  			b[23:20] = 0 reserved
 *  			b[30] = 0 Do not return to caller, 1 Return to caller
 *  			b[31] = 0 ROM takes over interrupts (vector table set to ROM table),
 *                      1 caller retains ownership of interrupt vector table
 *  \param [in] structure variable of type LOAD_DESCR
 *  \param [in] ecdsa public key, if authentication has been enabled; else NULL
 *  \param [in] ecdh private key, if encryption has been enabled; else NULL
 *  \param [in] buff2k buffer required for implementing the function, aligned on a 16 byte boundary;
                minimum required size is 4 bytes.
 *  \return bit[31] == 1 indicates an error
 *  		bit[31] == 0,
 *          bits[30:0] = loaded application entry point address.
*/
extern uint32_t
api_ldr_api0 ( uint32_t    config,
               LOAD_DESCR* pldr,
               uint32_t*   p256_ecdsa_pub,
               uint32_t*   p256_ecdh_prv,
               uint32_t*   buff2k );


/**
 *  \name api_rom_ver
 *
 *  \param [in] void
 *  \return Current Firmware's build number.
 */
extern uint32_t
api_rom_ver( void );

/**
 *  \name api_rom_dis_lock_shd_spi
 *
 *  \param [in] 0(do not modify lock values), 1(insure Shared SPI GPIO's are disabled (tri-state input) and
 *          these pins are locked.
 *  \return void
 *
 *  \details apply GPIO Locks as specified in customer section of EFUSE.
 *
 *  \note Disable all six QMSPI Shared SPI GPIO pins by writing their GPIO Control registers to 0x0000_0040
 * which is GPIO, input, no-PUD, no interrupt detect, no-invert, VTR power gate.
 */
extern void
api_rom_dis_lock_shd_spi(uint8_t lock_shd_spi);


extern const ELLIPTIC_CURVE ec_sect571r1;
extern const ELLIPTIC_CURVE ec_sect409r1;
extern const ELLIPTIC_CURVE ec_sect283r1;
extern const ELLIPTIC_CURVE ec_sect233r1;
extern const ELLIPTIC_CURVE ec_sect163r2;
extern const ELLIPTIC_CURVE ec_secp521r1;
extern const ELLIPTIC_CURVE ec_secp384r1;
extern const ELLIPTIC_CURVE ec_secp256r1;
extern const ELLIPTIC_CURVE ec_secp224r1;
extern const ELLIPTIC_CURVE ec_secp192r1;

#ifdef __cplusplus
}
#endif


#endif /* INCLUDE_MEC2016_MEC2016_ROM_API_H_ */
