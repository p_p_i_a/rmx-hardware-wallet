/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *   - Whereas this program also includes the software created under the copyright of ARM LIMITED, 
 *     the following conditions are also part of the additional terms:
 *
 * *************************************************************************
 * 	@file     system_ARMCM4.c
 *	@brief    CMSIS Device System Source File for
 *      ARMCM4 Device Series
 *	@version  V1.09
 *	@date     27. August 2014
 *
 * 	@note
 *
 ******************************************************************************
 *   Copyright (c) 2011 - 2014 ARM LIMITED
 *
 *     All rights reserved.
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  - Neither the name of ARM nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDERS AND CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE."
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. Shall the abovementioned 
 * disclaimer provide stricter or different rule, it is considered additional term permitted
 * by the GNU General Public License and it prevails to the extent it does not constitute
 * the further restriction according to section 10. of the GNU General Public License.
 *
 * You should have received a copy of the GNU General Public License along with this program.  
   If not, see <http://www.gnu.org/licenses/>.

   ---------------------------------------------------------------------------*/

#include "system_MEC170x.h"
#include "MCHP_MEC170x.h"

/*----------------------------------------------------------------------------
  Define clocks
 *----------------------------------------------------------------------------*/
#define __HSI             (48000000UL)    /* Value of Internal oscillator in Hz*/
#define __XTAL            (48000000UL)    /* Oscillator frequency             */

#define __SYSTEM_CLOCK    (__XTAL/PCR_CLK_DIVIDER)  /* see system_MEC170x.h   */

/*----------------------------------------------------------------------------
  System Core Clock Variable
 *----------------------------------------------------------------------------*/
uint32_t SystemCoreClock = __SYSTEM_CLOCK; /* System Core Clock Frequency      */

void SystemInit(void) {
  VBAT->CLOCK_EN_b.XOSEL = XOSEL_DUAL_CRYSTAL;     /* Crystal OSC double-ended*/
  VBAT->CLOCK_EN_b.EXT_32K = 0;                    /* 32K enabled      */

  /* delay a little while until 32KHz stable, for crystal only */
  for (int i=0; i<100; i++) asm("mov r0,r0");
  
  while (PCR->OSC_ID_b.PLL_LOCK == 0); /* wait for PLL to lock */

  SystemCoreClock = __SYSTEM_CLOCK;

  PCR->PROC_CLK_CNTRL = PCR_CLK_DIVIDER;
}
