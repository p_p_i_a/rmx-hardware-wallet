/*
 * This file is a part of RMX HARDWARE WALLET
 *
 * Copyright (C) 2018 i_a <digital fingerprint: 872E 748B 97F7 8E39 8D61  706D 2A81 682B 4BE0 C6A5> <i_a@rmxwallet.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version and under the following additional terms permitted by GNU General Public License: 
 *
 *   - Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   - Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   - Neither the name of RMX HARDWARE WALLET nor the term RMX nor the names of its contributors
 *     may be used to endorse or promote products derived from this
 *     software without specific prior written permission of the copyright holder.
 *             
 * This program is distributed in the hope that it will be useful,but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details. 
 *
 * You should have received a copy of the GNU General Public License along with this program.  
 * If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef CEC_1702_DEFINES_H
#define CEC_1702_DEFINES_H

// POINTERS TO PIN CONTROL REGISTERS 
#define GPIO001_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO001_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO002_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO002_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO003_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO003_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO004_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO004_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO007_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO007_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO010_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO010_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO012_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO012_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO013_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO013_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO016_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO016_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO017_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO017_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO020_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO020_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO021_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO021_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO026_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO026_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO027_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO027_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO030_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO030_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO031_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO031_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO032_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO032_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO034_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO034_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO036_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO036_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO040_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO040_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO045_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO045_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO046_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO046_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO047_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO047_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO050_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO050_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO051_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO051_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO053_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO053_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO054_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO054_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO055_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO055_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO056_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO056_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO104_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO104_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO105_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO105_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO107_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO107_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO112_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO112_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO113_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO113_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO120_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO120_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO121_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO121_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO122_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO122_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO124_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO124_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO125_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO125_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO127_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO127_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO134_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO134_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO135_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO135_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO140_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO140_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO145_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO145_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO146_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO146_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO147_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO147_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO150_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO150_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO154_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO154_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO155_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO155_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO156_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO156_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO157_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO157_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO162_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO162_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO163_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO163_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO165_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO165_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO170_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO170_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO171_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO171_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO200_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO200_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO201_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO201_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO202_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO202_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO203_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO203_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO204_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO204_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO223_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO223_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO224_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO224_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO225_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO225_PIN_CONTROL_REGISTER_ADDRESS ))
#define GPIO227_PIN_CONTROL_REGISTER     (*((volatile unsigned int*) GPIO227_PIN_CONTROL_REGISTER_ADDRESS ))

// POINTERS TO PIN CONTROL 2 REGISTERS 
#define GPIO001_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081504 ))
#define GPIO002_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081508 ))
#define GPIO003_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008150C ))
#define GPIO004_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081504 ))
#define GPIO007_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008151C ))
#define GPIO010_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081520 ))
#define GPIO012_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081528 ))
#define GPIO013_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008152C ))
#define GPIO016_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081538 ))
#define GPIO017_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008153C ))
#define GPIO020_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081540 ))
#define GPIO021_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081544 ))
#define GPIO026_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081558 ))
#define GPIO027_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008155C ))
#define GPIO030_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081560 ))
#define GPIO031_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081564 ))
#define GPIO032_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081568 ))
#define GPIO034_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081570 ))
#define GPIO036_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081578 ))
#define GPIO040_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081580 ))
#define GPIO045_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081594 ))
#define GPIO046_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081598 ))
#define GPIO047_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008159C ))
#define GPIO050_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400815A0 ))
#define GPIO051_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400815A4 ))
#define GPIO053_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400815AC ))
#define GPIO054_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400815B0 ))
#define GPIO055_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400815B4 ))
#define GPIO056_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400815B8 ))
#define GPIO104_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081610 ))
#define GPIO105_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081614 ))
#define GPIO107_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008161C ))
#define GPIO112_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081628 ))
#define GPIO113_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008162C ))
#define GPIO120_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081640 ))
#define GPIO121_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081644 ))
#define GPIO122_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081648 ))
#define GPIO124_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081650 ))
#define GPIO125_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081654 ))
#define GPIO127_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008165C ))
#define GPIO134_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081670 ))
#define GPIO135_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081674 ))
#define GPIO140_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081680 ))
#define GPIO145_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081694 ))
#define GPIO146_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081698 ))
#define GPIO147_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008169C ))
#define GPIO150_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816A0 ))
#define GPIO154_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816B0 ))
#define GPIO155_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816B4 ))
#define GPIO156_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816B8 ))
#define GPIO157_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816BC ))
#define GPIO162_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816C8 ))
#define GPIO163_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816CC ))
#define GPIO165_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816D4 ))
#define GPIO170_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816E0 ))
#define GPIO171_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x400816E4 ))
#define GPIO200_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081700 ))
#define GPIO201_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081704 ))
#define GPIO202_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081708 ))
#define GPIO203_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008170C ))
#define GPIO204_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081210 ))
#define GPIO223_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008174C ))
#define GPIO224_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081750 ))
#define GPIO225_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x40081754 ))
#define GPIO227_PIN_CONTROL_2_REGISTER   (*((volatile unsigned int*) 0x4008175C ))

// PIN CONTROL REGISTER_ADDRESSS 
#define GPIO001_PIN_CONTROL_REGISTER_ADDRESS   0x40081004
#define GPIO002_PIN_CONTROL_REGISTER_ADDRESS   0x40081008
#define GPIO003_PIN_CONTROL_REGISTER_ADDRESS   0x4008100C
#define GPIO004_PIN_CONTROL_REGISTER_ADDRESS   0x40081010
#define GPIO007_PIN_CONTROL_REGISTER_ADDRESS   0x4008101C
#define GPIO010_PIN_CONTROL_REGISTER_ADDRESS   0x40081020
#define GPIO012_PIN_CONTROL_REGISTER_ADDRESS   0x40081028
#define GPIO013_PIN_CONTROL_REGISTER_ADDRESS   0x4008102C
#define GPIO016_PIN_CONTROL_REGISTER_ADDRESS   0x40081038
#define GPIO017_PIN_CONTROL_REGISTER_ADDRESS   0x4008103C
#define GPIO020_PIN_CONTROL_REGISTER_ADDRESS   0x40081040
#define GPIO021_PIN_CONTROL_REGISTER_ADDRESS   0x40081044
#define GPIO026_PIN_CONTROL_REGISTER_ADDRESS   0x40081058
#define GPIO027_PIN_CONTROL_REGISTER_ADDRESS   0x4008105C
#define GPIO030_PIN_CONTROL_REGISTER_ADDRESS   0x40081060
#define GPIO031_PIN_CONTROL_REGISTER_ADDRESS   0x40081064
#define GPIO032_PIN_CONTROL_REGISTER_ADDRESS   0x40081068
#define GPIO034_PIN_CONTROL_REGISTER_ADDRESS   0x40081070
#define GPIO036_PIN_CONTROL_REGISTER_ADDRESS   0x40081078
#define GPIO040_PIN_CONTROL_REGISTER_ADDRESS   0x40081080
#define GPIO045_PIN_CONTROL_REGISTER_ADDRESS   0x40081094
#define GPIO046_PIN_CONTROL_REGISTER_ADDRESS   0x40081098
#define GPIO047_PIN_CONTROL_REGISTER_ADDRESS   0x4008109C
#define GPIO050_PIN_CONTROL_REGISTER_ADDRESS   0x400810A0
#define GPIO051_PIN_CONTROL_REGISTER_ADDRESS   0x400810A4
#define GPIO053_PIN_CONTROL_REGISTER_ADDRESS   0x400810AC
#define GPIO054_PIN_CONTROL_REGISTER_ADDRESS   0x400810B0
#define GPIO055_PIN_CONTROL_REGISTER_ADDRESS   0x400810B4
#define GPIO056_PIN_CONTROL_REGISTER_ADDRESS   0x400810B8
#define GPIO104_PIN_CONTROL_REGISTER_ADDRESS   0x40081110
#define GPIO105_PIN_CONTROL_REGISTER_ADDRESS   0x40081114
#define GPIO107_PIN_CONTROL_REGISTER_ADDRESS   0x4008111C
#define GPIO112_PIN_CONTROL_REGISTER_ADDRESS   0x40081128
#define GPIO113_PIN_CONTROL_REGISTER_ADDRESS   0x4008112C
#define GPIO120_PIN_CONTROL_REGISTER_ADDRESS   0x40081140
#define GPIO121_PIN_CONTROL_REGISTER_ADDRESS   0x40081144
#define GPIO122_PIN_CONTROL_REGISTER_ADDRESS   0x40081148
#define GPIO124_PIN_CONTROL_REGISTER_ADDRESS   0x40081150
#define GPIO125_PIN_CONTROL_REGISTER_ADDRESS   0x40081154
#define GPIO127_PIN_CONTROL_REGISTER_ADDRESS   0x4008115C
#define GPIO134_PIN_CONTROL_REGISTER_ADDRESS   0x40081170
#define GPIO135_PIN_CONTROL_REGISTER_ADDRESS   0x40081174
#define GPIO140_PIN_CONTROL_REGISTER_ADDRESS   0x40081180
#define GPIO145_PIN_CONTROL_REGISTER_ADDRESS   0x40081194
#define GPIO146_PIN_CONTROL_REGISTER_ADDRESS   0x40081198
#define GPIO147_PIN_CONTROL_REGISTER_ADDRESS   0x4008119C
#define GPIO150_PIN_CONTROL_REGISTER_ADDRESS   0x400811A0
#define GPIO154_PIN_CONTROL_REGISTER_ADDRESS   0x400811B0
#define GPIO155_PIN_CONTROL_REGISTER_ADDRESS   0x400811B4
#define GPIO156_PIN_CONTROL_REGISTER_ADDRESS   0x400811B8
#define GPIO157_PIN_CONTROL_REGISTER_ADDRESS   0x400811BC
#define GPIO162_PIN_CONTROL_REGISTER_ADDRESS   0x400811C8
#define GPIO163_PIN_CONTROL_REGISTER_ADDRESS   0x400811CC
#define GPIO165_PIN_CONTROL_REGISTER_ADDRESS   0x400811D4
#define GPIO170_PIN_CONTROL_REGISTER_ADDRESS   0x400811E0
#define GPIO171_PIN_CONTROL_REGISTER_ADDRESS   0x400811E4
#define GPIO200_PIN_CONTROL_REGISTER_ADDRESS   0x40081200
#define GPIO201_PIN_CONTROL_REGISTER_ADDRESS   0x40081204
#define GPIO202_PIN_CONTROL_REGISTER_ADDRESS   0x40081208
#define GPIO203_PIN_CONTROL_REGISTER_ADDRESS   0x4008120C
#define GPIO204_PIN_CONTROL_REGISTER_ADDRESS   0x40081210
#define GPIO223_PIN_CONTROL_REGISTER_ADDRESS   0x4008124C
#define GPIO224_PIN_CONTROL_REGISTER_ADDRESS   0x40081250
#define GPIO225_PIN_CONTROL_REGISTER_ADDRESS   0x40081254
#define GPIO227_PIN_CONTROL_REGISTER_ADDRESS   0x4008125C

#endif 





























